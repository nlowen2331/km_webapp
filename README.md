##Web Application for K&M Property Management LLC

This application allows clients to browse & book rental properties

[Features]

	- Visually appealing display of rental properties and their data
	
	- Ability to contact owners and make reservations
	
	- Owners log in portal to respond to bookings and messages
	
	- Owners can also update data for units and sync calenders with other booking sites