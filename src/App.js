import React,{useState,useEffect} from 'react';
import './App.css';
import Navbar from './components/Navbar.js';
import Footer from './components/Footer'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home.js';
import OurProperties from './pages/OurProperties.js';
import ContactUs from './pages/ContactUs.js';
import Test from './pages/Test.js';
import Unit from './pages/Unit.js';
import ScrollToTop from './components/ScrollToTop';
import FetchData from './data/UnitData';
import OwnersPage from './pages/OwnersPage';
import './components/DateFunctions';

function App() {

  const [units, updateUnits] = useState([])
  const [isLoaded, toggleIsLoaded] = useState(false)

  useEffect(() => {
    FetchData(updateUnits, toggleIsLoaded)
  }, [])

  let routes = []

  if(isLoaded) {
    routes = units.map((card)=>(
      <Route path={'/'+card.unitid} key={card.unitid}>
          <Unit attributes={card}/>
      </Route>
    ))
  }

  return (
    <>
      <Router>
        <ScrollToTop/>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home}/>
          <Route path='/OurProperties' component={OurProperties} />
          <Route path='/ContactUs' component={ContactUs} />
          <Route path='/Test' component={Test}/>
          <Route path='/Owners' component={OwnersPage}/>
          {routes}
        </Switch>
        <Footer/>
      </Router>
    </>
  );
}

export default App;