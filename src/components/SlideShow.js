import React, { useEffect, useState } from 'react';
import './SlideShow.css';
import Carousel, { Dots} from '@brainhubeu/react-carousel'
import SlideShowItem from './componentsv2/SlideShowItem';
import '@brainhubeu/react-carousel/lib/style.css';
import {MdKeyboardArrowLeft,MdKeyboardArrowRight} from 'react-icons/md'

function SlideShow({picKeys,index,setIndex}) {

    const [slides, setSlides] = useState(picKeys.map((key,i)=><SlideShowItem picKey={key}/>))

    const handleArrowClick = (e) =>{
        switch(e){
            case 1:
                if(index+1>=slides.length){
                    setIndex(0)
                }
                else{
                    setIndex(index=>index+1)
                }
                break;
            case -1:
                if(index-1<0){
                    setIndex(slides.length-1)
                }
                else{
                    setIndex(index=>index-1)
                }
                break;
        }
    }

    return (
        <>  
            <Carousel value={index} onChange={(value)=>setIndex(value)}>
                {slides}
            </Carousel>
            <Dots value={index} onChange={(value)=>setIndex(value)} number={slides.length}/>
            <div className='arrow-container'>
                <MdKeyboardArrowLeft className='arrow' id='left' onClick={()=>handleArrowClick(-1)}/>
                <MdKeyboardArrowRight className='arrow' id='right' onClick={()=>handleArrowClick(1)}/>
            </div>
        </>
    )
}

export default SlideShow
