import React from 'react'

/*
    Feed me a json object containing ammenities,
    I will give thee a JSX with a bulleted list
    of those items
*/
const getBulletedAmmenitiesList = (json)=>{

    let bulletedList = (
        <p>
            {json.sleeps!=null ? <span>• Sleeps {json.sleeps} </span> : ''}
            {json.bathrooms!=null ? <span>• {json.bathrooms} Bathroom </span> : ''}
            {json.bedrooms!=null ? <span>• {json.bedrooms} Bedroom </span> : ''}
            {json.parking!=null ? <span>• Parking for {json.parking} </span> : ''}
        </p>

    )

    return bulletedList
}

const getBulletedRulesList = (json)=>{

    let bulletedList = (
        <p>
            {json.pets!=null ? <span>• {json.bathrooms} No pets </span> : null}
            {json.smoking!=null ? <span>• {json.bathrooms} No smoking </span> : null}
        </p>

    )

    return bulletedList
}

export {getBulletedAmmenitiesList,getBulletedRulesList}