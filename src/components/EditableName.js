import React, { useState } from 'react'
import {MdCancel} from 'react-icons/md'
import * as FaIcons from 'react-icons/fa'
import styled from 'styled-components'

/**
 * Takes in editing?,name,()=>saveName()
 * 
 * Displays name as normal div when not in edit mode
 * 
 * When in edit mode, if name is clicked, it displays as an 
 * input with 'name' as default value and a 'submit' button alongside
 * 
 */

const max_input_length = 40

function EditableName(props) {

    const [nameInput,changeNameInput] = useState(props.name)
    const [editState,toggleEditState] = useState(false)
    const [hoveringOverName,toggleHoveringOverName] = useState(false)

    let FinalComponent = <></>

    const CleanUp = async ()=>{
        toggleEditState(false)
        toggleHoveringOverName(false)
        changeNameInput(props.name)
    }

    const Submit = async ()=>{
        toggleEditState(false)
        toggleHoveringOverName(false)
        await props.saveName({type: 'misc',evName: 'changeUnitName',value:nameInput})
    }

    //If the unit is being edited
    if(props.isEditing) {
        
        //If the user has clicked on 
        if(editState) {
            FinalComponent=(
                <div style={{display: 'grid',gridTemplateColumns: '1fr 1fr 1fr',gap: '10px',
                    alignItems: 'center',width: '250px',justifyContent: 'center'}}>
                    <Input maxLength={max_input_length} onChange={(e)=>changeNameInput(e.target.value)} value={nameInput} />
                    <MdCancel onClick={()=>CleanUp()} style={{color: 'darkred',height: '30px',width:'30px',cursor: 'pointer'}}/>
                    {props.name!==nameInput ?
                        <FaIcons.FaCheck onClick={()=>Submit()} style={{color: 'darkgreen',height: '30px',width:'30px',cursor: 'pointer'}}/>
                        :
                        ''
                    }
                    
                </div>
            )
        }
        else{
            FinalComponent=(
                <div onClick={()=>toggleEditState(true)} onMouseEnter={()=>toggleHoveringOverName(true)}
                    onMouseLeave={()=>toggleHoveringOverName(false)} style={{cursor: 'pointer'}}>
                    <span>
                        {props.name}<PencilIcon on={hoveringOverName}><FaIcons.FaPencilAlt/></PencilIcon>
                    </span>
                </div> 
            )
        }

    }
    else{
        FinalComponent=(<div>{props.name}</div>)
    }

    return FinalComponent
}

let PencilIcon = styled.div`
    opacity: ${props=>props.on ? 1 : 0};
    width: 20px;
    height: 20px;
    transition: 500ms;
    display: inline;
    margin-left: 10px;

`

let Input = styled.input`

    padding: .3em;

    &:focus{
        outline: solid 3px blueviolet

    }
`

export default EditableName
