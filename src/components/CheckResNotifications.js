
import imconfig from '../data/config.js'
const config = imconfig.config


const CheckResNotifications = (token) => fetch(config.ip + config.port + config.route_getInquiries + `/0/true`, {
    method: "get",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
}).then((res) => {
    return new Promise((resolve, reject) => {
        res.json().then(json => {
            if (json !== null && json.error_code == 0) {
                console.log('Recieved count')
                let data = json.count
                resolve(data)
            }
            else {
                console.log('There was an error getting notifications from the server')
                reject(json.message)
            }
        })
    });
})

export default CheckResNotifications