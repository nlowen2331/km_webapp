import React, { useState } from 'react'
import styled from 'styled-components'
import EditableMenu from './EditableMenu'

/**
 * Props:
 * picture (from db) {isCover,isHidden,picture_id}
 * updatePicture() function that changes the attributes of the picture
 * canEdit, allows this unit to open its menu
 * 
 * Functionality:
 * Image that responds to 'hidden' and 'cover' attributes
 * allows you to set attributes of specific picture
 * 
 */

function EditablePicture(props) {

    const [menuIsOn,toggleMenu] = useState(false)

    const EditPicture = (params)=>{
        console.log('Called EditPicture')
        props.updatePicture({...params,key: props.picture.picture_id,type: 'picture'})
    }

    return (
        <UnitImg isHidden={props.picture.isHidden} isCover={props.picture.isCover} img={props.picture.img} onMouseEnter={()=>toggleMenu(true)}
            onMouseLeave={()=>toggleMenu(false)}>
           {menuIsOn && props.canEdit ? <EditableMenu options={[
               {isOn: props.picture.isHidden, title: `Hide this picture?`, toggle: ()=>EditPicture({evName: `changeIsHidden`, value: !props.picture.isHidden})},
               {isOn: props.picture.isCover, title: `Mark as cover picture?`, toggle: ()=>EditPicture({evName: `changeIsCover`, value: !props.picture.isCover})}
               
               ]}/> : <></>}
        </UnitImg>
    )
}

let UnitImg = styled.div`

    opacity: ${props=> props.isHidden ? ' 0.5':' 1'};
    border: ${props=> props.isCover ? '3px solid red' : 'none'};
    border-radius: 1em;
    position: relative;
    transition: all 0.2s ease-in-out;
    background: url(${props=> props.img || './images/alt.jpg'});
    background-repeat: no-repeat;
    background-size: cover;
    height: 200px;
    width: 230px;

    &:hover {
        opacity: ${props=> props.canEdit ? '0.8' : 'initial'};
    }
`;

export default EditablePicture
