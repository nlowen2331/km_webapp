import imconfig from '../data/config'
const config = imconfig.config

/*
    Get the lease for a specified unitid
*/

const GetUnitLease = async (unitid) => {
    //Get server response
    let Response = await fetch(config.ip + config.port + config.route_GetLeaseForUnit + '/' + unitid, {
        method: "get",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/pdf'
        }
    })

    let Status = Response.status

    switch(Status){
        case 200:
            let Blob = await Response.blob()

            let PDF = URL.createObjectURL(Blob)

            return ({OK: true,pdf: PDF})
        case 404:
            return({OK: false, msg: `NoLeaseForUnit`})
        default:
            return({OK:false,msg: `UndefinedError`})
    }
}


export default GetUnitLease