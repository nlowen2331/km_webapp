import React from 'react'
import styled from 'styled-components'
import * as FaIcons from 'react-icons/fa'

/**
 * 
 * @param {options} props 
 * 
 * options [{isOn, title, toggle}[,...]]
 * 
 * 
 */

function EditableMenu(props) {

    return (
        <Menu width={props.width}>
            {props.options.map(option=>(
                <MenuOptionContainer>
                    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                        <CheckBox onClick={option.toggle}>
                            {option.isOn ? <FaIcons.FaCheck style={{position: "absolute"}}/> : <></>}
                        </CheckBox>
                    </div>
                    <div style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start',
                    fontSize: '.9em', textAlign: 'start'}}>
                        {option.title}
                    </div>
                </MenuOptionContainer>
            ))}
           
        </Menu>
    )
}

const Menu = styled.div`
    background-color: white;
    width: ${props=>props.width||'100%'};
    padding: 0px;
    border: 2px solid black;
    z-index: 20;
    border-radius: inherit;
    padding-top: .5em;
    padding-bottom: .5em;

    &:hover {
        cursor: default;
    }
`

const MenuOptionContainer = styled.div`
    display: grid;
    grid-template-columns: 30px 1fr;
    grid-template-rows: 1fr;
    grid-gap: 1em;
    width: 100%;
`

const CheckBox = styled.div`
    border: 1px solid black;
    height: 1em;
    width: 1em;
    margin: 0em;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
`

const Input = styled.input`
    border: 1px solid black;
    border-radius: 10px;
    width: 15em;
    padding: .3em;
    transition: all 0.2s ease-in-out;

    &:focus {
        border-color: lightblue;
    }
`


export default EditableMenu
