import React from 'react'
import styled from 'styled-components'
import * as MdIcons from 'react-icons/md'

//Takes in selecting,unselecting,off,toggleSelecting,toggleUnselecting,unSelectAll
function DaySelectionPanel(props) {
    return (
        <MainContainer>
            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%',marginBottom: '10px'}}>
                Select
            </div>
            <div style={{display: 'flex',justifyContent: 'space-around',alignItems: 'center', width: '100%'}}>
                {!props.selecting ? <MdIcons.MdAddCircleOutline onClick={()=>{props.toggleSelecting(true);props.toggleUnselecting(false)}}
                    style={{cursor: 'pointer'}}/> : <MdIcons.MdAddCircle/>}
                {!props.unselecting ? <MdIcons.MdRemoveCircleOutline onClick={()=>{props.toggleSelecting(false);props.toggleUnselecting(true)}}
                    style={{cursor: 'pointer'}}/> : <MdIcons.MdRemoveCircle/>}
                <MdIcons.MdCancel onClick={()=>props.unSelectAll()} style={{cursor: 'pointer'}}/>
            </div>
        </MainContainer>
    )
}

let MainContainer = styled.div`
    border-radius: inherit;
    padding: 10px;
    border: 1px solid black;
    background-color: white;
`

export default DaySelectionPanel
