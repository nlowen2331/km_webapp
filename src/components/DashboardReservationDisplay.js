import React, { useEffect, useState } from 'react'
import GetDashboardNotifications from '../data/GetDashboardNotifications'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import * as ImIcons from 'react-icons/im'
import * as GrIcons from 'react-icons/gr'
import './DashboardReservationDisplay.css'
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import UpdateStatus from './UpdateStatus'
import statuses from './Status_Dictionary'
import PostOwnerComment from './PostOwnerComment'
import currency from 'currency.js'
import GetCalenderAndPriceInfonit from './GetCalenderAndPriceInfo'
import dayjs from 'dayjs'
import styled from 'styled-components'
import AcceptInquiry from './AcceptInquiry'

const _DEBUG = true
const TAX_RATE = 0.09

function DashboardReservationDisplay(props) {

    const [openedNotification, changeOpenNotification] = useState(null)
    const [trashConfirmVis, toggleTrashConfirmVis] = useState(false)
    const [acceptScreenVis, toggleAcceptScreenVis] = useState(false)
    const [refreshing, toggleRefreshing] = useState(false)
    const [comment, changeComment] = useState('')
    const [altCalenderVis, toggleAltCalenderVis] = useState(false)
    const [unitCalender, changeUnitCalender] = useState(null)
    const [unitPriceM, changeUnitPriceM ] = useState(null)
    const [dayChoice1, changeDayChoice1] = useState(null)
    const [dayChoice2, changeDayChoice2] = useState(null)
    const [showRejected,toggleShowRejected] =useState(false)
    const [shouldBookDates,toggleShouldBookDates] = useState(true)
    const [shouldSendAcceptance,toggleShouldSendAcceptance] = useState(true)
    const [shouldSendLease,toggleShouldSendLease] = useState(true)

    const [finalUntaxed, changeFinalUntaxed] = useState(null)
    const [finalTaxed, changeFinalTaxed] = useState(null)

    const handleOpenNotification = (e) => {
        if (e.id === openedNotification) {
            changeOpenNotification(null)
        }
        else {
            changeOpenNotification(e.id)
            resetState()
            changeComment('')
            console.log(e.id,e.status,statuses.READ)
            if (e.id && e.status<statuses.READ) { UpdateStatus(props.token, statuses.READ, e.id) }
        }
    }

    async function refresh() {
        if (refreshing) { return }
        toggleRefreshing(true)
        await GetDashboardNotifications(props.changeNotifications, props.token, true)
        changeOpenNotification(null)
        resetState()
        toggleRefreshing(false)
    }

    const resetState = () => {
        toggleTrashConfirmVis(false)
        toggleAcceptScreenVis(false)
        toggleAltCalenderVis(false)
        changeFinalTaxed(null)
        changeFinalUntaxed(null)
        changeUnitCalender(null)
        changeUnitPriceM(null)
        changeDayChoice1(null)
        changeDayChoice2(null)
    }

    const handleTrashed = (e,instance) => {
        if(instance.status<=statuses.READ){
            toggleTrashConfirmVis(true)
        }
        if (e) { e.stopPropagation() }
    }

    const handleAccepted = (e, instance) => {
        if(instance.status<=statuses.READ){
            toggleAcceptScreenVis(!acceptScreenVis)
            const priceInfo = JSON.parse(instance.priceinfo)||{}
            changeFinalUntaxed(currency(priceInfo.totalPriceUnTaxed))
            changeFinalTaxed(currency(priceInfo.totalPriceTaxed))
        }
        if (e) { e.stopPropagation() }
    }

    const handleComment = (e) => {
        changeComment(e.target.value)
    }

    const handleTrashedConfirmation = (e, id) => {
        if (id) {
            UpdateStatus(props.token, statuses.REJECTED, id).then(() => {
                refresh()
            })
        }
        if (e) { e.stopPropagation() }
    }
    const handleTrashedCancel = (e) => {
        toggleTrashConfirmVis(false)
        if (e) { e.stopPropagation() }
    }
    const handlePostComment = (id) => {
        PostOwnerComment(props.token, comment, id).then(() => GetDashboardNotifications(props.changeNotifications, props.token, true))
    }
    const handleUntaxedPriceChange = (e) => {
        changeFinalUntaxed(currency(e.target.value))
    }
    const handleTaxedPriceChange = (e) => {
        changeFinalTaxed(currency(e.target.value))
    }
    const handleChangeDates = (e, id) => {
        toggleAltCalenderVis(!altCalenderVis)
        GetCalenderAndPriceInfonit(id).then(res=>{
            if(res.OK){
                changeUnitCalender(JSON.parse(res.calender_res))
                changeUnitCalender(JSON.parse(res.price_m_res))
            }
        })
    }
    const handleDayClick = (day, { selected, disabled }) => {
        if (dayChoice1 == null) {
            changeDayChoice1(day)
        }
        else if (dayChoice2 == null) {
            changeDayChoice2(day)
        }
        else {
            changeDayChoice1(day)
            changeDayChoice2(null)
        }
    }

    const handleFinalSubmit = (e, inquiry) => {
        console.log('handle final submit')
        let s = JSON.parse(inquiry.specs)
        let arr = s.arrival
        let dep = s.departure
        if (dayChoice1 != null && dayChoice2 != null) {
            arr = dayChoice1
            dep = dayChoice2
            if (Date.parse(dayChoice2) < Date.parse(dayChoice1)) {
                arr = dayChoice2
                dep = dayChoice1
            }
        }
        let json = {
            ...s,
            finalAcceptedUntaxedPrice: finalUntaxed,
            finalAcceptedTaxedPrice: finalTaxed,
            dateOfFinalization: dayjs().format('YYYY-MM-DD'),
            acceptedArrivalDate: dayjs(arr).format('YYYY-MM-DD'),
            acceptedDepartureDate:  dayjs(dep).format('YYYY-MM-DD'),
            shouldBookDates: shouldBookDates,
            shouldSendAcceptance: shouldSendAcceptance,
            shouldSendLease: shouldSendLease,
        }

        AcceptInquiry({specs: json, id: inquiry.id},props.token).then(res=>{
            if(res.OK) {
                alert(`Successfully accepted request`)
                CleanUp()
            }
            else{
                alert(`Failed to accept request, try again later`)
            }

        })

    }

    const CleanUp = ()=>{
        changeOpenNotification(null)
        toggleShouldBookDates(true)
        toggleShouldSendAcceptance(true)
        toggleShouldSendLease(true)
        changeComment('')
        toggleAltCalenderVis(false)
        changeDayChoice1(null)
        changeDayChoice2(null)
        toggleAcceptScreenVis(false)
    }

    const today = new Date()

    useEffect(() => {
        setImmediate(() => { GetDashboardNotifications(props.changeNotifications, props.token, true) })
    }, [])

    let msgComponents
    let filteredNotifications = []

    if (props.notifications.length > 0) {

        filteredNotifications = props.notifications.filter((e) => {
            if (e.status > statuses.READ && !showRejected) { return false }
            else { return true }

        })

        msgComponents = filteredNotifications.map((e, index) => {
            let disableCalender = false
            const s = JSON.parse(e.specs)
            const priceInfo = JSON.parse(e.priceinfo)||{}
            if (s.arrival == null || s.departure == null) {
                disableCalender = true
            }

            var pastDays = Date.prototype.getRange(new Date(today.getFullYear(), today.getMonth(), 1), today)
            var finalDateInRange = new Date(today.getFullYear() + 1, today.getMonth(), today.getDate())
            var arrival = today
            var departure = today
            var departureMonth = today
            var arrivalMonth = today
            var selectedDays = []

            if (!disableCalender) {
                arrival = new Date(s.arrival.substring(0, 4), s.arrival.substring(5, 7) - 1, s.arrival.substring(8, 10))
                departure = new Date(s.departure.substring(0, 4), s.departure.substring(5, 7) - 1, s.departure.substring(8, 10))
                departureMonth = new Date(departure.getFullYear(), departure.getMonth(), 1)
                arrivalMonth = new Date(arrival.getFullYear(), arrival.getMonth(), 1)
                selectedDays = Date.prototype.getRange(arrival, departure)
                finalDateInRange = new Date(departure.getFullYear() + 1, departure.getMonth(), departure.getDate())
            }

            var disabledDates
            if (unitCalender != null) {
                disabledDates =
                    unitCalender.days.filter((e) => {
                        if (!e.available)
                            return true
                        else
                            return false
                    }).map(e => (new Date(e.date.substring(0, 4), e.date.substring(5, 7) - 1, e.date.substring(8, 10))))
            }

            var finalArrivalDate = arrival
            var finalDepartureDate = departure

            if (dayChoice1 != null && dayChoice2 != null) {
                let a = dayChoice1
                let b = dayChoice2
                if (dayChoice2 < dayChoice1) {
                    a = dayChoice2
                    b = dayChoice1
                }
                finalArrivalDate = a
                finalDepartureDate = b
            }

            const noDateConflict = () => {
                if (dayChoice1 == null || dayChoice2 == null) {
                    return true
                }
                let isChoiceLegal = true
                disabledDates.every((e) => {
                    e = Date.prototype.normalize(new Date(e.getFullYear(), e.getMonth(), e.getDate()))
                    let choice1 = Date.prototype.normalize(new Date(dayChoice1.getFullYear(), dayChoice1.getMonth(), dayChoice1.getDate()))
                    let choice2 = Date.prototype.normalize(new Date(dayChoice2.getFullYear(), dayChoice2.getMonth(), dayChoice2.getDate()))
                    if (e === choice1 || e === choice2) {
                        isChoiceLegal = false
                        return false
                    }
                    return true
                })
                let a = dayChoice1
                let b = dayChoice2
                if (Date.parse(b) < Date.parse(a)) {
                    a = dayChoice2
                    b = dayChoice1
                }
                let range = Date.prototype.getRange(a, b)
                let isRangeLegal = true
                range.every(e => {
                    e = Date.prototype.normalize(new Date(e.getFullYear(), e.getMonth(), e.getDate()))
                    disabledDates.every(e2 => {
                        e2 = Date.prototype.normalize(new Date(e2.getFullYear(), e2.getMonth(), e2.getDate()))
                        if (e === e2) {
                            isRangeLegal = false
                        }
                        return isRangeLegal
                    })
                    return isRangeLegal
                })

                return isChoiceLegal && isRangeLegal
            }

            var rentalCost = currency(priceInfo.totalRentalFee)
            var unTaxed = currency(priceInfo.totalPriceUnTaxed)
            var taxes = currency(priceInfo.totalTax)
            var taxed = currency(priceInfo.totalPriceTaxed)


            if (dayChoice1 != null && dayChoice2 != null) {
                console.log('generating rental cost')
                let rental = currency(0)

                let dateRange = Date.prototype.getRange(new Date(Date.prototype.normalize(dayChoice1)), new Date(Date.prototype.normalize(dayChoice2)))
                dateRange.forEach((e, index) => {
                    if (index === dateRange.length - 1)
                        return
                    let calenderDate = unitCalender.days.find(calender_entry => dayjs(calender_entry.date).isSame(dayjs(e),'day'))
                    let calenderDateCopy = {...calenderDate}

                    if (calenderDate != null) {

                        //Check if date has any associated price manipulation
                        if(unitPriceM!=null){
                            //iterate through each manipulator
                                unitPriceM.forEach(manipulator=>{
            
                                    if(!manipulator.enabled){
                                        return 
                                    }
                                        
                                    //iterate through each date
                                    manipulator.dates.every(man_date=>{
                                        if(dayjs(man_date).isSame(dayjs(e),'day')&&typeof manipulator.value==='number'
                                            && manipulator.value===manipulator.value){
                                            switch(manipulator.operation){
                                                case '+':
                                                    calenderDateCopy.price=(currency(calenderDateCopy.price).add(manipulator.value)).value
                                                    break;
                                                case '-':
                                                    calenderDateCopy.price=(currency(calenderDateCopy.price).subtract(manipulator.value)).value
                                                    break;
                                                case '*':
                                                    calenderDateCopy.price=(currency(calenderDateCopy.price).multiply(manipulator.value)).value
                                                    break;
                                                case '/':
                                                    calenderDateCopy.price=(currency(calenderDateCopy.price).divide(manipulator.value)).value
                                                    break;
                                                default:
                                                    console.error(`Error: Invalid operation for a manipulated date: ${manipulator.operation}`)
                                                    break;
                                            }
            
                                            return false
                                        }
                                        return true
                                    })
                                })
                            }
                        


                        rental = rental.add(calenderDateCopy.price)
                    }
                    else {
                        console.log('Could not find date in calender: ')
                        console.log(Date.prototype.normalize(e))
                    }

                })

                rentalCost=rental
                unTaxed= rental.add(currency(priceInfo.totalCleaningFee)).add(currency(priceInfo.totalPetFee))
                taxes = unTaxed.multiply(TAX_RATE)
                taxed=taxes.add(unTaxed)
            }

            const getStatusIcon =(status)=>{
                switch(status) {
                    case statuses.UNREAD:
                        return <FaIcons.FaHome className='dashboard-icon' />
                    case statuses.READ:
                        return <AiIcons.AiOutlineHome className='dashboard-icon' />
                    case statuses.REJECTED:
                        return <ImIcons.ImCross className='dashboard-icon'/>
                    case statuses.CONFIRMED:
                        return <FaIcons.FaCheck className='dashboard-icon'/>
                }
            }

            return (
                <div className='dashboard-notification' >
                    <div className='notification-unselected' onClick={() => handleOpenNotification(e)}>
                        <div className='dashboard-notification-icon'>
                            {getStatusIcon(e.status)}
                        </div>
                        <div className='dashboard-notification-id'>
                            {e.id}
                        </div>
                        <div className='dashboard-notification-unitname'>
                            {e.unitname}
                        </div>
                        <div className='dashboard-notification-price'>
                            ${priceInfo.totalPriceTaxed}
                        </div>
                        <div className='dashboard-notification-name'>
                            {s.lname}, {s.fname}
                        </div>
                        <div className='dashboard-notification-date'>
                            {Date.prototype.normalize(new Date(e.dateof), true)}
                        </div>
                    </div>
                    {openedNotification === e.id ?

                        <div className='notification-selected' onClick={resetState}>
                            <div className='notification-acceptance-form' style={{ visibility: acceptScreenVis ? 'visible' : 'hidden' }}
                                onClick={(ev) => {
                                    if (ev) { ev.stopPropagation() }
                                }}>
                                <div className='acceptance-options'>
                                    <div style={{ fontWeight: 'bold', width: '100%', justifyContent: 'center', display: 'flex', marginBottom: '.5em' }}>
                                        Accept Request
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Final Untaxed Price</p>
                                        <p>${finalUntaxed == null ? '' : finalUntaxed.toString()}
                                            {currency(finalUntaxed).value === currency(priceInfo.totalPriceUnTaxed).value ?
                                                ''
                                                :
                                                <span style={{ color: 'darksalmon', fontSize: '1.3em' }} title='This price has been modified from the original final untaxed price'>*</span>
                                            }</p>
                                        <div >
                                            $<input defaultValue={priceInfo.totalPriceUnTaxed} onChange={handleUntaxedPriceChange} />
                                        </div>
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Final Taxed Price</p>
                                        <p>${finalTaxed == null ? '' : finalTaxed.toString()}
                                            {currency(finalTaxed).value === currency(priceInfo.totalPriceTaxed).value ?
                                                ''
                                                :
                                                <span style={{ color: 'darksalmon', fontSize: '1.3em' }} title='This price has been modified from the original final taxed price'>*</span>
                                            }</p>
                                        <div>
                                            $<input defaultValue={priceInfo.totalPriceTaxed} onChange={handleTaxedPriceChange} />
                                        </div>
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Final Arrival Date</p>
                                        <p>{Date.prototype.normalize(new Date(finalArrivalDate))}
                                            {Date.prototype.normalize(finalArrivalDate) === Date.prototype.normalize(arrival) ?
                                                ''
                                                :
                                                <span style={{ color: 'darksalmon', fontSize: '1.3em' }} title='This date has been modified from the original date'>*</span>
                                            }</p>
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Final Departure Date</p>
                                        <p>{Date.prototype.normalize(new Date(finalDepartureDate))}
                                            {Date.prototype.normalize(finalDepartureDate) === Date.prototype.normalize(departure) ?
                                                ''
                                                :
                                                <span style={{ color: 'darksalmon', fontSize: '1.3em' }} title='This date has been modified from the original date'>*</span>
                                            }</p>
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Book date on BnB?</p>
                                        <CheckBox onClick={()=>{toggleShouldBookDates(!shouldBookDates)}}
                                                            >{shouldBookDates ? <FaIcons.FaCheck style={{color: 'green'}}/> : <></>}</CheckBox>
                                    </div>
                                    <div className='acceptance-options-inner'>
                                        <p>Send...</p>
                                        <div className='acc-op-in-cont'><p>Acceptance Email</p>
                                        <CheckBox onClick={()=>{toggleShouldSendAcceptance(!shouldSendAcceptance);if(shouldSendLease&&shouldSendAcceptance)toggleShouldSendLease(false)}}>
                                        {shouldSendAcceptance ? <FaIcons.FaCheck style={{color: 'green'}}/> : <></>}</CheckBox></div>
                                        
                                        <div className='acc-op-in-cont'><p style={{width: '100px'}}>Lease</p><CheckBox onClick={()=>{toggleShouldSendLease(!shouldSendLease);
                                                                                                                    if(!shouldSendLease)toggleShouldSendAcceptance(!shouldSendLease)}}>
                                        {shouldSendLease ? <FaIcons.FaCheck style={{color: 'green'}}/> : <></>}</CheckBox></div>
                                        
                                    </div>
                                    <div className='submit-row' style={{ marginTop: '.5em', marginBottom: '.5em' }}>
                                        <div className='acceptance-date-change-btn' title='Changed final dates will only appear 
                                                on this confirmation tab' onClick={(ev) => handleChangeDates(ev, e.unitid)}>
                                            Change Dates?
                                        </div>
                                        <FaIcons.FaCheck style={{ color: 'green',height: '30px',width: '30px'}} className='notifications-action-icon'
                                            onClick={(ev) => handleFinalSubmit(ev, e)} />
                                    </div>
                                    <p style={{
                                        color: 'darkred', marginBottom: '.3em', visibility: noDateConflict() ? 'hidden' : 'visible', position: noDateConflict() ? 'absolute'
                                            : 'static'
                                    }}>Warning: Dates you've selected could cause double booking</p>
                                </div>
                            </div>
                            <div className='acceptance-calender' style={{ visibility: altCalenderVis ? 'visible' : 'hidden' }}
                                onClick={(ev) => {
                                    if (ev) { ev.stopPropagation() }
                                }}>
                                {unitCalender == null ? 'Loading...' :
                                    <DayPicker fromMonth={today} toMonth={finalDateInRange} initialMonth={arrivalMonth}
                                        modifiers={{ selected: [dayChoice1, dayChoice2], disabled: [...disabledDates, ...pastDays] }} onDayClick={handleDayClick} />
                                }
                            </div>
                            <div className='acceptance-price-container' style={{ visibility: acceptScreenVis ? 'visible' : 'hidden'}}
                                onClick={(e)=>{
                                    if(e){e.stopPropagation()}
                                }}>
                                <div className='price-item' style={{border: '1px solid black',borderRadius: '10%',
                                                                    backgroundColor: 'white'}}>
                                    <div className='price-item-header'>
                                        Pricing Info
                                    </div>
                                    <div className='price-item-body'>
                                        <p>Rental Cost: ${rentalCost.toString()}</p>
                                        <p>Cleaning Fee: ${priceInfo.totalCleaningFee}</p>
                                        <p>{s.hasPets ? 'Pet Fee: $' + priceInfo.totalPetFee : ''}</p>
                                        <p>Un-Taxed Total: ${unTaxed.toString()}</p>
                                        <p>Taxes: ${taxes.toString()}</p>
                                        <p style={{
                                            borderTop: '2px solid black', paddingTop: '10px',
                                            color: 'blue', marginTop: '10px', fontWeight: 'bold'
                                        }}>
                                            Total Price: ${taxed.toString()}</p>
                                        <div className='acceptance-date-change-btn'
                                            onClick={()=>{changeFinalUntaxed(unTaxed);changeFinalTaxed(taxed)}}>
                                            Alter Final Prices
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='notification-selected-top'>
                                <div className='notification-email'>
                                    Email: {s.email}
                                </div>
                                <div className='notification-phone'>
                                    Phone: {s.phone}
                                </div>
                            </div>
                            <div className='notification-selected-middle'>
                                <div className='notification-comments'>
                                    <p>Guests expected: {s.guests}</p>
                                    <p>Unit: {e.unitid}</p>
                                    <p>{s.comments === '' ? 'Client had no comments' : s.comments}</p>
                                    <p>{s.pets > 0 && s.petDetails !== '' ? 'Pet Details: ' + s.petDetails : ''}</p>
                                    <p>Owner-side Comments:</p>
                                    <textarea className='notification-comment-input'
                                        onChange={handleComment} defaultValue={e.comment}>
                                    </textarea>
                                    Save comment
                                    <FaIcons.FaCheck className='notifications-action-icon' style={{ color: 'green' }}
                                        onClick={() => handlePostComment(e.id)} />
                                </div>
                                <div className='notification-calender'>
                                    {disableCalender ? <p>This rental request featured invalid rental dates...
                                    This is a serious error. The admin will be alerted. You can
                                    reach out to the client with the given contact info to resolve
                                    this issue.
                                    </p> : <DayPicker selectedDays={selectedDays} initialMonth={arrivalMonth}
                                            fromMonth={arrivalMonth} toMonth={departureMonth} />}
                                </div>
                            </div>
                            <div className='notification-selected-bottom'>
                                <div className='notification-reject'>
                                    <FaIcons.FaTrashAlt className='notifications-action-icon' style={{ color: e.status>statuses.READ ? 'gray': 'darkred', cursor: e.status>statuses.READ ? 'default' : 'pointer' }}
                                        onClick={(ev)=>handleTrashed(ev,e)} title="Mark this reservation as rejected"
                                    />
                                </div>
                                <div className='notification-rejection-confirmation' style={{ visibility: trashConfirmVis ? 'visible' : 'hidden' }}>
                                    <p style={{ flex: 8 }}>Are you sure you want to reject this revervation?</p>
                                    <FaIcons.FaCheck className='notifications-action-icon' style={{ color: 'green', flex: 1 }} onClick={(ev) => handleTrashedConfirmation(ev, e.id)} />
                                    <ImIcons.ImCancelCircle className='notifications-action-icon' style={{ color: 'darkred', flex: 1 }} onClick={handleTrashedCancel} />
                                </div>
                                <div className='notification-accept'>
                                    <FaIcons.FaCheck className='notifications-action-icon' style={{  color: e.status>statuses.READ ? 'gray': 'green',cursor: e.status>statuses.READ ? 'default' : 'pointer' }} onClick={(ev) => handleAccepted(ev, e)}
                                        title="Begin process to accept request" />
                                </div>
                            </div>
                            <div>
                                {s.dateOfFinalization!=null ? 'Date finalized: '+Date.prototype.normalize(new Date(s.dateOfFinalization)) : ''}
                            </div>
                        </div>

                        : ''}

                </div>
            )
        })
    }

    if (refreshing) {
        return <></>
    }

    return (
        <div className='dashboard-display-main'>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{display: 'flex',justifyContent: 'space-between',width: '25%'}}>
                    <GrIcons.GrRefresh onClick={refresh} className='notifications-action-icon' title='Refresh all reservations'
                        style={{color : refreshing ? 'blue' : 'black'}}/>
                    <FaIcons.FaRecycle  className='notifications-action-icon' onClick={()=>{toggleShowRejected(!showRejected)}}
                        title='Show all rejected and accepted reservations' style={{color : showRejected ? 'blue' : 'black'}}/>
                </div>
                
                <p >Reservations</p>
            </div>
            {filteredNotifications.length > 0 ? msgComponents : <div className='dashboard-no-content'>No new reservations</div>}
        </div>
    )
}

let CheckBox=styled.div`
    height: 25px;
    width: 25px;
    border: 1px solid black;
    padding: .2em;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`

export default DashboardReservationDisplay