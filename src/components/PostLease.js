import imconfig from '../data/config'
const config = imconfig.config

/*
    request --> {bucket,key,file}
*/

const PostLease = async (formdata,token) => {

    console.log(formdata)

    //Get server response
    let Response = await fetch(config.ip + config.port + config.route_PostLeaseToS3, {
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: formdata
    })

    let Status = Response.status

    switch(Status){
        case 200:
            return({OK: true})
        default:
            return({OK:false,msg: `UndefinedError`})
    }
}


export default PostLease