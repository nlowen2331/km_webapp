import imconfig from '../data/config'
const config = imconfig.config

/*
    The parameters must be in the form of a json object+unitid, the object must contain:
    - Date arriving
    - Date departing
    - #Guests
    - #Pets
    - Pet comments
    - First name
    - Last name
    - Phone #
    - email
    - general comments
*/

const PostInquiry = async (params) => {
    //Get server response
    let Response = await fetch(config.ip + config.port + config.route_postInq, {
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            specs: params.specs,
            priceInfo: params.priceInfo,
            id: params.id
        })
    })

    let Status = Response.status

    switch(Status){
        case 200:
            return({OK: true})
        default:
            return({OK:false})
    }
}


export default PostInquiry