import React, { useEffect,useState } from 'react'
import styled from 'styled-components'
import * as BiIcons from 'react-icons/bi'
import SelectableOption from './SelectableOption'
import * as FaIcons from 'react-icons/fa'
import * as FcIcons from 'react-icons/fc'
import dayjs from 'dayjs'

function PriceManipulatorMenu(props) {

    const [renameFormOn,toggleRenameForm] = useState(false)
    const [renameInput,changeRenameInput] = useState('')

    useEffect(()=>{
        if(props.highlighting&&props.priceManipulators!=null) {
            let Selected = props.priceManipulators.find(price_manipulator=>price_manipulator.id===props.selectedManipulator)
            if(Selected==null){
                console.log('Could not find dates of the selected manipulator')
                return
            }
            props.changeHighlightedDays(Selected.dates.map(date=>(
                new Date(dayjs(date).toString())
            )))
        }
        else if(!props.highlighting){
            props.changeHighlightedDays([])
        }
        changeRenameInput('')
        
    },[props.highlighting,props.selectedManipulator,props.selectedDays,props.changesStack])

    const AddManipulator = async ()=>{
        console.log('Add manipulator')
        //Return if editing is not allowed
        if(!props.canEdit){
            return
        }

        let Response = await props.changePriceManipulators({
            type: 'priceM',
            evName: 'addManipulator'
        })

        if(Response.OK){
            
        }

    }

    const TrashManipulator = async (id)=>{
        console.log('Trash manipulator')
        //Return if editing is not allowed
        if(!props.canEdit){
            return
        }

        let Response = await props.changePriceManipulators({
            type: 'priceM',
            evName: 'removeManipulator',
            key: id
        })

        if(Response.OK){
            props.setSelectedManipulator(null)
        }
    }

    const RenameManipulator = async (params)=>{
        console.log('Rename manipulator')
        //Return if editing is not allowed
        if(!props.canEdit){
            return
        }

        let Response = await props.changePriceManipulators({
            type: 'priceM',
            evName: 'renameManipulator',
            key: params.id,
            value: params.name
        })

        if(Response.OK){
            toggleRenameForm(false)
            changeRenameInput('')
        }
    }

    let SelectableOptions =[]

    if(props.priceManipulators!=null) {
        SelectableOptions = props.priceManipulators.map((price_manipulator,index)=>(
            <SelectableOption name={price_manipulator.name} selected={price_manipulator.id===props.selectedManipulator} id={price_manipulator.id}
                selectMe={()=>props.setSelectedManipulator(price_manipulator.id)} color={index%2===0 ? 'cornsilk' : 'azure'}
                    trashManipulator={TrashManipulator} toggleRenameMode={()=>toggleRenameForm(!renameFormOn)} renameSelected={renameFormOn}
                        changeName={changeRenameInput} renameInput={renameInput} submitRename={RenameManipulator} canEdit={props.canEdit}/>
        ))
    }
    
    return (
        <MainContainer>
            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%',marginBottom: '10px'}}>
                <div>
                    Price Manipulators
                </div>
                {props.isHidden ? 
                <BiIcons.BiDownArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                :
                <BiIcons.BiUpArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                }
            </div>
            {props.isHidden ? 
                <div>
                </div>
            :   
                <div>
                    <div style={{display: 'flex',justifyContent: 'center',width: '100%'}}>
                        <div style={{overflow: 'auto',minHeight: '5em',maxHeight: '5em',border: '1px solid black',width: '100%',position: 'relative',
                        backgroundColor: 'cornsilk'}}>
                            {SelectableOptions.length>0 ? SelectableOptions : <div></div>}
                            {props.canEdit ?
                                <FcIcons.FcPlus style={{position: 'absolute',left: '10px',
                                width: '30px',height: '30px',cursor: props.canEdit ? 'pointer' : 'default',
                                color: props.canEdit ? 'darkgreen' : 'lightgray'}} onClick={AddManipulator}/>
                                :
                                <></>
                            }   
                        </div>
                    </div>
                    <div style={{display: 'flex',justifyContent: 'flex-end',alignItems: 'center'}}>
                        <div style={{fontSize: '.7em'}}>Highlight?</div>
                        <div style={{marginLeft: '.7em'}}>
                            <CheckBox onClick={()=>props.toggleHighlighting()}>
                            {props.highlighting ? <FaIcons.FaCheck style={{width: '60%',height: '60%', color: 'darkgreen'}}/> : <div></div>}
                            </CheckBox>
                        </div>
                    </div>
                </div>
            }          
        </MainContainer>
    )
}

let MainContainer = styled.div`
    padding: 10px;
    border-radius: inherit;
    border: 1px solid black;
    background-color: white;
    padding: 10px;
`

let CheckBox = styled.div`
    height: 15px;
    width: 15px;
    border: 1px solid black;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;

`

export default PriceManipulatorMenu
