import {useSpring, animated} from 'react-spring'
import React from 'react'

function SpringComponent(props) {
  const spring = useSpring({opacity: 1, from: {opacity: 0}})
  return <animated.div style={spring}>{props.words}</animated.div>
}

export default SpringComponent