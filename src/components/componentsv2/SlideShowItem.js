import React, { useEffect, useState } from 'react'
import Loader from 'react-spinners/PuffLoader'
import { GetPictureFullKey } from '../../data/GetPicturesForUnitV2.js'
import BlurryLoadingImage from './BlurryLoadingImage.js'

function SlideShowItem({
    callback,
    picKey
}) {

    const [image,setImage] = useState(null)

    useEffect(()=>{
        GetPictureFullKey(picKey).then(res=>{
            setImage(res.img)
        })
    },[])

    return (
        <div className='slideshow-item'>
            {image ? 
                <BlurryLoadingImage image={image} onLoad={()=>{if(callback)callback()}} alt={'Property image'}
                    bgColor='lightgray' preview={'./images/alt.jpg'} imageStyleClass='slideshow-image'/>
                :
                <Loader color='teal' size={100}/>
            }
        </div>
    )
}

export default SlideShowItem
