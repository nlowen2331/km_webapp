import React, {useState,useEffect} from 'react';
import Carousel, { Dots} from '@brainhubeu/react-carousel'
import '@brainhubeu/react-carousel/lib/style.css';
import UnitCard from '../UnitCard';
import GetUnits from '../../components/GetUnits';
import Loader from 'react-spinners/PuffLoader';
import useInterval from '../../hooks/useInterval';
import {MdKeyboardArrowLeft,MdKeyboardArrowRight} from 'react-icons/md'

function CardSlideShow({index,setIndex}) {

    const [units, setUnits] = useState([])
    const [hovering,setHovering] = useState(false)
    const [loaded,toggleLoaded] = useState(false)

    useEffect(() => {
        GetUnits().then(res=>{
          if(res.OK){
            //only pass in units that meet these criteria
            setUnits(res.units.filter(unit=>unit.specifications&&unit.pictures&&unit.calender))
            toggleLoaded(true)
          }
        })
      },[])

    const handleAutoPlayStep = () =>{
        if(hovering)return
        setIndex((state)=>{
            if(state+1>=units.length){
                return 0
            }else{
                return state+1
            }
        })
    }

    const handleArrowClick = (e) =>{
        switch(e){
            case 1:
                if(index+1>=units.length){
                    setIndex(0)
                }
                else{
                    setIndex(index=>index+1)
                }
                break;
            case -1:
                if(index-1<0){
                    setIndex(units.length-1)
                }
                else{
                    setIndex(index=>index-1)
                }
                break;
        }
    }

    //useInterval(()=>{handleAutoPlayStep()},5000)

    return (
        <>  
            {units ?
            <div onMouseEnter={()=>setHovering(true)} onMouseLeave={()=>setHovering(false)} style={{position:'relative'}}>
                <Carousel value={index} onChange={(value)=>setIndex(value)}>
                    {units.map((unit,i)=><UnitCard {...unit}/>)}
                </Carousel>
                <Dots value={index} onChange={(value)=>setIndex(value)} number={units.length}/>
                <div className='arrow-container2' style={{display: loaded ? null : 'none'}}>
                    <MdKeyboardArrowLeft className='arrow' id='left' onClick={()=>handleArrowClick(-1)}/>
                    <MdKeyboardArrowRight className='arrow' id='right' onClick={()=>handleArrowClick(1)}/>
                </div>
            </div>
                :
                <div style={{height: '100%',width: '100%',display: 'flex',justifyContent:'center',alignItems:'center',minHeight:'30vh'}}>
                    <Loader/>
                </div>
            }
        </>
    )
}

export default CardSlideShow
