import dayjs from 'dayjs'
import React, { useState } from 'react'
import DayPicker from 'react-day-picker';
import { useTransition,animated } from 'react-spring';
import useClickAway from '../../hooks/useClickAway';

function DateSelectorButton({
    selection,
    title,
    pickDate,
}) {

    const [calenderOn,toggleCalender] = useState(false)
    const transition = useTransition(calenderOn,null,{
        from: {opacity: 0,position: 'absolute'},
        enter: {opacity: 1},
        leave: {opacity: 0}
    })

    const clickAwayRef = useClickAway(()=>toggleCalender(false))

    const handleDayClick = (day,{disabled,selected}) =>{
        if(disabled){
            return
        }
        else if(selected){
            pickDate(null)
        }
        else{
            pickDate(day)
        }
        toggleCalender(false)
    }

    return (
        <div ref={clickAwayRef} className='date-selector'>
            <div className='date-selector-button' onClick={()=>toggleCalender(state=>!state)}>
                <div className='date-selector-title'>
                    {title}
                </div>
                <div className='date-selector-date'>
                    {selection&&dayjs(selection).format('MM/DD/YYYY')}
                </div>
            </div>
            {transition.map(({item,key,props})=>(
                <animated.div style={props} className='date-selector-picker-container'>
                    {item&&<DayPicker fromMonth={new Date()} toMonth={dayjs().add(1,'year').toDate()}
                        disabledDays={{before: new Date()}} onDayClick={handleDayClick}
                        selectedDays={selection&&dayjs(selection).toDate()}/>}
                </animated.div>
            ))}
        </div>
    )
}

export default DateSelectorButton
