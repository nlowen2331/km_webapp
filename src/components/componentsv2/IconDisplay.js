import React from 'react'
import {FaBed,FaToilet,FaCar,FaSwimmingPool,FaSmokingBan} from 'react-icons/fa'
import {AiOutlineWifi} from 'react-icons/ai'
import {MdPets,MdSmokingRooms} from 'react-icons/md'
import {GiBigWave} from 'react-icons/gi'

function IconDisplay({rules,ammenities}) {

    return (
        <div className='icon-display-wrapper'>
            <div className='icon-display'>
                {ammenities.beds&&<div title='Beds' className='icon-container'><p>{`${ammenities.beds}`}</p><FaBed className='display-icon'/></div>}
                {ammenities.bathrooms&&<div title='Bathrooms'  className='icon-container'><p>{`${ammenities.bathrooms}`}</p><FaToilet className='display-icon'/></div>}
                {ammenities.parking&&<div  title='Parking' className='icon-container'><p>{`${ammenities.parking}`}</p><FaCar className='display-icon'/></div>}
                {ammenities.wifi&&<div  title='This unit has wifi' className='icon-container'><AiOutlineWifi className='display-icon'/></div>}
                {rules.pets&&<div  title='This unit allows pets' className='icon-container'><MdPets className='display-icon'/></div>}
                {ammenities.ocean&&<div  title='This unit is close to the ocean' className='icon-container'><GiBigWave className='display-icon'/></div>}
                {ammenities.pool&&<div  title='This unit has a pool' className='icon-container'><FaSwimmingPool className='display-icon'/></div>}
                {rules.smoking&&<div  title='No smoking' className='icon-container'><FaSmokingBan className='display-icon'/></div>}
            </div>
        </div>
    )
}

export default IconDisplay
