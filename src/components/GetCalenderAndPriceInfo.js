import imconfig from '../data/config'
const config = imconfig.config

const GetCalenderAndPriceInfo = async (id) => {
    //fetch calenders+price manipulators
    let Response 
    let error='none'
    try{
        Response= await Promise.all([GetCalender(id),GetPriceManipulators(id)])
        console.log(Response)
    }
    catch(err){
        error=err
    }
    if(error!=='none'){
        return({OK: false, msg: error})
    }

    if(Response[0].OK&&Response[1].OK){
        return {OK: true, calender_res: Response[0].res, price_m_res: Response[1].res}
    }
    else {
        return {OK: false, calender_success: Response[0].OK, price_m_success: Response[1].OK}
    }
    
}

const GetCalender = async (id)=>{
    //fetch calender
    let FetchRes = await fetch(config.ip + config.port + config.route_getCalenderByUnit + '/' + id)

    let ResBody = await FetchRes.json()

    if(FetchRes.status!==200){
        return({OK: false, ...ResBody})
    }

    return ({OK: true, ...ResBody})

}

const GetPriceManipulators = async (id)=>{
    //fetch calender
    let FetchRes = await fetch(config.ip + config.port + config.route_GetPriceManipulators + '/' + id)

    let ResBody = await FetchRes.json()

    if(FetchRes.status!==200){
        return({OK: false})
    }

    return ({OK: true, res: ResBody})

}

export default GetCalenderAndPriceInfo