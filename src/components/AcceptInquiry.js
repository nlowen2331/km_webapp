
import imconfig from '../data/config'
const config = imconfig.config

/*
    request --> {bucket,key,file}
*/

const AcceptInquiry = async (inquiry,token) => {

    console.log(inquiry)

    //Get server response
    let Response = await fetch(config.ip + config.port + config.route_AcceptInquiryV2, {
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
            id: inquiry.id,
            specs: inquiry.specs
        })
    })

    let Status = Response.status

    switch(Status){
        case 200:
            return({OK: true})
        default:
            return({OK:false,msg: `UndefinedError`})
    }
}

export default AcceptInquiry