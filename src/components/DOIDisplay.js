import React, { useEffect, useRef, useState } from 'react'
import currency from 'currency.js'
import styled from 'styled-components'
import Loader from './Loader'
import dayjs from 'dayjs'

/**
 * Sorry that this function is a mess :(
 * 
 */

//Takes in a day and displays all properties of that day
function DOIDisplay(props) {

    const [determinedManipulators,toggleDeterminedManipulators] = useState(false)
    const [manipulators,setManipulators] = useState([])
    const [success,toggleSuccess] = useState(false)
    const actualPrice = useRef(0)

    useEffect(()=>{

        new Promise((resolve,reject)=>{
            setTimeout(()=>{reject('Timeout')},4*1000)

            let FinalPrice = currency(props.day.price)
            let Manipulators = []
            
            if(props.priceManipulators!=null) {

                //Link all manipulators that include this date to the display
                props.priceManipulators.forEach(price_manipulator=>{
                    price_manipulator.dates.forEach(date=>{
                        if(dayjs(props.day.date).isSame(dayjs(date),'day')) {
                            Manipulators.push({
                                name: price_manipulator.name,
                                value: price_manipulator.value,
                                operation: price_manipulator.operation,
                                enabled: price_manipulator.enabled,
                                id: price_manipulator.id
                            })
                        }
                    })
                })

                //Find final price based on applicable manipulators
                Manipulators.forEach(manipulator=>{
                    if(!manipulator.enabled){return}
                    switch(manipulator.operation) {
                        case '+':
                            FinalPrice=FinalPrice.add(manipulator.value)
                            break;
                        case '-':
                            FinalPrice=FinalPrice.subtract(manipulator.value)
                            break;
                        case '*':
                            FinalPrice=FinalPrice.multiply(manipulator.value)
                            break;
                        case '/':
                            FinalPrice=FinalPrice.divide(manipulator.value)
                            break;
                        default:
                            throw new Error('Manipulator contained unauthorized data')
                    }
                })
            }
            setManipulators(Manipulators)
            actualPrice.current=FinalPrice.value

            resolve({OK: true})
            
        }).then((Res)=>{

            if(Res.OK) {
                toggleSuccess(true)
            }
    
            toggleDeterminedManipulators(true)

        }).catch(err=>{
            console.log(err)
        })

    },[props.day])

    return (
        <MainContainer available={props.day.available}>
            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%'}}>
                {props.day.date}
            </div>
            <Grid>
                <div>
                    Availablility:
                </div>
                <div>
                    {props.day.available ? 'Available' : 'Booked'}
                </div>
                <div>
                    Base Price:
                </div>
                <div>
                    ${currency(props.day.price).toString()}
                </div>
                <div>
                    {/*!success||currency(actualPrice.current).value===currency(props.day.price).value? 'Final Price': 'Base Price'*/}
                    Final Price:
                </div>
                <div>
                    ${currency(actualPrice.current).toString()}
                </div>
                <div>
                    Price Sync:
                </div>
                <div>
                    {props.day.syncPrice ? 'On': 'Off'}
                </div>
            </Grid>
            {/*!determinedManipulators ? 
                <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%',marginBottom: '10px'}}>
                    <Loader shrink={true}/>
                </div>
                :
                success&&currency(actualPrice.current).value!==currency(props.day.price).value ?
                    <Grid>
                        <div>
                            Actual Price:
                        </div>
                        <div>
                            ${currency(actualPrice.current).toString()}
                        </div> 
                    </Grid>
                    :
                    <div>
                        
                    </div>
            */}
            {
            //This feature shows price manipulators on the DOI
            /*success&&manipulators.length>0 ?
                    <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%'}}>
                        Manipulators
                    </div>
                :
                    <div>
                            
                    </div>
            */}
            {/*success&&manipulators.length>0 ? 
                manipulators.map((manipulator)=>(
                    <Grid>
                        <div>
                            {manipulator.name}
                        </div>
                        <div>
                            {manipulator.enabled ? 
                                'Enabled'
                                :
                                'Disabled'
                            }
                        </div>
                    </Grid>
                ))
                :
                <div>

                </div>
            */}
        </MainContainer>
    )
}

let MainContainer = styled.div`
    padding: 10px;
    background-color: ${props=>props.available ? 'aliceblue':'lightcoral'};
    border-radius: inherit;
    border: 1px solid black;
`

let Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
`

export default DOIDisplay
