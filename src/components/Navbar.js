import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link } from 'react-router-dom';
import './Navbar.css';
import { IconContext } from 'react-icons';

function Navbar() {

  return (
    <>
      <IconContext.Provider value={{ color: ' #030b1b' }}>
        <div className='navbar'>
            <Link to="/" className='navbar-title' >
                K&M Properties
            </Link>
            <ul className='nav-ul'>
                <li className='nav-item'>
                    <Link to='/' className='nav-link'>
                        Welcome
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to='/OurProperties' className='nav-link'>
                        Our Properties
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to ='/ContactUs' className='nav-link'>
                        Contact Us
                    </Link>
                </li>
            </ul>
        </div>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;