import React, { useEffect,useState } from 'react'
import styled from 'styled-components'
import Loader from './Loader'
import {PDFReader} from 'reactjs-pdf-reader'
import {useDropzone} from 'react-dropzone'
import * as HiIcons from 'react-icons/hi'
import * as FaIcons from 'react-icons/fa'
import {GrDocumentMissing} from 'react-icons/gr'

function LeaseView(props) {

    const [uploadedFileState,changeUploadedFileState] = useState('nofile'/*nofile,accept,reject */)

    const {acceptedFiles,getRootProps, fileRejections, getInputProps} = useDropzone({
        maxFiles: 1,
        accept: 'application/pdf',
    })

    const SubmitLease = async ()=>{

        console.log('Lease view submit lease')

        props.SubmitLease(props.isViewingLease).then(res=>{
            if(res){
                changeUploadedFileState('nofile')
                props.setUploadedLease(null)
                window.alert(`Lease uploaded successfully`)
            }
            else{
                alert(`Could not upload lease, try again later`)
            }
        })

    }

    useEffect(()=>{
        if(acceptedFiles.length>0){
            props.setUploadedLease(Object.assign(acceptedFiles[0],{preview: URL.createObjectURL(acceptedFiles[0])}))
            changeUploadedFileState('accept')
        }
        else if(fileRejections.length>0){
            changeUploadedFileState('reject')
        }
        else{
            changeUploadedFileState('nofile')
        }

        return (()=>{acceptedFiles.forEach(file=>URL.revokeObjectURL(file.preview))})

    },[acceptedFiles,fileRejections])

    let FileStateIcon
    switch(uploadedFileState){
        case 'accept':
            FileStateIcon=<HiIcons.HiOutlineCheckCircle style={{height: '50px',width: '50px'}}/>
            break;
        case 'reject':
            FileStateIcon=<HiIcons.HiOutlineXCircle style={{height: '50px',width: '50px'}}/>
            break;
        default:
            FileStateIcon=<HiIcons.HiOutlineDocumentAdd style={{height: '50px',width: '50px'}}/>
            break;
    }

    let Dropzone = (
        <DropContainer {...getRootProps({className: 'Dropzone'})} style={{height: '100px',width: '200px',backgroundColor:'white'}}>
            <input {...getInputProps()}/>
            {FileStateIcon}
        </DropContainer>
    )

    return (
        <BlackOut onClick={()=>props.StopViewingLease()}>
            <div onClick={(e)=>{e.stopPropagation()}}>
                {props.isLeaseLoaded?
                    props.lease==null ? 
                        <div style={{height: '700px',display: 'flex',
                            flexDirection: 'column', justifyContent:'center',alignItems:'center',
                            backgroundColor: 'white',padding: '3em'}}>
                            <div style={{fontSize: '1.8em'}}>
                                Lease unavailable or missing
                            </div>
                            <GrDocumentMissing style={{height: '100px',width: '100px'}}/>
                        </div>
                        :
                        <div style={{overflowY: 'auto',height: '700px'}}>
                            {props.uploadedLease==null ? 
                                <PDFReader url={props.lease} key={props.lease}/>
                                :
                                <PDFReader url={props.uploadedLease.preview} key={props.uploadedLease.preview}/>
                            }
                            
                        </div>
                    :
                    <Loader/>
                }
                <div style={{display: 'flex', justifyContent: 'flex-start'}}>
                    {Dropzone}
                    <Button style={{display: props.uploadedLease==null ? 'none':'flex'}} onClick={()=>SubmitLease()}>
                        <HiIcons.HiUpload style={{height: '50px',width: '50px',color: 'lightblue'}}/>
                    </Button>
                    <Button style={{display: props.uploadedLease==null ? 'none':'flex'}} onClick={()=>{props.setUploadedLease(null);changeUploadedFileState('nofile')}}>
                        <FaIcons.FaTrashAlt style={{height: '50px',width: '50px',color: 'darkred'}}/>
                    </Button>
                </div>  
            </div>
        </BlackOut>
    )
}

let BlackOut = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    z-index: 200;
    background-color: rgba(0,0,0,0.8);
    display: flex;
    justify-content: center;
    align-items: center;
`

let DropContainer = styled.div`
    margin-top: 15px;
    background-color: white;
    z-index: 201;
    height: 100px;
    width: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
`

let Button = styled.div`
    margin-top: 15px;
    margin-left: 15px;
    cursor: ${props=>props.off ? 'initial': 'pointer'};
    background-color: ${props=>props.color||'white'};
    padding: 10px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
`

let FileDisplay = styled.div`
    margin-top: 15px;
    margin-left: 15px;
    background-color: ${props=>props.color||'white'};
    padding: 10px;
    height: 50px;
    padding:10px;
    max-width: 220px;
    overflow: hidden;
    display: flex;
    align-items: center;
    font-size: small;
`

export default LeaseView
