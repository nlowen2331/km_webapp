import React, { useEffect, useState } from 'react'
import './UnitCard.css'
import {getBulletedAmmenitiesList,getBulletedRulesList} from '../components/ListConverter'
import { Link } from 'react-router-dom'
import {GetPicture} from '../data/GetPicturesForUnitV2'
import Loader from 'react-spinners/PuffLoader'
import TextLoader from 'react-spinners/BeatLoader'
import BlurryLoadingImage from './componentsv2/BlurryLoadingImage'
import IconDisplay from './componentsv2/IconDisplay'
import useMedia from '../hooks/useMedia'

function UnitCard({unitid, unitname, specifications,minprice,maxprice,pictures,onPictureFullyLoaded,longdesc}) {

    if(!specifications)specifications='{}'

    const s = JSON.parse(specifications)

    const [cover,setCover] = useState([])
    const [isLoaded,setIsLoaded] = useState(false)
    const charCount = useMedia(['(max-width: 525px)','(max-width: 600px)'],[100,200],400)

    useEffect(()=> {
        let p = pictures&&JSON.parse(pictures)
        let cover_pic = {}
        
        if(p){
            cover_pic = p.find(pic=>pic.isCover===true)

            if(cover_pic){
                GetPicture(unitid,cover_pic.picture_id).then(res=>{
                    setCover(res.img)
                    setIsLoaded(true)
                }).catch(err=>{
                    //Set picture to problem picture
                    setIsLoaded(true)
                })
            }    
        }

    },[])

    
    const ShortenDescripiton = (desc) =>{
        let shortend = desc
        if(desc.length>charCount){
            shortend=desc.substring(0,charCount)
            shortend=shortend+'...'
        }
        return shortend
    }

    return (
        <>
            <div className='unit-card-container'>
                <div className='unit-card-graphic-side'>
                    <div className='unit-card-city'>
                        {s.city&&s.state ?
                            <p>{`${s.city}, ${s.state}`}</p>
                                :
                            <TextLoader/>
                        }
                    </div>
                    <div className='loader-pic-container'>  
                        <Link className = 'unit-link' id='unit-link-for-pic' to={unitid ? `/${unitid}` : '/'}>
                            {   isLoaded ?
                                <BlurryLoadingImage image={cover} alt='Picture of a rental property'
                                bgColor='lightgray' preview={'./images/alt.jpg'} divStyleClass='unit-card-pic-container'
                                    imageStyleClass='unit-card-pic-img' onLoad={()=>{if(onPictureFullyLoaded)onPictureFullyLoaded()}}/>
                                    :
                                <Loader color='aqua'/>
                                }
                        </Link>
                    </div>
                    
                </div>
                <div className='unit-card-text-side'>
                    <div className='unit-card-text-body'>
                        <div className='unit-card-title-row'>
                            <div className='unit-card-title'>
                                <Link className = 'unit-link' to={unitid ? `/${unitid}` : '/'}>{unitname ? <p>{unitname}</p> : <TextLoader/> }</Link>  
                            </div>
                            <div className='unit-card-price'>
                                {minprice&&maxprice ? <p>${minprice}-{maxprice} a night</p> : <TextLoader/>}
                            </div>
                        </div>
                        <div className='unit-card-desc'>
                            <p>
                                {ShortenDescripiton(longdesc)}{' '}<Link className='unit-link' style={{color:'lightblue'}} to={unitid ? `/${unitid}` : '/'}>read more</Link>
                            </p>
                        </div>   
                        <IconDisplay rules={s.rules} ammenities={s.ammenities}/>
                    </div>
                </div>
            </div>
        </>
    )
}



export default UnitCard
