const define = (name, value) =>{
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("UNREAD", 0);
define("READ", 1);
define("CONFIRMED", 2);
define("REJECTED", 3);
define("TRASHED",4)
