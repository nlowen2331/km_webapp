import React, { useEffect, useState } from 'react'
import GetDashboardNotificationsMessages from '../data/GetDashboardNotificationsMessages'
import * as FaIcons from 'react-icons/fa'
import * as ImIcons from 'react-icons/im'
import * as GrIcons from 'react-icons/gr'
import './DashboardMessageDisplay.css'
import 'react-day-picker/lib/style.css';
import UpdateStatusMessages from './UpdateStatusMessages'
import statuses from './Status_Dictionary'

const _DEBUG = true


function DashboardMessageDisplay(props) {

    const [openedNotification, changeOpenNotification] = useState(null)
    const [trashConfirmVis, toggleTrashConfirmVis] = useState(false)
    const [refreshing, toggleRefreshing] = useState(false)

    const handleOpenNotification = (id) => {
        if (id === openedNotification) {
            changeOpenNotification(null)
        }
        else {
            changeOpenNotification(id)
            resetState()
            if (id) { UpdateStatusMessages(props.token, statuses.READ, id) }
        }
    }

    async function refresh() {
        if (refreshing) { return }
        toggleRefreshing(true)
        await GetDashboardNotificationsMessages(props.changeNotifications, props.token, true)
        changeOpenNotification(null)
        resetState()
        toggleRefreshing(false)
    }

    const resetState = () => {
        toggleTrashConfirmVis(false)
    }

    const handleTrashed = (e) => {
        toggleTrashConfirmVis(true)
        if (e) { e.stopPropagation() }
    }

    const handleTrashedConfirmation = (e, id) => {
        if (id) {
            UpdateStatusMessages(props.token, statuses.REJECTED, id).then(() => {
                refresh()
            })
        }
        if (e) { e.stopPropagation() }
    }
    const handleTrashedCancel = (e) => {
        toggleTrashConfirmVis(false)
        if (e) { e.stopPropagation() }
    }

    useEffect(() => {
        setImmediate(() => { GetDashboardNotificationsMessages(props.changeNotifications, props.token, true) })
    }, [])

    let msgComponents
    let filteredNotifications =[]

    if (props.notifications.length > 0) {

        filteredNotifications = props.notifications.filter((e) => {
            if (e.status > statuses.READ) { return false }
            else { return true }

        })

        msgComponents = filteredNotifications.map((e, index) => {

            return (
                <div className='dashboard-notification' >
                    <div className='notification-unselected' onClick={() => handleOpenNotification(e.messageid)}>
                        <div className='dashboard-notification-icon'>
                            {e.status > 0 ? <FaIcons.FaRegEnvelopeOpen className='dashboard-icon' /> :
                                <FaIcons.FaEnvelope className='dashboard-icon' />}
                        </div>
                        <div className='dashboard-notification-id'>
                            {e.messageid}
                        </div>
                        <div className='dashboard-notification-name'>
                            {e.lastname}, {e.firstname}
                        </div>
                        <div className='dashboard-notification-date'>
                            {Date.prototype.normalize(new Date(e.dateof), true)}
                        </div>
                    </div>
                    {openedNotification === e.messageid ?

                        <div className='notification-selected' onClick={resetState}>
                            <div className='notification-selected-top'>
                                <div className='notification-email'>
                                    Email: {e.email}
                                </div>
                                <div className='notification-phone'>
                                    Phone: {e.phone}
                                </div>
                            </div>
                            <div className='notification-selected-middle-messages'>
                                <div className='notification-comments-messages'>
                                    <p>{e.message === '' ? 'This is a blank message, there may have been an error accepting the client\'s msg' : e.message}</p>
                                </div>
                            </div>
                            <div className='notification-selected-bottom'>
                                <div className='notification-reject'>
                                    <FaIcons.FaTrashAlt className='notifications-action-icon' style={{ color: 'darkred' }}
                                        onClick={handleTrashed} title="Erase this message (recoverable)"
                                    />
                                </div>
                                <div className='notification-rejection-confirmation' style={{ visibility: trashConfirmVis ? 'visible' : 'hidden' }}>
                                    <p style={{ flex: 8 }}>Are you sure you want to trash this message?</p>
                                    <FaIcons.FaCheck className='notifications-action-icon' style={{ color: 'green', flex: 1 }} onClick={(ev) => handleTrashedConfirmation(ev, e.messageid)} />
                                    <ImIcons.ImCancelCircle className='notifications-action-icon' style={{ color: 'darkred', flex: 1 }} onClick={handleTrashedCancel} />
                                </div>
                            </div>
                        </div>

                        : ''}

                </div>
            )
        })
    }

    if (refreshing) {
        return <></>
    }

    return (
        <div className='dashboard-display-main'>
            <div style={{display: 'flex',justifyContent: 'space-between'}}>
                <GrIcons.GrRefresh onClick={refresh} className='notifications-action-icon' />
                <p>Messages</p>
            </div>
            
            {filteredNotifications.length > 0 ? msgComponents : <div className='dashboard-no-content'>No new messages</div>}
        </div>
    )
}

export default DashboardMessageDisplay