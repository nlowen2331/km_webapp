import React,{useEffect} from 'react'
import './UserSettings.css'
import GetOwner from './GetOwner'


function UserSettings(props) {

    useEffect(()=>{
        GetOwner(props.token,props.changeUserInfo)
    },[])

    return (
        <div className='user-settings-main'>
            <div className='user-settings-inner'>
                <div className='user-inner-title'>
                    <h3>Info</h3>
                </div> 
                <p>Username: <span style={{color: 'darkblue'}}>{props.userName}</span></p>
                <p>Email: <span style={{color: 'darkblue'}}>{props.email}</span></p>
            </div>
            <div className='user-settings-inner'>
                <div className='user-inner-title'>
                    <h3>Options</h3>
                </div>
                <p>Change Username</p>
                <p>Change Password</p>
                <div className='user-log-out' onClick={props.clearToken}>
                    Logout
                </div>
            </div>
        </div>
    )
}

export default UserSettings
