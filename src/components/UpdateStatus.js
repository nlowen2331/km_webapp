import imconfig from '../data/config'
const config = imconfig.config

const UpdateStatus= (token,status,inqid) => fetch(config.ip + config.port + config.route_updateStatus, {
    method: "post",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +token
    },
    body: JSON.stringify({
        inqid: inqid,
        status: status
    })
}).then((res) => {
    console.log(`using inqid: ${inqid} and using status: ${status}`)
    res.json().then(json => {
        if(json!==null && json.error_code==0){
            console.log(`No errors, updated status for inq id: ${inqid}`)
        }
        else{
            console.log('There was an error updating status')
            console.log(json.error_code)
        }
    });
}).catch(err => {
    console.log('There was an error making the request to the server')
    console.log(err)
});


export default UpdateStatus