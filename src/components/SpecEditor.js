import React, {useState} from 'react'
import * as FaIcons from 'react-icons/fa'
import styled from 'styled-components'
import currency from 'currency.js'

/**
 * Props
 * specs --> a badly formatted list of specifications for the unit
 * isEditing --> when this is true the user may edit
 * 
 * Accepts: sleeps,bathrooms,bedrooms,parking,pets,smoking,pet_fee,cleaning_fee
 * 
 * specs will never be null
 *
 */

 const DEFAULT_WIDTH='50px'
 const small_text_width='60px'
 const text_width='130px'
 const max_input_length=10

function SpecEditor(props) {

    const handleSubmit = (className)=>{
        console.log('Handle changed')
        console.log(className)
        switch(className) {
            case 'guests':
                props.changeSpecs({evName: 'changeGuests',value: guests,type:'specs'})
                break
            case 'sleeps':
                props.changeSpecs({evName: 'changeSleeps',value: sleeps,type:'specs'})
                break
            case 'bathrooms':
                props.changeSpecs({evName: 'changeBathrooms',value: bathrooms,type:'specs'})
                break
            case 'bedrooms':
                props.changeSpecs({evName: 'changeBedrooms',value: bedrooms,type:'specs'})
                break
            case 'parking':
                props.changeSpecs({evName: 'changeParking',value: parking,type:'specs'})
                break
            case 'petFee':
                props.changeSpecs({evName: 'changePetFee',value: currency(petFee).toString(),type:'specs'})
                break
            case 'cleaningFee':
                props.changeSpecs({evName: 'changeCleaningFee',value: currency(cleaningFee).toString(),type:'specs'})
                break
            case 'pets':
                props.changeSpecs({evName: 'changeAllowPets',value: pets,type:'specs'})
                break
            case 'smoking':
                props.changeSpecs({evName: 'changeAllowSmoking',value: smoking,type:'specs'})
                break
            default:
                throw new Error(`Uknown className: ${className} in handleSubmit in SpecEditor`)
        }
    }

    const [guests,setGuests]= useState(props.specs.guests)
    const [sleeps,setSleeps]= useState(props.specs.ammenities.sleeps)
    const [bathrooms,setBathrooms]= useState(props.specs.ammenities.bathrooms)
    const [bedrooms,setBedrooms]= useState(props.specs.ammenities.bedrooms)
    const [parking,setParking]= useState(props.specs.ammenities.parking)
    const [pets,setPets]= useState(props.specs.rules.pets)
    const [smoking,setSmoking]= useState(props.specs.rules.smoking)
    const [petFee,setPetFee]= useState(props.specs.pet_fee)
    const [cleaningFee,setCleaningFee]= useState(props.specs.cleaning_fee)

    return (
        <EditorContainer>
            <EditorOption>
                <GridText width={text_width}>Guests?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.guests===guests ? 
                    <div>
                        {props.specs.guests}
                    </div>
                    :
                    <div>
                        {guests}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setGuests(e.target.value)} value={guests} disabled={!props.isEditing}/>
                <Button onClick={()=>handleSubmit('guests')} off={!props.isEditing} className={"guests"}>
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Sleeps?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.ammenities.sleeps===sleeps ? 
                    <div>
                        {props.specs.ammenities.sleeps}
                    </div>
                    :
                    <div>
                        {sleeps}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setSleeps(e.target.value)} value={sleeps} disabled={!props.isEditing}/>
                <Button onClick={()=>handleSubmit('sleeps')} off={!props.isEditing} className={"sleeps"}>
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Bathrooms?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.ammenities.bathrooms===bathrooms ? 
                    <div>
                        {props.specs.ammenities.bathrooms}
                    </div>
                    :
                    <div>
                        {bathrooms}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input  maxLength={max_input_length} onChange={(e)=>setBathrooms(e.target.value)} value={bathrooms} disabled={!props.isEditing}/>
                <Button onClick={()=>handleSubmit('bathrooms')} off={!props.isEditing} className={"bathrooms"}>
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Bedrooms?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.ammenities.bedrooms===bedrooms ? 
                    <div>
                        {props.specs.ammenities.bedrooms}
                    </div>
                    :
                    <div>
                        {bedrooms}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setBedrooms(e.target.value)} value={bedrooms} disabled={!props.isEditing}/>
                <Button onClick={()=>handleSubmit('bedrooms')} off={!props.isEditing} className={"bedrooms"}>
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Parking?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.ammenities.parking===parking ? 
                    <div>
                        {props.specs.ammenities.parking}
                    </div>
                    :
                    <div>
                        {parking}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setParking(e.target.value)} value={parking} disabled={!props.isEditing}/>
                <Button onClick={()=>handleSubmit('parking')} off={!props.isEditing} className={"parking"}>
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Allow Pets?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.rules.pets===pets ? 
                    <div>
                        {props.specs.rules.pets}
                    </div>
                    :
                    <div>
                        {pets}<span style={{color: 'red'}}>*</span>
                    </div>
                }

                </GridText>
                <Button onClick={()=>setPets(pets==='true' ? 'false':'true')} off={!props.isEditing}>
                    {pets==='true' ? 'Disallow' : 'Allow'}
                </Button>
                <Button onClick={()=>handleSubmit('pets')} off={!props.isEditing} className={"pets"} >
                    Update
                </Button>
            </EditorOption>
            <EditorOption off={!pets}>
                <GridText width={text_width}>Pet Fee?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.pet_fee===petFee ? 
                    <div>
                        ${currency(props.specs.pet_fee).toString()}
                    </div>
                    :
                    <div>
                        ${currency(petFee).toString()}<span style={{color: 'red'}}>*</span>
                    </div>
                }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setPetFee(e.target.value)} value={petFee} disabled={!pets||!props.isEditing}/>
                {pets ? 
                    <Button onClick={()=>handleSubmit('petFee')} off={!props.isEditing} className={"petFee"} >
                        Update
                    </Button>
                    :
                    <Button off={true}>

                    </Button>
                }
                
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Ban Smoking?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.rules.smoking===smoking ? 
                    <div>
                        {props.specs.rules.smoking}
                    </div>
                    :
                    <div>
                        {smoking}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Button onClick={()=>setSmoking(smoking==='true' ? 'false':'true')} off={!props.isEditing}>
                    {smoking==='true' ? 'Allow' : 'Ban'}
                </Button>
                <Button onClick={()=>handleSubmit('smoking')} off={!props.isEditing} className={"smoking"} >
                    Update
                </Button>
            </EditorOption>
            <EditorOption>
                <GridText width={text_width}>Cleaning Fee?</GridText>
                <GridText width={small_text_width}>
                    {props.specs.cleaning_fee===cleaningFee ? 
                    <div>
                        ${currency(props.specs.cleaning_fee).toString()}
                    </div>
                    :
                    <div>
                        ${currency(cleaningFee).toString()}<span style={{color: 'red'}}>*</span>
                    </div>
                    }
                </GridText>
                <Input maxLength={max_input_length} onChange={(e)=>setCleaningFee(e.target.value)} value={cleaningFee} disabled={!pets||!props.isEditing}/>
                <Button onClick={()=>handleSubmit('cleaningFee')} off={!props.isEditing} className={"cleaningFee"} >
                    Update
                </Button>
            </EditorOption>
            
        </EditorContainer>
    )
}

let EditorContainer = styled.div`
    background-color: white;
    border: 1px solid black;
    border-radius: 20px;
    width: 100%;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr;
    padding: 10px;
    grid-gap: 1em;
    z-index: 1;
    justify-content: flex-start;
`

let EditorOption = styled.div`
    display: grid;
    grid-auto-flow: column;
    grid-auto-columns: 1fr;
    grid-gap: 1em;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    min-width: 440px;
    color: ${props=>props.off ? 'gray': 'black'};
`

let Button = styled.div`
    cursor: ${props=>props.off ? 'initial': 'pointer'};
    background-color: ${props=>props.off ? 'lightgray' : 'lightblue'};
    border: 1px solid black;
    color: white;
    padding: .2em;
    border-radius: 10px;
    min-width: 70px;
    max-width: 70px;
    overflow: hidden;
    display: flex;
    justify-content: center;
`

let Input = styled.input`

    padding: .3em;

    &:focus{
        outline: solid 3px blueviolet

    }
`

let GridText = styled.div`
    white-space: 'nowrap';
    min-width: ${props=>props.width||DEFAULT_WIDTH};
    max-width: ${props=>props.width||DEFAULT_WIDTH};
    overflow: hidden
`

export default SpecEditor
