import React from 'react'
import styled from 'styled-components'
import * as FaIcons from 'react-icons/fa'
import * as TiIcons from 'react-icons/ti'

function SelectableOption(props) {
    return (
        <MainContainer color={props.selected ? 'aquamarine' : props.color} canClick={!props.selected}
            onClick={()=>props.selectMe()}>
            {props.renameSelected&&props.selected  ?
                <Input value={props.renameInput} onChange={(e)=>props.changeName(e.target.value)}
                    maxLength={20}/>
            : props.name }
            {props.canEdit ?
                <div style={{position: 'absolute',top: '4px',right: '10px',width: '40px',
                            display: 'flex',justifyContent: 'space-around',alignItems: 'center'}}>
                    {
                        props.renameSelected&&props.selected ?
                        <FaIcons.FaCheck style={{height: '18px',width: '18px',cursor:'pointer'}}
                            onClick={()=>props.submitRename({id: props.id, name: props.renameInput})}/>
                        :
                        <FaIcons.FaPencilAlt onClick={()=>{
                            if(props.canEdit){props.toggleRenameMode(true)}
                            }
                        } 
                        style={{height: '15px',width: '15px',cursor: props.canEdit ? 'pointer' : 'default'}}/>
                    }
                    {

                        props.renameSelected&&props.selected  ?
                        <TiIcons.TiCancel onClick={()=>{props.toggleRenameMode(false);props.changeName('')}}
                        style={{height: '20px',width: '20px',cursor:'pointer'}}/>
                        :
                        <FaIcons.FaTrashAlt onClick={()=>{
                            if(props.canEdit){props.trashManipulator(props.id)}
                            }
                        } 
                        style={{height: '15px',width: '15px',cursor: props.canEdit ? 'pointer' : 'default'}}/>
                    }
                    
                </div>
                :
                <></>
            }
            
        </MainContainer>
    )
}

let MainContainer = styled.div`

    background-color: ${props=>props.color};
    cursor: ${props=>props.canClick ? 'pointer' : 'default' };
    width: 100%;
    padding-left: 10px;
    padding-right: 10px;
    display: flex;
    align-items: center;
    position: relative;

`

let Input = styled.input`

    padding: .3em;
    max-width: 200px;
    min-width: 200px;

    &:focus{
        outline: solid 3px blueviolet;

    }
`

export default SelectableOption
