import React, { useEffect, useRef, useState } from 'react'
import './EditableUnit.css'
import * as BiIcons from 'react-icons/bi'
import * as FaIcons from 'react-icons/fa'
import * as VscIcons from 'react-icons/vsc'
import styled from 'styled-components'
import { GetPicture, GetPictureFullKey } from '../data/GetPicturesForUnitV2'
import Loader from './Loader'
import EditablePicture from './EditablePicture'
import EditableMenu from './EditableMenu'
import SpecEditor from './SpecEditor'
import EditableDays from './EditableDays'
import dayjs from 'dayjs'
import currency from 'currency.js'
import EditableName from './EditableName'
import DescriptionEditor from './DescriptionEditor'

/**
 * 
 * @param {unit,claimEditing, isEditing} props 
 * 
 * isEditing lets the Unit know if it is in editing mode or not
 * 
 * claimEditing is a function that makes a call to the parent component to attempt to toggle editing mode (pass any falsy value to release claim)
 * 
 */

function EditableUnit(props) {

    const [pictures, setPictures] = useState([])
    const [isUpdatingPictures, changeIsUpdatingPictures] = useState(false)
    const [isUpdatingSpecs, changeIsUpdatingSpecs] = useState(false)
    const [isUpdatingMisc, changeIsUpdatingMisc] = useState(false)
    const [changesStack, updatechangesStack] = useState([])
    //const [isUpdatingChanges, toggleIsUpdatingChanges] = useState(false)
    const [picturesHidden, toggleHidePictures] = useState(true)
    const [specsHidden, toggleHideSpecs] = useState(true)
    const [specs, setSpecs] = useState(JSON.parse(props.unit.specifications))
    //local calender state
    const [calender,setCalender] = useState(JSON.parse(props.unit.calender).days)
    const [isUpdatingCalender,changeIsUpdatingCalender] = useState(false)
    const [calenderHidden, toggleCalenderHidden] = useState(true)
    const [priceManipulators,setPriceManipulators] = useState(JSON.parse(props.unit.priceManipulators))
    const [isUpdatingPriceM,changeIsUpdatingPriceM] = useState(false)
    const [selectedDays,setSelectedDays] = useState([])
    //name editing
    const [unitName,changeUnitName] = useState(props.unit.unitname)
    const [descHidden,toggleDescHidden] = useState(true)
    const [unitDescription,changeUnitDescription] = useState(props.unit.longdesc)

    //Calls 'setPictures' 
    //takes in a command/event like {changeIsHidden: true} and a 'key' for the picture which requests it
    const UpdatePicture = async (params) => {
        console.log('called update picture')
        //Make sure changes are not blocked
        if (isUpdatingPictures) {
            props.pushMessage(`Slow down! Still working on previous changes...`)
            return
        }

        //Block new changes to picture while updating
        changeIsUpdatingPictures(true)
        //Append changes to pictures
        await new Promise((resolve, __) => {
            //Record a succesful change
            let changeSuccessful = true
            //Find index of property that needs changing
            let index = pictures.findIndex(picture => picture.picture_id === params.key)
            //Copy array
            let UpdatedPictures = pictures.slice()
            //Update the copied array with new information
            let prevCoverIndex
            switch (params.evName) {
                case `changeIsHidden`:
                    if (UpdatedPictures[index].isCover) {
                        props.pushMessage(`Cannot hide the cover picture`)
                        changeSuccessful = false
                        break;
                    }
                    UpdatedPictures[index] = { ...UpdatedPictures[index], isHidden: params.value }
                    break;
                case `changeIsCover`:
                    if (!params.value) {
                        props.pushMessage(`Must have 1 valid cover picture`)
                        changeSuccessful = false
                        break;
                    }
                    if (UpdatedPictures[index].isHidden) {
                        props.pushMessage(`Cannot choose a hidden picture as the cover picture`)
                        changeSuccessful = false
                        break;
                    }
                    //Find previous cover for undo purposes also clear prev cover holder

                    UpdatedPictures.forEach((picture, index) => {
                        if (picture.isCover) {
                            prevCoverIndex = index
                            picture.isCover = false
                        }
                    })
                    UpdatedPictures[index] = { ...UpdatedPictures[index], isCover: params.value }
                    break;
                default:
                    throw new Error(`Unknown evName given to UpdatePicture: ${params.evName}`)
            }

            //Set state and resolve

            if (changeSuccessful) {
                setPictures(UpdatedPictures)
                updatechangesStack(prevArray => [...prevArray, { ...params, prevCoverIndex: prevCoverIndex }])
            }
            else {
                props.pushMessage(`Unexpected error... Try again`)
            }
            resolve()
        })
        //Allow new changes
        changeIsUpdatingPictures(false)
    }

    const Finalize = async () => {
        console.log('Saving changes to server')
        if (changesStack.length < 1) {
            return
        }
        //Determine change types
        let PictureChanges = []
        let SpecsChanges = []
        let CalenderChanges = []
        let PriceMChanges= []
        let MiscChanges = []

        changesStack.forEach(change => {
            if (change.type === 'specs') {
                SpecsChanges.push(change)
            }
            else if (change.type === 'picture') {
                PictureChanges.push(change)
            }
            else if(change.type ==='calender') {
                CalenderChanges.push(change)
            }
            else if(change.type==='priceM') {
                PriceMChanges.push(change)
            }
            else if(change.type==='misc'){
                MiscChanges.push(change)
            }
            else{
                throw new Error(`Could not identify change type for finalization ${change.type}`)
            }

        })

        //Test for success
        let PictureChangesSuccess=PictureChanges.length<1
        let SpecsChangesSuccess=SpecsChanges.length<1
        let CalenderChangesSuccess=CalenderChanges.length<1
        let PriceMChangesSuccess=PriceMChanges.length<1
        let MiscChangesSuccess=MiscChanges.length<1

        //Check if any picture changes should be pushed
        if (!PictureChangesSuccess) {
            //No changes can be made while posting to server
            changeIsUpdatingPictures(true)
            //Send the array of changes you made to postChanges function and await result
            let response = await props.postChanges({ changeStack: PictureChanges, evName: `postPictures` })
            if(response.msg === 'OK'){
                PictureChangesSuccess=true
            }
            changeIsUpdatingPictures(false)
        }

        //Check for specs changes
        if (!SpecsChangesSuccess) {
            //No changes can be made while posting to server
            changeIsUpdatingSpecs(true)
            //Send the array of changes you made to postChanges function and await result
            let response = await props.postChanges({ changeStack: SpecsChanges, evName: `postSpecs` })
            if (response.msg === 'OK') {
                SpecsChangesSuccess=true
            }
            changeIsUpdatingSpecs(false)
        }

        //Check for calender changes
        if (!CalenderChangesSuccess) {
            //No changes can be made while posting to server
            changeIsUpdatingSpecs(true)
            //Send the array of changes you made to postChanges function and await result
            let response = await props.postChanges({ changeStack: CalenderChanges, evName: `postCalender` })
            if (response.msg === 'OK') {
                CalenderChangesSuccess=true
            }
            changeIsUpdatingSpecs(false)
        }

        //Check for price manipulator changes
        if (!PriceMChangesSuccess) {
            //No changes can be made while posting to server
            changeIsUpdatingSpecs(true)
            //Send the array of changes you made to postChanges function and await result
            let response = await props.postChanges({ changeStack: PriceMChanges, evName: `postPriceM` })
            if (response.msg === 'OK') {
                PriceMChangesSuccess=true
            }
            changeIsUpdatingSpecs(false)
        }

        //Check for misc changes
        if (!MiscChangesSuccess) {
            //No changes can be made while posting to server
            changeIsUpdatingMisc(true)
            //Send the array of changes you made to postChanges function and await result
            let response = await props.postChanges({ changeStack: MiscChanges, evName: `postMisc` })
            if (response.msg === 'OK') {
                MiscChangesSuccess=true
            }
            changeIsUpdatingMisc(false)
        }

        if(SpecsChangesSuccess&&PictureChangesSuccess&&
            CalenderChangesSuccess&&PriceMChangesSuccess&&MiscChangesSuccess) {
            props.pushMessage(`Saved!`)
            updatechangesStack([])
            setSelectedDays([])
            props.claimEditing(false)
        }
        else {
            props.pushMessage(`Some changes did not save... This could be a server error`)
        }
    }

    const Undo = async () => {
        console.log('Called undo')
        //End if there are no changes
        if (changesStack.length < 1) {
            return
        }

        //Temp variables
        let TargetIndex
        let error='none'

        //Copy changes array
        let UpdatedchangesStack = changesStack.slice()

        //Pop last change
        let lastChange = UpdatedchangesStack.pop()
        console.log(lastChange)

        //Get event type
        switch (lastChange.type) {
            case 'misc':
                //Make sure misc changes aren't blocked
                if (isUpdatingMisc) {
                    props.pushMessage(`Slow down! Still working on previous changes...`)
                    return
                }

                //Block new misc changes
                changeIsUpdatingSpecs(true)

                switch(lastChange.evName) {
                    case 'changeUnitName':
                        changeUnitName(lastChange.prevValue)
                        break;
                    case 'changeUnitDesc':
                        changeUnitDescription(lastChange.prevValue)
                        break;
                    default:
                        throw new Error(`Unknown evName given to Undo: ${lastChange.evName}`)
                }

                updatechangesStack(UpdatedchangesStack)
                changeIsUpdatingMisc(false)

                break;
            case 'picture':
                //Make sure picture changes are not blocked
                if (isUpdatingPictures) {
                    props.pushMessage(`Slow down! Still working on previous changes...`)
                    return
                }
                //Block new changes to picture while updating
                changeIsUpdatingPictures(true)

                //Find index that needs to change
                let index = pictures.findIndex(picture => picture.picture_id === lastChange.key)

                //Copy array
                let UpdatedPictures = pictures.slice()

                switch (lastChange.evName) {
                    case `changeIsHidden`:
                        UpdatedPictures[index] = { ...UpdatedPictures[index], isHidden: !lastChange.value }
                        break;
                    case `changeIsCover`:
                        UpdatedPictures[lastChange.prevCoverIndex] = { ...UpdatedPictures[lastChange.prevCoverIndex], isCover: true }
                        UpdatedPictures[index] = { ...UpdatedPictures[index], isCover: false }
                        break;
                    default:
                        throw new Error(`Unknown evName given to Undo: ${lastChange.evName}`)
                }

                //Finalize
                updatechangesStack(UpdatedchangesStack)
                setPictures(UpdatedPictures)
                changeIsUpdatingPictures(false)

                break;
            case 'specs':
                //Make sure specs changes are not blocked
                if (isUpdatingSpecs) {
                    props.pushMessage(`Slow down! Still working on previous changes...`)
                    return
                }

                //just in case
                if (specs == null) {
                    props.pushMessage(`An unexpected error occured... try again`)
                    return
                }
                //Block new changes to picture while updating
                changeIsUpdatingSpecs(true)

                //Copy specs
                let UpdatedSpecs = { ...specs }

                switch (lastChange.evName) {
                    case 'changeGuests':
                        UpdatedSpecs.guests = lastChange.prevValue
                        break
                    case 'changeSleeps':
                        UpdatedSpecs.ammenities.sleeps = lastChange.prevValue
                        break
                    case 'changeBathrooms':
                        UpdatedSpecs.ammenities.bathrooms = lastChange.prevValue
                        break
                    case 'changeBedrooms':
                        UpdatedSpecs.ammenities.bedrooms = lastChange.prevValue
                        break
                    case 'changeParking':
                        UpdatedSpecs.ammenities.parking = lastChange.prevValue
                        break
                    case 'changePetFee':
                        UpdatedSpecs.pet_fee = lastChange.prevValue
                        break
                    case 'changeCleaningFee':
                        UpdatedSpecs.cleaning_fee = lastChange.prevValue
                        break
                    case 'changeAllowPets':
                        UpdatedSpecs.rules.pets = lastChange.prevValue
                        break
                    case 'changeAllowSmoking':
                        UpdatedSpecs.rules.smoking = lastChange.prevValue
                        break
                    default:
                        throw new Error(`Uknown evName: ${lastChange.evName} in handleSubmit in SpecEditor`)
                }

                //Finalize changes
                updatechangesStack(UpdatedchangesStack)
                setSpecs(UpdatedSpecs)

                //Allow new changes
                changeIsUpdatingSpecs(false)

                break;
            case 'calender':

                //Check if update is already occuring
                if (isUpdatingCalender) {
                    props.pushMessage(`Slow down! Still working on previous changes...`)
                    return
                }

                //block changes
                changeIsUpdatingCalender(true)

                //Copy array of objects
                let UpdatedCalender = [...calender]

                //For each day in lastChange daysAffected, get the index of those dates

                //record all the indicies where changes will need to occur
                let Indicies = []

                lastChange.daysAffected.forEach(affected_day=>{
                    Indicies.push(calender.findIndex(calender_day=>dayjs(calender_day.date).isSame(dayjs(affected_day),'day')))
                })

                console.log(Indicies)

                //Confirm all affected dates were matched
                if(Indicies.length===lastChange.daysAffected.length) {
                    //Iterate through all dates that need changing and apply the change
                    Indicies.forEach(index=>{
                        //Check for the previous value that matches the date of the current index
                        lastChange.prevValue.every(previous_value=>{
                            //If a match is found replace existing values with previous values
                            if(dayjs(UpdatedCalender[index].date).isSame(dayjs(previous_value.date),'day')) {
                                UpdatedCalender[index]={...UpdatedCalender,...previous_value}
                                return false
                            }
                            return true
                        })
                    })
                }

                //Finalize changes
                updatechangesStack(UpdatedchangesStack)
                setCalender(UpdatedCalender)

                //Allow new changes
                changeIsUpdatingCalender(false)
                break;
                case 'priceM':
                    //Check if update is already occuring
                    if (isUpdatingPriceM) {
                        props.pushMessage(`Slow down! Still working on previous changes...`)
                        return
                    }

                    //block changes
                    changeIsUpdatingPriceM(true)

                    //Copy array of objects
                    let UpdatedPriceM=[...priceManipulators]

                    switch(lastChange.evName){
                        case 'addDates':
                        case 'removeDates':
                            //find the correct index based on last change
                            TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===lastChange.key)
                            if(TargetIndex===-1) {
                                error='Could not identify selected manipulator'
                                break;
                            }
                            UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex],dates: [...lastChange.prevValue]}
                            break;
                        case 'changeManipulatorValues':
                            UpdatedPriceM[lastChange.prevValue.id]={...lastChange.prevValue}
                            break;
                        case 'renameManipulator':
                            TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===lastChange.key)
                            UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex], name: lastChange.prevValue}
                            break;
                        case 'removeManipulator':
                            //Shit check, make sure the name and id are unique throw error if not
                            let throwError= !UpdatedPriceM.every(price_m=>{
                                if(lastChange.prevValue.name===price_m.name||
                                    lastChange.prevValue.id===price_m.id){
                                        return false
                                    }
                                    return true
                            })
                            if(throwError){
                                throw new Error('Invalid data would be introduced if action were allowed')
                            }
                            UpdatedPriceM=[...UpdatedPriceM,lastChange.prevValue]
                            break;
                        case 'addManipulator':
                            UpdatedPriceM=[...UpdatedPriceM.filter(price_m=>(
                                price_m.id!==lastChange.prevValue.id
                            ))]
                            break;
                        default:
                            throw new Error(`Unrecognized evName when undoing price manipulator data: ${lastChange.evName}`)

                    }

                    //Finalize changes
                    updatechangesStack(UpdatedchangesStack)
                    console.log(UpdatedPriceM)
                    setPriceManipulators(UpdatedPriceM)
                    //Allow new changes
                    changeIsUpdatingPriceM(false)

                    break;
            default:
                throw new Error(`Uknown event type in Undo: ${lastChange.type}`)
        }



    }

    const UpdateSpecs = async (params) => {

        console.log('Called update specs')
        console.log(params)

        //Check if update is already occuring
        if (isUpdatingSpecs) {
            props.pushMessage(`Slow down! Still working on previous changes...`)
            return
        }

        //precaution because i don't understand why it's null in the first place
        if (specs == null) {
            props.pushMessage(`Unexpected error... Try again`)
            return
        }

        //block changes
        changeIsUpdatingSpecs(true)
        //Update
        await new Promise((resolve, __) => {
            //Record a succesful change
            let changeSuccessful = false

            //copy specs
            let UpdatedSpecs = { ...specs }

            //Save last value
            let previousValue

            //Create regexps
            let BetweenRangeExp = /^[0-9]{1,2}-[0-9]{1,2}$/
            let SmallNumExp = /^[0-9]{1,2}$/

            //choose action based on evName
            switch (params.evName) {
                case `changeGuests`:
                    if (SmallNumExp.test(params.value)) {
                        previousValue = UpdatedSpecs.guests
                        UpdatedSpecs.guests = params.value
                        changeSuccessful = true
                    }
                    break;
                case `changeSleeps`:
                    if (SmallNumExp.test(params.value) || BetweenRangeExp.test(params.value)) {
                        previousValue = UpdatedSpecs.ammenities.sleeps
                        UpdatedSpecs.ammenities.sleeps = params.value
                        changeSuccessful = true
                    }
                    break;
                case `changeBathrooms`:
                    if (SmallNumExp.test(params.value)) {
                        previousValue = UpdatedSpecs.ammenities.bathrooms
                        UpdatedSpecs.ammenities.bathrooms = params.value
                        changeSuccessful = true
                    }
                    break;
                case `changeBedrooms`:
                    if (SmallNumExp.test(params.value)) {
                        previousValue = UpdatedSpecs.ammenities.bedrooms
                        UpdatedSpecs.ammenities.bedrooms = params.value
                        changeSuccessful = true
                    }
                    break;
                case `changeParking`:
                    if (SmallNumExp.test(params.value)) {
                        previousValue = UpdatedSpecs.ammenities.parking
                        UpdatedSpecs.ammenities.parking = params.value
                        changeSuccessful = true
                    }
                    break;
                case `changePetFee`:
                    previousValue = UpdatedSpecs.pet_fee
                    UpdatedSpecs.pet_fee = params.value
                    changeSuccessful = true
                    break;
                case `changeCleaningFee`:
                    previousValue = UpdatedSpecs.cleaning_fee
                    UpdatedSpecs.cleaning_fee = params.value
                    changeSuccessful = true
                    break;
                case `changeAllowPets`:
                    previousValue = UpdatedSpecs.rules.pets
                    UpdatedSpecs.rules.pets = params.value
                    changeSuccessful = true
                    break;
                case `changeAllowSmoking`:
                    previousValue = UpdatedSpecs.rules.smoking
                    UpdatedSpecs.rules.smoking = params.value
                    changeSuccessful = true
                    break;
                default:
                    throw new Error(`Unknown evName given to UpdatePicture: ${params.evName}`)
            }

            //Set state and resolve

            if (changeSuccessful) {
                setSpecs(UpdatedSpecs)
                updatechangesStack(prevArray => [...prevArray, { ...params, prevValue: previousValue }])
            }
            else {
                props.pushMessage(`Unexpected value: ${params.value}, try a different value`)
            }
            resolve()
        })
        //Allow new changes
        changeIsUpdatingSpecs(false)


    }

    //Return if successful
    const UpdatePriceManipulators = async (params)=>{

        console.log('Update Price manipulators')
        console.log(params)
    
        if (isUpdatingPriceM) {
            props.pushMessage(`Slow down! Still working on previous changes...`)
            return({OK: false})
        }

        //block changes
        changeIsUpdatingPriceM(true)

        //Record a succesful change
        let changeSuccessful = false

        await new Promise((resolve,__)=>{
            //Copy existing price manipulators
            let UpdatedPriceM = [...priceManipulators]

            //Error recording
            let error ='Unexpected error ocurred'

            //Prev values
            let previousValue

            //For manipulation
            let TargetIndex
            
            //change strategy based on evName
            switch(params.evName) {
                case 'addDates':
                    TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===params.key)
                    if(TargetIndex===-1) {
                        error='Could not identify selected manipulator'
                        break;
                    }
                    previousValue=UpdatedPriceM[TargetIndex].dates
                    if(params.dates==null||params.dates.length<1){
                        error='No changes detected'
                        break;
                    }
                    UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex],dates: [...UpdatedPriceM[TargetIndex].dates,...params.dates]}
                    changeSuccessful=true
                    break;
                case 'removeDates':
                    TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===params.key)
                    if(TargetIndex===-1) {
                        error='Could not identify selected manipulator'
                        break;
                    }
                    previousValue=UpdatedPriceM[TargetIndex].dates
                    if(params.dates==null||params.dates.length<1){
                        error='No changes detected'
                        break;
                    }
                    UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex],dates: [...UpdatedPriceM[TargetIndex].dates.filter(
                        checking_date=>(
                            params.dates.findIndex(to_remove_date=>dayjs(to_remove_date).isSame(dayjs(checking_date),'day'))===-1
                        )
                    )]}
                    changeSuccessful=true
                    break;
                case 'changeManipulatorValues':
                    TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===params.key)
                    if(TargetIndex===-1) {
                        error='Could not identify selected manipulator'
                        break;
                    }
                    previousValue={...UpdatedPriceM[TargetIndex]}
                    if(params.toChange==null||params.toChange.length<1) {
                        error='No changes detected'
                        break;
                    }
                    changeSuccessful=true
                    params.toChange.forEach(change=>{
                        switch(change) {
                            case 'enabled':
                                UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex],enabled: params.value.enabled}
                                break;
                            case 'value':
                                if(typeof params.value.value!=='number'||params.value.value!==params.value.value){
                                    error='Value could not be accepted'
                                    changeSuccessful=false
                                    break;
                                }
                                UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex], operation: params.value.operation,
                                                                value: params.value.value}
                                break;
                            default:
                                throw new Error(`Unknown change supplied to changeManipulator: ${change}`)
                        }
                    })
                    break;
                case 'renameManipulator':
                    //Find index of obj to manipulate
                    TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===params.key)
                    if(TargetIndex===-1) {
                        error='Could not identify selected manipulator'
                        break;
                    }
                    if(params.value===''){
                        error='Invalid name'
                        break;
                    }
                    //Check if name exists already
                    if(UpdatedPriceM.findIndex(manipulator=>manipulator.name===params.value)>-1){
                        error='Sorry, that name is already in use'
                        break;
                    }
                    previousValue=UpdatedPriceM[TargetIndex].name
                    UpdatedPriceM[TargetIndex]={...UpdatedPriceM[TargetIndex],name: params.value}
                    changeSuccessful=true
                    break;
                case 'removeManipulator':
                    //Find index of obj to manipulate
                    TargetIndex = UpdatedPriceM.findIndex(manipulator=>manipulator.id===params.key)
                    if(TargetIndex===-1) {
                        error='Could not identify selected manipulator'
                        break;
                    }
                    previousValue={...UpdatedPriceM[TargetIndex]}
                    UpdatedPriceM.splice(TargetIndex,1)
                    changeSuccessful=true
                    break;
                case 'addManipulator':
                    //Find suitable ID
                    let max = 0
                    let name= 'NewManipulator'
                    let initError='none'
                    //If price manipulators exist, do some extra work
                    if(UpdatedPriceM.length>0) {
                        //Find highest id
                        max = -1
                        UpdatedPriceM.forEach(manipulator=>{
                            if(manipulator.id>max) {
                                max=manipulator.id
                            }
                        })
                        //If no id was set, shoot an error
                        if(max===-1) {
                            error='Could not find suitable identification for the new manipulator'
                            break;
                        }
                        //Safely assign name
                        let iterator=1
                        while(true){
                            if(UpdatedPriceM.findIndex(manipulator=>manipulator.name===name)===-1){
                                break;
                            }

                            //break at arbitrary value to stop infinite loops
                            if(iterator>10) {
                                initError='Too many manipulators with default naming'
                                break;
                            }

                            name=`NewManipulator${iterator}`
                            iterator++
                            
                        }
                        
                    } 
                    if(initError!='none') {
                        error=initError
                        break;
                    }
                    //Create template for new manipulator
                    let Template = {
                        id: max+1,
                        name: name,
                        dates: [],
                        value: 1,
                        enabled: false,
                        operation: '*'
                    }
                    previousValue=Template
                    UpdatedPriceM=[...UpdatedPriceM,Template]
                    changeSuccessful=true
                    break;
                default:
                    throw new Error(`Unknown evName in attempt to update Price Manipulators: ${params.evName}`)
            }

            console.log('Updated Price Manipulators')
            console.log(UpdatedPriceM)

            if (changeSuccessful) {
                setPriceManipulators(UpdatedPriceM)
                updatechangesStack(prevArray => [...prevArray, { ...params, prevValue: previousValue}])
            }
            else {
                props.pushMessage(error)
            }
            resolve()
            
        })

        changeIsUpdatingPriceM(false)
        return({OK: changeSuccessful})
    }

    const UpdateCalender = async (params)=>{
        
        console.log('Called update calenders')
        console.log(params)

        //Check if update is already occuring
        if (isUpdatingCalender) {
            props.pushMessage(`Slow down! Still working on previous changes...`)
            return
        }

        //block changes
        changeIsUpdatingCalender(true)

        //Record a succesful change
        let changeSuccessful = false

        //Update
        await new Promise((resolve, __) => {

            //Copy array of objects
            let UpdatedCalender = [...calender]
            //Store the state of all calender dates that will be changed
            let PreviousValues = []

            //for each affected day get the index of that corresponding day
            //in the calender array

            //record all the indicies where changes will need to occur
            let Indicies = []

            params.daysAffected.forEach(affected_day=>{
                Indicies.push(calender.findIndex(calender_day=>dayjs(calender_day.date).isSame(dayjs(affected_day),'day')))
            })

            console.log(Indicies)

            //Confirm all affected dates were matched
            if(Indicies.length===params.daysAffected.length) {
                //Iterate through all dates that need changing and apply the change
                Indicies.forEach(index=>{
                    console.log('Change -->')
                    console.log(UpdatedCalender[index])
                    //push the object before updating it
                    PreviousValues.push(UpdatedCalender[index])
                    //apply price sync change
                    switch(params.value.priceSyncSetting) {
                        case 'sync':
                            UpdatedCalender[index]={...UpdatedCalender[index], syncPrice: true}
                            console.log('Synced price')
                            break;
                        case 'nosync':
                            UpdatedCalender[index]={...UpdatedCalender[index], syncPrice: false}
                            console.log('Unsynced price')
                            break;
                        default:
                            break;
                    }
                    //apply price change
                    if(params.value.priceChange && !UpdatedCalender[index].syncPrice) {
                        UpdatedCalender[index]={...UpdatedCalender[index],price: currency(params.value.price).toString()}
                    }
                    console.log('Finish -->')
                    console.log(UpdatedCalender[index])
                })

                changeSuccessful=true
            }

            console.log(UpdatedCalender)


            //Set state and resolve

            if (changeSuccessful) {
                setCalender(UpdatedCalender)
                updatechangesStack(prevArray => [...prevArray, { ...params, prevValue: PreviousValues }])
            }
            else {
                props.pushMessage(`An unexpected error occured, could not make changes to the calender`)
            }
            resolve()
        })
        //Allow new changes
        changeIsUpdatingCalender(false)
        return(changeSuccessful)

    }

    const UpdateMisc = async (params)=>{
        console.log('Called update misc')
        console.log(params)

        //Check if update is already occuring
        if (isUpdatingMisc) {
            props.pushMessage(`Slow down! Still working on previous changes...`)
            return
        }
        //block changes
        changeIsUpdatingMisc(true)
        //Update
        await new Promise((resolve, __) => {
            //Save last value
            let previousValue=''

            //choose action based on evName
            switch (params.evName) {
                case 'changeUnitName':
                    previousValue=unitName
                    changeUnitName(params.value)
                    break;
                case 'changeUnitDesc':
                    previousValue=unitDescription
                    changeUnitDescription(params.value)
                    break;
                default:
                    throw new Error(`Unknown evName given to UpdateMisc: ${params.evName}`)
            }

            //Set state and resolve
            updatechangesStack(prevArray => [...prevArray, { ...params, prevValue: previousValue }])
            resolve()
        })
        //Allow new changes
        changeIsUpdatingMisc(false)
        return(true)

    }

    const DiscardChanges = async () => {
        console.log('Called discard changes')
        updatechangesStack([])
        setSelectedDays([])
        changeUnitName(props.unit.unitname)
        changeUnitDescription(props.unit.longdesc)
        props.claimEditing(false)
    }

    useEffect(() => {
        if (props.unit.pictures == null) {
            return
        }
        let Pictures = JSON.parse(props.unit.pictures)
        Pictures.forEach(pic => {
            GetPictureFullKey(`${props.unit.unitid}/${pic.picture_id}`).then(res => {
                setPictures(pictures => [...pictures, { ...pic, img: res.img }])
            }).catch(err => {
                console.log(err)
                setPictures(pictures => [...pictures, { ...pic, img: `images/alt.jpg` }])
            })
        })

    }, [])


    return (
        <div className='editable-unit-main' style={{ position: 'relative',minWidth: '1260px' }}>
            <div style={{
                position: 'absolute', height: `80%`, width: `95%`, display: 'flex', justifyContent: 'flex-end',
                pointerEvents: 'none'
            }}>
                <StickyUndo isHidden={!props.isEditing} onClick={Undo}>
                    <FaIcons.FaUndoAlt style={{
                        color: changesStack.length > 0 ? 'blue' : 'black', height: '70%', width: '70%',
                        cursor: changesStack.length > 0 ? 'pointer' : 'initial',
                        pointerEvents: 'auto'
                    }} />
                </StickyUndo>
            </div>
            <div className='editable-unit-main-item'>
                <div className="row" style={{ marginBottom: '20px' }}>
                    <div>
                        <EditableName name={unitName||props.unit.unitname} 
                           saveName={UpdateMisc} isEditing={props.isEditing}/>
                    </div>
                    <div style={{
                        width: '200px',height: '60px', display: 'flex', justifyContent: props.isEditing ? 'space-between' : 'center',
                        padding: '10px', position: 'relative'
                    }}>
                        {props.isEditing ?
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%', position: 'sticky', top: '0px' }}>
                                <span onClick={Undo}><FaIcons.FaUndoAlt style={{
                                    color: changesStack.length > 0 ? 'blue' : 'black',
                                    cursor: changesStack.length > 0 ? 'pointer' : 'initial',
                                    height: '40px',width: '40px'
                                }} /></span>
                                <span onClick={Finalize}><FaIcons.FaCheck style={{
                                    color: changesStack.length > 0 ? 'green' : 'black',
                                    cursor: changesStack.length > 0 ? 'pointer' : 'initial',
                                    height: '40px',width: '40px'
                                }} /></span>
                                <span onClick={DiscardChanges}><FaIcons.FaTrashAlt style={{ color: 'darkred', cursor: 'pointer',
                                    height: '40px',width: '40px' }} /></span>
                            </div>
                            :
                            <span onClick={() => props.claimEditing(true)} style={{ cursor: 'pointer' }}>Edit<FaIcons.FaPencilAlt style={{marginLeft: '10px'}}/></span>
                        }

                    </div>
                </div>
                <div className='editable-unit-inner-menu-item'>
                    <div>
                        Pictures
                </div>
                    <BiIcons.BiDownArrow style={{ cursor: 'pointer',transition: '500ms', transform: 
                        !picturesHidden ?'initial' : 'rotate(180deg)' }} onClick={() => toggleHidePictures(!picturesHidden)} />
                </div>
                <div className='row' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                    <ImgContainer isHidden={picturesHidden}>
                        {pictures.map(pic => (
                            <EditablePicture picture={pic} updatePicture={UpdatePicture} canEdit={props.isEditing && !isUpdatingPictures} />
                        ))}
                    </ImgContainer>
                </div>
            </div>
            <div className='editable-unit-inner-menu-item'>
                <div>
                    Specifications
                </div>
                <BiIcons.BiDownArrow style={{ cursor: 'pointer',transition: '500ms', transform: 
                        !specsHidden ?'initial' : 'rotate(180deg)' }} onClick={() => toggleHideSpecs(!specsHidden)} />
                {specs != null && !specsHidden ? <SpecEditor specs={specs} changeSpecs={(ev) => UpdateSpecs(ev)} isEditing={props.isEditing} /> : <></>}
            </div>
           
            <div className='editable-unit-inner-menu-item'>
                <div>
                   Calenders/Prices
                </div>
                <BiIcons.BiDownArrow style={{ cursor: 'pointer',transition: '500ms', transform: 
                        !calenderHidden ?'initial' : 'rotate(180deg)' }} onClick={() => toggleCalenderHidden(!calenderHidden)}/>
                {calender != null && !calenderHidden ? <EditableDays days={calender} isEditing={props.isEditing}
                    changeCalender={UpdateCalender} priceManipulators={priceManipulators}
                    changePriceManipulators={UpdatePriceManipulators} changesStack={changesStack}
                    selectedDays={selectedDays} setSelectedDays={setSelectedDays}/> : <></>}
            </div>
            <div className='editable-unit-inner-menu-item'>
                <div>
                    Description
                </div>
                <BiIcons.BiDownArrow style={{ cursor: 'pointer',transition: '500ms', transform: 
                    !descHidden ?'initial' : 'rotate(180deg)' }} onClick={() => toggleDescHidden(!descHidden)}/>
                {!descHidden ? <DescriptionEditor description={unitDescription}
                                    isEditing={props.isEditing}  saveDescription={UpdateMisc}/> : <></>}
            </div>
            <div style={{marginTop: '30px'}}></div>
            <div>
                <Button onClick={()=>props.ViewLease(props.unit.unitid)}>
                    View Lease
                </Button>
            </div>
        </div>
    )
}


let ImgContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr;
    justify-items: center;
    grid-gap: 1em;
    opacity: ${props => props.isHidden ? 0 : 1};
    height: ${props => props.isHidden ? '0px' : 'auto'};
    z-index: ${props => props.isHidden ? -1 : 1};

`;

let StickyUndo = styled.div`
    opacity: ${props => props.isHidden ? 0 : 0.2};
    border: 1px solid black;
    border-radius: 50%;
    transition: all 0.2s ease-in-out;
    height: ${props => props.isHidden ? '0px' : '75px'};
    width: ${props => props.isHidden ? '0px' : '75px'};
    background-color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    position: sticky;
    top: 40px;
    z-index: 10;

    &:hover {
        opacity: ${props => props.isHidden ? 0 : 0.9};
        height: 80px;
        width: 80px;
    }

`
let Button = styled.div`
    cursor: ${props=>props.off ? 'initial': 'pointer'};
    background-color: ${props=>props.off ? 'lightgray' : 'lightblue'};
    border: 1px solid black;
    color: white;
    padding: .2em;
    border-radius: 10px;
    min-width: 150px;
    max-width: 150px;
    overflow: hidden;
    display: flex;
    justify-content: center;
`



export default EditableUnit
