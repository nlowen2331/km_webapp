
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

//includes current date and stop date

Date.prototype.getRange = (startDate,stopDate) => {
    let dateArray = new Array();
    let currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate()));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

//get months in range

Date.prototype.getRangeOfMonths = (startDate,stopDate,debug) => {
    let dateArray = new Array();
    let currentDate = startDate;
    let _currentDate=currentDate;
    if(debug) {
        console.log('--Debug getRangeofMonths--')
        console.log('Date passed to function '+startDate)
        console.log('Reference to date passed '+_currentDate)
        
        console.log(Object.prototype.toString.call(_currentDate))
    }
    while (_currentDate <= stopDate) {
        dateArray.push(new Date (_currentDate.getFullYear(),_currentDate.getMonth(),1));
        _currentDate = new Date(currentDate.getFullYear(),_currentDate.setMonth(_currentDate.getMonth()+1),1)
    }
    if(debug) {
        console.log('--After while loop--')
        console.log('Date passed to function '+startDate)
        console.log('Reference to date passed '+_currentDate)
        console.log('Return value '+dateArray)
    }
    return dateArray;
}

//takes in a date, gives this format YYYY-MM-DD-HH:MM:SS

Date.prototype.normalize = (date,time) => {
    if(time) {return (date.getFullYear() + '-'+ ('0'+(date.getMonth()+1)).slice(-2) 
                        + '-' + ('0'+(date.getDate())).slice(-2)+' | '+('0'+date.getHours()).slice(-2)+':'
                        +('0'+date.getMinutes()).slice(-2)+':'+('0'+date.getSeconds()).slice(-2))}
    return (date.getFullYear() + '-'+ ('0'+(date.getMonth()+1)).slice(-2) + '-' + ('0'+(date.getDate())).slice(-2))
}