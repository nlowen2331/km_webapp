import imconfig from '../data/config'
const config = imconfig.config

/*
    The parameters must be in the form of a json object, the object must contain:
    - First name
    - Last name
    - Phone #
    - email
    - message
*/

const accept = {
    msg: 'Your Message Has Been Sent! We will get back to you as soon as possible.',
    accept : true
}

const reject = {   
    msg: 'Your Message was not processed. Please try again, or contact us directly instead.',
    accept: false
}


const PostMessage = (params, returnMessage,returnToggle,resetState) => fetch(config.ip + config.port + config.route_postMessage, {
    method: "post",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        fname: params.fname,
        lname: params.lname,
        phone: params.phone,
        email:params.email,
        message: params.message
    })
}).then((res) => {
    res.json().then(json => {
        if(json!==null && json.error_code==0){
            returnMessage(accept)
            returnToggle(true)
            setTimeout(()=>{returnToggle(false)},8000)
            resetState(true)
        }
        else{
            console.log('There was an error posting message')
            console.log(json.error_code)
            returnMessage(reject)
            returnToggle(true)
            setTimeout(()=>{returnToggle(false)},8000)
        }
    });
}).catch(err => {
    returnMessage(reject)
    returnToggle(true)
    setTimeout(()=>{returnToggle(false)},8000)
    console.log('There was an error posting message')
    console.log(err)
});


export default PostMessage