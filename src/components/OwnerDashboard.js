import React, { useEffect, useState } from 'react'
import * as FaIcons from 'react-icons/fa'
import DashboardMessageDisplay from './DashboardMessageDisplay'
import DashboardReservationDisplay from './DashboardReservationDisplay'
import './OwnerDashboard.css'
import UserSettings from './UserSettings'
import CheckResNotifications from './CheckResNotifications'
import CheckMsgNotifications from './CheckMsgNotifications'
import GetUnits from './GetUnits'
import OwnerUnitsView from './OwnerUnitsView'
import styled from 'styled-components'

function OwnerDashboard(props) {

    const [reservations,changeReservations] = useState([])
    const [messages,changeMessages] = useState([])
    const [userInfo,changeUserInfo] = useState({})
    const [displayOption,changeDisplayOption] =useState('res')
    const [unreadReservations,changeUnreadReservations] =useState(0)
    const [unreadMessages,changeUnreadMessages] =useState(0)
    const [unitData, changeUnitData] = useState([])
    const [unitsLoaded,toggleUnitsLoaded] = useState(false)
    const [unitDisplayMessageQueue, setUnitDisplayMessageQueue] = useState([])
    const [isDisplayOn, toggleDisplay] = useState(false)

    useEffect(()=>{
        CheckResNotifications(props.token).then((res)=>{
            changeUnreadReservations(res)
        }).catch(err=>{
            console.log(err)
        })
        CheckMsgNotifications(props.token).then((res)=>{
            changeUnreadMessages(res)
        }).catch(err=>{
            console.log(err)
        })
        GetUnits().then(res=>{
            if(res.OK){
                changeUnitData(res.units)
            }
            toggleUnitsLoaded(true)
        }).catch(err=>{
            console.log(err)
        })
    },[displayOption])

    useEffect(()=>{
        if(unitDisplayMessageQueue.length<1) {
            return
        }
        else if(!isDisplayOn) {
            toggleDisplay(true)
            setTimeout(displayTimeout,3000/**3 seconds */)
        } 

    },[unitDisplayMessageQueue])

    const UpdateUnits = ()=>{
        GetUnits().then(res=>{
            if(res.OK){
                changeUnitData(res.units)
            }
            toggleUnitsLoaded(true)
        }).catch(err=>{
            console.log(err)
        })
    }

    const PushDisplayMessage = (msg)=>{
        setUnitDisplayMessageQueue(prevState=>[...prevState,msg])
    }

    const displayTimeout = ()=>{
        toggleDisplay(false)
        let newArray = unitDisplayMessageQueue
        newArray.shift()
        setUnitDisplayMessageQueue(newArray)
    }

    const resOp = <DashboardReservationDisplay token={props.token} notifications={reservations} changeNotifications={(n)=>changeReservations(n)}/>
    const msgOp =<DashboardMessageDisplay token={props.token} notifications={messages} changeNotifications={(n)=>changeMessages(n)}/>
    const userOp= <UserSettings token={props.token} userName={userInfo.username} email={userInfo.email} changeUserInfo={(n)=>changeUserInfo(n)}
        clearToken={props.clearToken}/>

    const mainDisplay = () =>{
        switch(displayOption) {
            case 'res':
                return resOp
            case 'msg':
                return msgOp
            case 'user':
                return userOp
        }
    }


    return (
        <div className='dashboard-main' style={{backgroundImage: `url(./images/cover_york_small.jpg)`}}>
            <div className='dashboard-header'>
                <div title="Logged In As..." className='dashboard-user-id'>
                    <div className='dashboard-name'>
                        {props.name}
                    </div>  
                </div>
                <div title="Reservation Requests" className='dashboard-reservation-icon'>
                    <FaIcons.FaHome className='dashboard-header-icon' onClick={()=>{changeDisplayOption('res')}}
                    style={{color: displayOption==='res' ? 'blue' : 'black' }}/>
                    <div className='unread-notification-symbol' style={{visibility: unreadReservations===0 ? 'hidden' : 'visible'}}>{unreadReservations}</div>
                </div>
                <div title="Messages" className='dashboard-messages-icon'>
                    <FaIcons.FaEnvelope className='dashboard-header-icon' onClick={()=>{changeDisplayOption('msg')}}
                        style={{color: displayOption==='msg' ? 'blue' : 'black' }}/>
                    <div className='unread-notification-symbol' style={{visibility: unreadMessages===0 ? 'hidden' : 'visible'}}>{unreadMessages}</div>
                </div>
                <div title="User Settings" className='dashboard-settings-icon'>
                    <FaIcons.FaUserCog className='dashboard-header-icon' onClick={()=>{changeDisplayOption('user')}}
                        style={{color: displayOption==='user' ? 'blue' : 'black' }}/>
                </div>
            </div>
            <div className='dashboard-main-display'>
                {
                    mainDisplay()
                }
            </div>
            <div className='dashboard-lower-menu' style={{position: 'relative'}}>
                <DisplayMessage isDisplayOn={isDisplayOn}>{unitDisplayMessageQueue.length>0 ? unitDisplayMessageQueue[unitDisplayMessageQueue.length-1] : ''}</DisplayMessage>
                <OwnerUnitsView token={props.token} units={unitData} 
                    unitsLoaded = {unitsLoaded} changeUnitData={(n)=>changeUnitData(n)}
                    pushMessage={(msg)=>PushDisplayMessage(msg)} UpdateUnits={UpdateUnits}/>
            </div>
        </div>
    )
}

const DisplayMessage = styled.div`
    position: absolute;
    border: 1px solid black;
    border-radius: inherit;
    width: 80%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: yellowgreen;
    z-index: 10;
    opacity: ${props=>props.isDisplayOn ? 1:0};
`

export default OwnerDashboard
