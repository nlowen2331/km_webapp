import React,{useEffect, useState} from 'react'
import styled from 'styled-components'
import * as FaIcons from 'react-icons/fa'
import {MdCancel} from 'react-icons/md'

/**
 * props-> description,isEditing,()=>saveDescription()
 * 
 * Display the description and allow editing if in edit mode,
 * on save send results up the parent
 * 
 */

function DescriptionEditor(props) {

    const [descInput,changeDescInput] = useState(props.description)

    useEffect(()=>{
        changeDescInput(props.description)
    },[props.description])

    const Cancel = async ()=>{
        changeDescInput(props.description)
    }

    const Submit = async ()=>{
        props.saveDescription({type: 'misc',evName: 'changeUnitDesc', value: descInput})
    }

    let DisplayComponent = <></>

    //If the unit is being edited
    if(props.isEditing) {   
        DisplayComponent=(
            <div style={{position: 'relative',width: '100%', height: '100%'}}>
                <div style={{position: 'absolute', opacity: props.description!==descInput ?
                          1  : 0, transition: '200ms', transform: props.description!==descInput ?
                       'scale(1)' : 'scale(0)', width: '90px', display: 'flex',justifyContent: 'space-around'}}>
                    <FaIcons.FaCheck style={{color: 'darkgreen',height: '30px',width: '30px',
                                            cursor: 'pointer'}} onClick={Submit}/>
                    <MdCancel style={{color: 'darkred',height: '30px',width: '30px',cursor: 'pointer'}}
                                onClick={Cancel}/>
                </div>
                
                <TextArea value={descInput} onChange={(e)=>changeDescInput(e.target.value)}/>

            </div>
        )
    }
    else{
        DisplayComponent=(<div>{props.description ? 
        <pre style={{whiteSpace: 'pre-wrap'}}>{props.description}</pre>: 'This Unit has no description'}</div>)
    }

    return (
        <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center',width: '100%'}}>
            <MainContainer>
                {DisplayComponent}
            </MainContainer>
        </div>
        
    )
}

let MainContainer=styled.div`
    
    border: 1px solid lightgray;
    border-radius: 10%;
    margin: 10px;
    padding: 20px;
    height: 600px;
    width: 600px;
    overflow-y: auto;
    text-align: center;

`

let TextArea = styled.textarea`
    font-size: 15px;
    border: none;
    resize: none;
    white-space: 'pre-wrap';
    width: 100%;
    height: 100%;
    padding: 40px 10px 10px 10px;
    background-color: inherit;
    text-align: center;

    &:focus{
        outline: lightgray solid 1px;
    }
`


export default DescriptionEditor
