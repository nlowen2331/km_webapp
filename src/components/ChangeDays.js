import React from 'react'
import styled from 'styled-components'
import currency from 'currency.js'
import * as FaIcons from 'react-icons/fa'
import * as BiIcons from 'react-icons/bi'
/*
    Props--->
    priceInput
    changePriceInput
    priceSyncSetting
    changePriceSync
    submitChanges
    cancelChanges
*/
function ChangeDays(props) {

    return (
        <MainContainer>
            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%',marginBottom: '10px'}}>
                Change Base Prices
                {props.isHidden ? 
                <BiIcons.BiDownArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                :
                <BiIcons.BiUpArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                }
            </div>
            {props.isHidden ? 
                    <></>
                :
                    <Grid>
                    <div style={{marginBottom: '10px'}}>
                        Change Price(s):
                    </div>
                    <div style={{display: 'flex',justifyContent: 'space-around',alignItems: 'center',width: '100%'}}>
                        <Input maxLength={8} value={props.priceInput} onChange={(e)=>{props.changePriceInput(e.target.value);props.changePriceSync('nosync')}}/>
                        <div>
                            {currency(props.priceInput).value===0
                                || currency(props.priceInput).value!==currency(props.priceInput).value ? 'No changes' : `$${currency(props.priceInput).toString()}`}
                        </div>
                    </div>
                    <div style={{marginBottom: '10px'}}>
                    Sync prices with AirBnB?
                    </div>
                    <div style={{display: 'flex',justifyContent: 'space-around',alignItems: 'center',width: '100%'}}>
                        <Button color={props.priceSyncSetting==='sync' ? 'lightgreen':'lightgray'} onClick={()=>props.changePriceSync('sync')}>
                            Sync Prices
                    </Button>
                    <Button color={props.priceSyncSetting==='nosync' ? 'lightgreen':'lightgray'} onClick={()=>props.changePriceSync('nosync')}>
                        Don't Sync Prices
                    </Button>
                    <Button color={props.priceSyncSetting==='none' ? 'lightgreen':'lightgray'} onClick={()=>props.changePriceSync('none')}>
                        Don't Change
                    </Button>
                    </div>
                    <div style={{display: 'flex',justifyContent: 'space-between',alignItems: 'center',width: '100%',marginTop: '30px'}}>
                        <FaIcons.FaTrashAlt style={{color: 'darkred',cursor: 'pointer'}} onClick={props.cancelChanges}/>
                        <FaIcons.FaCheck style={{color: 'green',cursor: 'pointer'}} 
                            onClick={()=>props.submitChanges(props.priceSyncSetting!=='none'||(currency(props.priceInput).value!==0
                                && currency(props.priceInput).value!==currency(props.priceInput).value))}/>
                    </div>
                </Grid>
            }
            
        </MainContainer>
    )
}

let MainContainer = styled.div`
    padding: 10px;
    border-radius: inherit;
    border: 1px solid black;
    background-color: white;
`

let Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr;
`

let Button = styled.div`
    border: 1px solid black;
    background-color: ${props=>props.color};
    cursor: pointer;
    color: white;
    padding: .2em;
    border-radius: 10px;
    min-width: 70px;
    max-width: 70px;
    height: 40px;
    font-size: .7em;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
`

let Input = styled.input`

    padding: .3em;
    max-width: 200px;
    min-width: 200px;

    &:focus{
        outline: solid 3px blueviolet

    }
`

export default ChangeDays
