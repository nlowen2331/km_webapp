
import imconfig from '../data/config.js'
const config = imconfig.config


const CheckMsgNotifications = (token) => fetch(config.ip + config.port + config.route_getMessages+`/0`, {
    method: "get",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
}).then((res) => {
    return new Promise((resolve,reject) =>{
        res.json().then(json => {
            if (json !== null && json.error_code == 0) {
                console.log('Recieved msgs from server with no errors')
                resolve(json.res.length)
            }
            else {
                console.log('There was an error getting notifications from the server')
                reject(json.error_code)
            }
        });
    })  
}).catch(err => {
    console.log('There was an error getting inquiry notifications')
    console.log(err)
});

export default CheckMsgNotifications