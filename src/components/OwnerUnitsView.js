import React, { useEffect, useState } from 'react'
import EditableUnit from './EditableUnit'
import './OwnerUnitView.css'
import * as BiIcons from 'react-icons/bi'
import PostChangesToServer from '../data/PostOwnerChanges'
import { config } from '../data/config'
import GetUnitLease from './GetUnitLease'
import LeaseView from './LeaseView'
import PostLease from './PostLease'

const LEASE_BUCKET = 'kandmleases'

function OwnerUnitsView(props) {

    const [openedUnits, changeOpenedUnits] = useState([])
    const [currentEditingUnit,changeEditingUnit] = useState(null)
    //Lease manip/view
    const [isViewingLease, toggleIsViewingLease] = useState(false)
    const [lease, setLease] = useState(null)
    const [isLeaseLoaded, toggleIsLeaseLoaded] = useState(false)
    const [uploadedLease,setUploadedLease] = useState(null)

    useEffect(() => {
        if (props.units.length > 0)
            changeOpenedUnits(props.units.map(e => (false)))
    }, [props.unitsLoaded])

    const toggleUnit = (index) => {
        console.log('toggle')
        let opened = openedUnits.slice()
        opened[index]=!opened[index]
        changeOpenedUnits(opened)
    }

    const ViewLease = async (unitid) =>{

        console.log('View Lease Clicked')

        toggleIsViewingLease(unitid)

        let LeaseResponse = await GetUnitLease(unitid)

        if(LeaseResponse.OK){
            setLease(LeaseResponse.pdf)
        }

        toggleIsLeaseLoaded(true)
    }

    const StopViewingLease = ()=>{

        toggleIsViewingLease(false)
        setLease(null)
        toggleIsLeaseLoaded(false)

    }

    const SubmitLease = async (unitid)=>{
        console.log('Submitting Lease')

        let fd = new FormData()
        fd.append(`bucket`,LEASE_BUCKET)
        fd.append(`key`,`${unitid}.pdf`)
        fd.append(`lease`,uploadedLease)

        let Response ={}
        try{
            Response = await PostLease(fd,props.token)
        }
        catch(err){
            Response.OK=false
        }
        
        if(Response.OK){

            toggleIsLeaseLoaded(false)

            let LeaseResponse = await GetUnitLease(unitid)

            if(LeaseResponse.OK){
                setLease(LeaseResponse.pdf)
            }

            toggleIsLeaseLoaded(true)

            return true

        }

        return false
        
    }

    const PostChanges = async (options,unitid)=>{
        console.log('Posting changes to server')
        console.log(options)
        let Response
        switch(options.evName) {
            case `postPictures`:
                //Post
                Response = await PostChangesToServer({changes: options.changeStack, unitid: unitid, token: props.token,route: config.route_PostPictureChanges})
                break;
            case `postSpecs`:
                Response = await PostChangesToServer({changes: options.changeStack, unitid: unitid, token: props.token,route: config.route_PostSpecsChanges})
                break;
            case `postCalender`:
                Response = await PostChangesToServer({changes: options.changeStack, unitid: unitid, token: props.token,route: config.route_PostCalenderChanges})
                break;
            case `postPriceM`:
                Response = await PostChangesToServer({changes: options.changeStack, unitid: unitid, token: props.token,route: config.route_PostPriceMChanges})
                break;
            case `postMisc`:
                Response = await PostChangesToServer({changes: options.changeStack, unitid: unitid, token: props.token,route: config.route_PostMiscChanges})
                break;
            default:
                throw new Error(`Unknown evName given to PostChanges: ${options.evName} `)
        }

        props.UpdateUnits()
        return Response

    }

    const ChangeEditor = (newIndex)=>{
        if(newIndex==null){
            changeEditingUnit(null)
        }
        else if(currentEditingUnit!=null) {
            props.pushMessage(`Please save ${props.units[currentEditingUnit].unitname} changes before editing a new unit`)
        }
        else if(currentEditingUnit==newIndex){
            console.log(`Already Editing that unit`)
        }
        else{
            changeEditingUnit(newIndex)
        }
    }

    let EditableUnits

    if (props.unitsLoaded && props.units.length > 0) {
        EditableUnits = props.units.map((unit, index) => {
            return (
                <div className='editable-unit-container' key={unit.unitid} style={{backgroundColor: currentEditingUnit===index ? 'blanchedalmond' : 'lightgray'}}>
                    <div className='editable-unit-container-upper'>
                        <div className='editable-unit-container-inner'
                            style={{ fontSize: '1.4em' }}>
                            {unit.unitname}
                        </div>
                        <div className='editable-unit-container-inner'>
                            <BiIcons.BiDownArrow style={{ cursor: 'pointer',transition: '500ms', transform: 
                        openedUnits[index] ?'initial' : 'rotate(180deg)' }}
                                onClick={() => toggleUnit(index)} />
                        </div>
                    </div>
                    <div className='editable-unit-container-lower'>
                        {openedUnits[index] ? 
                        <EditableUnit unit={unit} claimEditing={(take)=>ChangeEditor(take ? index:null)} isEditing={currentEditingUnit===index}
                            postChanges={(options)=>PostChanges(options,unit.unitid)} pushMessage={props.pushMessage} ViewLease={ViewLease}/>
                            :
                            <></>
                        }
                    </div>
                </div>
            )
        })

    }


    return (
        <div className='unit-view-main'>
            {isViewingLease ? 
                <LeaseView lease={lease} isLeaseLoaded={isLeaseLoaded} 
                    StopViewingLease={StopViewingLease} setUploadedLease={setUploadedLease}
                        uploadedLease={uploadedLease} SubmitLease={SubmitLease} isViewingLease={isViewingLease}/>
                :
                <></>
            }
            <div className='unit-view-title'>
                Your Units
            </div>
            <div className='unit-view-main-container' style={{ visibility: props.unitsLoaded ? 'visible' : 'hidden' }}>
                {EditableUnits}
            </div>
        </div>
    )
}

export default OwnerUnitsView
