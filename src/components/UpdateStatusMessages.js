import imconfig from '../data/config'
const config = imconfig.config

const UpdateStatusMessages= (token,status,msgid) => fetch(config.ip + config.port + config.route_update_msg_status, {
    method: "post",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +token
    },
    body: JSON.stringify({
        msgid: msgid,
        status: status
    })
}).then((res) => {
    console.log(`using msgid: ${msgid} and using status: ${status}`)
    res.json().then(json => {
        if(json!==null && json.error_code==0){
            console.log(`No errors, updated status for msg id: ${msgid}`)
        }
        else{
            console.log('There was an error updating status')
            console.log(json.error_code)
        }
    });
}).catch(err => {
    console.log('There was an error making the request to the server')
    console.log(err)
});


export default UpdateStatusMessages