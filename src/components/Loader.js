import React from 'react'
import styled,{keyframes} from 'styled-components'

export default function Loader(props) {
    return (
      <div>
        {props.shrink ? <SmallGraphic/>: <Graphic/>}
      </div>

    )
}

const Rotater=keyframes`
from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Graphic= styled.div`
border-top: 16px solid  #f3f3f3;
border-right: 16px solid blue;
border-bottom: 16px solid #f3f3f3;
border-left: 16px solid #f3f3f3;
border-radius: 50%;
width: 120px;
height: 120px;
animation: ${Rotater} 2s linear infinite;
`;

const SmallGraphic= styled.div`
border-top: 4px solid  #f3f3f3;
border-right: 4px solid blue;
border-bottom: 4px solid #f3f3f3;
border-left: 4px solid #f3f3f3;
border-radius: 50%;
width: 30px;
height: 30px;
animation: ${Rotater} 2s linear infinite;
`;