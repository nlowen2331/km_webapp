import imconfig from '../data/config.js'
const config = imconfig.config

const GetUnits = ()=> fetch(config.ip+config.port+config.route_getUnits).then((res)=> {
    return new Promise((resolve,reject)=>{
        res.json().then(json=>{
            if(json!==null && json.error_code==0) {
    
                const data = json[0]
                resolve({units: data,OK: true})

            }
            else{
                console.log('There was an error with server response')
                resolve({OK:false})
            }
        }).catch(err=>{
            console.log('There was an error resolving the server data')
            resolve({OK:false})
        })
    })
    
})

export default GetUnits