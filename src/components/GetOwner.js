import imconfig from '../data/config'
const config = imconfig.config

const GetOwner= (token,callBack) => fetch(config.ip + config.port + config.route_getOwner, {
    method: "get",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +token
    }
}).then((res) => {
    res.json().then(json => {
        if(json!==null && json.error_code==0){
            console.log(`No errors, successfully got owner data`)
            callBack(json[0][0])
        }
        else{
            console.log('There was an getting owner data')
            console.log(json.error_code)
        }
    });
}).catch(err => {
    console.log('There was an error making the request to the server')
    console.log(err)
});


export default GetOwner