import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import * as BiIcons from 'react-icons/bi'
import * as FaIcons from 'react-icons/fa'
import currency from 'currency.js'
import dayjs from 'dayjs'

function ManipulatorEditor(props) {

    const [valueInput,changeValueInput] = useState('')
    const [enabled,toggleEnabled] = useState(props.selectedManipulator.enabled)
    const [addWillEffect,toggleAddWillAffect] = useState(false)
    const [removeWillEffect,toggleRemoveWillAffect] = useState(false)

    useEffect(()=>{
        //Recalculate if add selected dates/remove should be clickable
        AddSelectedCalc().then(res=>{
            toggleAddWillAffect(res)
        })
        RemoveSelectedCalc().then(res=>{
            toggleRemoveWillAffect(res)
        })
        changeValueInput('')
        toggleEnabled(props.selectedManipulator.enabled)
    },[props.selectedManipulator,props.selectedDays])
    
    //Calculate whether adding a date would change data
    const AddSelectedCalc = async ()=>{
        //Iterate through all the selected dates
        let addingWillEffect = !props.selectedDays.every(selected_date=>{
            //Test if a man. date exists that matches the current selected_date
            if(props.selectedManipulator.dates.findIndex(man_date=>dayjs(man_date).isSame(dayjs(selected_date),'day'))===-1){
                //This date does not match, therefore end the search//addingWillEffect is true
                //because there is a day in selectedDays that has no manipulator counterpart
                return false
            }
            //This date does match, therefore continue searching for a non-matching date
            return true
        })
        return(addingWillEffect)
    }

    //Calculate whether removing a date would change data
    const RemoveSelectedCalc = async ()=>{
        //Iterate through every selected date
        let removingWillEffect = !props.selectedDays.every(selected_date=>{
            //Test if man. date exists that matches the current selected date
            if(props.selectedManipulator.dates.findIndex(manipulator_date=>dayjs(manipulator_date).isSame(dayjs(selected_date),'day'))!==-1){
                //This date matches, therefore removingWillEffect=true
                //because there is at least 1 date that exists in selected dates that also exists in
                //manipulator dates
                return false
            }
            //This dates does not match, therefore continue searching for a matching date
            return true
        })
        return(removingWillEffect)
    }

    const EraseChanges = async () =>{
        console.log('Erasing changes')
        changeValueInput('')
    }

    const PushValueChanges = async ()=>{
        console.log('pushing changes')
        //record what will changes
        let Changes = []
        if(enabled!==props.selectedManipulator.enabled){
            Changes.push('enabled')
        }
        //Get value to change
        let UpdatedValue = null
        if(valueInput!=''){
            UpdatedValue=ValueInterprter(valueInput)
            if(UpdatedValue.stringValue!==`${props.selectedManipulator.operation}${currency(props.selectedManipulator.value).value}`){
                Changes.push('value')
            }
        }
        let Response = await props.changePriceManipulators({
            type: 'priceM',
            evName: 'changeManipulatorValues',
            key: props.selectedManipulator.id,
            toChange: Changes,/*'enabled,value */
            value: {
                operation: UpdatedValue!=null ? UpdatedValue.value.operator : null,
                value: UpdatedValue!=null ? UpdatedValue.value.value : null,
                enabled: enabled
            }
        })

        if(Response.OK) {
            EraseChanges()
        }
    }

    const ChangeDates = async (option) =>{
        console.log('Changing dates for manipulator')
        let Response
        switch(option) {
            case 'add':
                if(!addWillEffect){
                    return
                }
                let DatesToAdd=[]

                //Iterate through each selected date
                props.selectedDays.forEach(selected_date=>{
                    //for each selected date check each man_date
                    //if it exists in man_dates don't add date
                    let willAddDate = props.selectedManipulator.dates==null||props.selectedManipulator.dates.length<1
                    ||props.selectedManipulator.dates.every(man_date=>{
                        if(dayjs(selected_date).isSame(dayjs(man_date),'day')){
                            return false
                        }
                        return true
                    })
                    if(willAddDate) {
                        DatesToAdd.push(dayjs(selected_date).format('YYYY-MM-DD'))
                    }
                })

                Response = await props.changePriceManipulators({
                    type: 'priceM',
                    evName: 'addDates',
                    key: props.selectedManipulator.id,
                    dates: DatesToAdd
                })

                if(Response.OK){
                    props.clearSelectedDays()
                }

                break;
            case 'remove':
                if(!removeWillEffect){
                    return
                }
                let DatesToRemove=[]
                props.selectedDays.forEach(selected_date=>{
                    let DateExists = !props.selectedManipulator.dates.every(manipulator_date=>{
                        if(dayjs(selected_date).isSame(dayjs(manipulator_date),'day')){
                            return false
                        }
                        return true
                    })
                    if(DateExists){
                        DatesToRemove.push(selected_date)
                    }
                })

                Response = await props.changePriceManipulators({
                    type: 'priceM',
                    evName: 'removeDates',
                    key: props.selectedManipulator.id,
                    dates: DatesToRemove
                })

                if(Response.OK){
                    props.clearSelectedDays()
                }

                break;
        }
    }

    return (
        <MainContainer>
            <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center', width: '100%',marginBottom: '10px'}}>
                <div>
                   Edit {props.selectedManipulator.name}
                </div>
                {props.isHidden ? 
                <BiIcons.BiDownArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                :
                <BiIcons.BiUpArrow onClick={()=>props.toggleIsHidden()} style={{cursor: 'pointer'}}/>
                }
            </div>
            {props.isHidden ?
                <></>
            :
                <div>
                    <div style={{maxHeight: '.8em',minHeight: '.8em',width: '100%',marginTop: '5px',marginBottom: '10px'}}>
                        {props.selectedDays&&props.selectedDays.length>0 ?
                            <div style={{display: 'flex',justifyContent: 'space-between',alignItems: 'center', width: '100%'}}>
                                <div style={{color: addWillEffect ? 'darkgreen' : 'lightgray',fontSize: '.7em',cursor: 
                                        addWillEffect ? 'pointer' : 'default'}} onClick={()=>ChangeDates('add')}>
                                    Add Selected Dates
                                </div>
                                <div style={{color: removeWillEffect ? 'darkred' : 'lightgray',fontSize: '.7em',
                                        cursor: removeWillEffect ? 'pointer' : 'default'}} onClick={()=>ChangeDates('remove')}>
                                    Remove Selected Dates
                                </div>
                            </div>
                        :
                            <></>
                        }
                    </div>
                    <Grid>
                        <div>
                            Value
                        </div>
                            <Grid2>
                                <div style={{fontSize: '1.5em',maxWidth: '100px',minWidth: '100px',overflow: 'hidden',
                                            color: valueInput===''||ValueInterprter(valueInput).stringValue===`${props.selectedManipulator.operation}${currency(props.selectedManipulator.value).value}`
                                                ? 'black' : 'red'}}>
                                    {valueInput===''||ValueInterprter(valueInput).stringValue===`${props.selectedManipulator.operation}${currency(props.selectedManipulator.value).value}`?
                                            `${props.selectedManipulator.operation}${props.selectedManipulator.operation==='+'||props.selectedManipulator.operation==='-' ? '$':''}${currency(props.selectedManipulator.value).toString()}`
                                        :
                                            `${ValueInterprter(valueInput).display}`
                                    }
                                </div>
                                <Input value={valueInput} onChange={(e)=>changeValueInput(e.target.value)}
                                    maxLength={8} placeholder={`${props.selectedManipulator.operation} ${props.selectedManipulator.value}`}/> 
                            </Grid2>
                        <div>
                            Enabled
                                {enabled===props.selectedManipulator.enabled ? 
                                    <></>
                                    :
                                    <span style={{color: 'red'}}>
                                        *
                                    </span>
                                }
                            
                        </div> 
                        <CheckBox onClick={()=>toggleEnabled(!enabled)}>
                            {enabled ? <FaIcons.FaCheck style={{width: '60%',height: '60%', color: 'darkgreen'}}/> : <div></div>}
                        </CheckBox>
                    </Grid>
                    <div style={{display: 'flex',justifyContent: 'space-between',alignItems: 'center', width: '100%',marginBottom: '10px',
                                marginTop: '10px'}}>
                        <FaIcons.FaTrashAlt style={{color: 'darkred',cursor: 'pointer'}}
                            onClick={EraseChanges}/>
                        <FaIcons.FaCheck style={{color: 'green',cursor: 'pointer'}} 
                            onClick={PushValueChanges}/>
                    </div>
                </div>
            }
            
            
        </MainContainer>
    )
}

//returns an object with {display,stringValue,value}
//display format: +30.00, stringValue format: +30,value format: {operator: '+', value: 30}
const ValueInterprter = (value) =>{

    //Set both regexps
    let operatorRegEx=/(add|subtract|sub|divide|div|times|mul|mult|multiply|x|\+|-|\*|\/){1}/g
    //Get Operator
    let Operator=operatorRegEx.exec(value)
    let OperatorValue

    //determine operator
    if(Operator!=null) {
        switch(Operator[0]) {
            case 'subtract':
            case 'sub':
            case 'su':
            case 's':
            case 'minus':
            case 'min':
            case '-':
                OperatorValue='-'
                break;
            case 'multiply':
            case 'mult':
            case 'mul':
            case 'mu':
            case 'm':
            case 'times':
            case 'x':
            case '*':
                OperatorValue='*'
                break;
            case 'divide':
            case 'div':
            case 'di':
            case 'd':
            case '/':
                OperatorValue='/'
                break;
            default:
                console.log('Defaulted to +')
                OperatorValue='+'
                break;
        }
    }
    else{
        OperatorValue='+'
    }
    
    //Set floatingRegEx
    let floatingRegEx=/[0-9]+.{0,1}[0-9]*/g

    //Get float
    let Float = floatingRegEx.exec(value)
    let CurrencyValue = OperatorValue==='*'||OperatorValue==='/' ? currency(1) : currency(0)
    //Set default if null
    if(Float!=null) {
        CurrencyValue=currency(Float[0])
    }

    //Return
    return {
        display: `${OperatorValue}${OperatorValue==='-'||OperatorValue==='+' ? '$': ''}${CurrencyValue.toString()}`,
        stringValue: `${OperatorValue}${CurrencyValue.value}`,
        value: {
            operator: OperatorValue,
            value: CurrencyValue.value
        }
    }
}

let MainContainer = styled.div`
    padding: 10px;
    border-radius: inherit;
    border: 1px solid black;
    background-color: white;
    padding: 10px;
`
let Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 10px;
`

let Grid2 = styled.div`
    display: grid;
    grid-template-columns: 1fr 5fr;
    gap: 30px;
`

let Input = styled.input`

    padding: .3em;
    max-width: 70px;
    min-width: 70px;

    &:focus{
        outline: solid 3px blueviolet;
    }
`

let CheckBox = styled.div`
    height: 30px;
    width: 30px;
    border: 1px solid black;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    cursor: pointer;
`

let Button = styled.div`
    cursor: ${props=>props.off ? 'initial': 'pointer'};
    background-color: ${props=>props.off ? 'lightgray' : 'lightblue'};
    border: 1px solid black;
    color: white;
    padding: .2em;
    border-radius: 10px;
    min-width: 70px;
    max-width: 70px;
    overflow: hidden;
    display: flex;
    justify-content: center;
`

export default ManipulatorEditor
