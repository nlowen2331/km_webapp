import React, {useRef, useState,useMemo, useEffect} from 'react'
import styled from 'styled-components'
import DayPicker from 'react-day-picker';
import dayjs from 'dayjs'
import Loader from './Loader';
import DOIDisplay from './DOIDisplay';
import DaySelectionPanel from './DaySelectionPanel';
import ChangeDays from './ChangeDays';
import currency from 'currency.js';
import * as MdIcons from 'react-icons/md'
import PriceManipulatorMenu from './PriceManipulatorMenu';
import ManipulatorEditor from './ManipulatorEditor';

/**
 * 
 * @param {days,pushChanges} props 
 * 
 * Takes in a list of days {date,priec,available,syncPrice,priceMultiplier}
 */

//Get first date of the THIS month
let startDate = dayjs().set('date',1)
//Get fist date of THIS year
let startYear = dayjs().set('year',dayjs().year()).set('month',0).set('date',1)

function EditableDays(props) {

    const [hoveringDay, setHoveringDay] = useState(null)
    const [selectionModeOn,toggleSelectionMode] = useState(false)
    const [unselectionModeOn,toggleUnselectionMode] = useState(false)
    const [priceInput,changePriceInput] = useState('0')
    const [priceSyncSetting,changePriceSync] = useState('none'/*none,sync,nosync*/)
    const [hidePriceManipulators,togglePriceManipulators] = useState(true)
    const [highlightedDays,setHighlightedDays] = useState([])
    const [selectedManipulator,setSelectedManipulator] =useState(null)
    const [hideManipulatorEditor,toggleHideManipulatorEditor]=useState(true)
    const [hideChangeDays,toggleHideChangeDays] = useState(true)
    const [highlighting,toggleHighlighting] = useState(false)

    let Selected=null
    if(props.priceManipulators!=null) {
        Selected = props.priceManipulators.find(manipulator=>manipulator.id===selectedManipulator)
    }

    let DayPickers=[]

    let BookedDates = GetBookedDates(props.days)

    const bookedStyle = `.DayPicker-Day--booked {
        color: lightgray;
    }`

    const highlightedStyle = `.DayPicker-Day--highlighted {
        color: gold;
    }`

    const modifiers = {
        booked: [...BookedDates],
        highlighted: [...highlightedDays]
    }

    //Handle hover over date
    const HandleHover = (day, {selected,disabled})=>{
        setHoveringDay(GetDayObjectByDate(day))
    }

    //handle day click
    const HandleDayClick = (day, {selected,disabled})=>{
        if(selectionModeOn&&!selected){
            props.setSelectedDays(prevState=>[...prevState,day])
        }
        else if(unselectionModeOn&&selected){   
            props.setSelectedDays(prevState=>[...prevState.filter(selected_day=>(
                !dayjs(selected_day).isSame(dayjs(day),'day')
            ))])
        }
    }
    //handle week click
    const HandleWeekClick = (weekNumber,days)=>{
        console.log(days)
        if(selectionModeOn){
            props.setSelectedDays(prevState=>[...prevState,...days])
        }
        else if(unselectionModeOn){   
            let startOfWeek = startYear.add(weekNumber,'week').set('day',0)
            console.log(startOfWeek)
            let endOfWeek = startOfWeek.set('day',6)
            console.log(endOfWeek)
            props.setSelectedDays(prevState=>[...prevState.filter(selected_day=>(
                //Check if this date is within the week
                !dayjs(selected_day).isSame(startOfWeek)&&(
                    dayjs(selected_day).isBefore(startOfWeek,'day') ||
                    dayjs(selected_day).isAfter(endOfWeek,'day')
                )
            ))])
        }
    }

    const UpdateCalenders= async (update)=>{
        console.log('Update Calenders clicked')

        //If update is false, return from the function
        if(!update) {
            return
        }

        let Success = await props.changeCalender({
            type: 'calender',
            evName: 'calenderEvent',
            value: {
                priceSyncSetting: priceSyncSetting,
                price: priceInput,
                priceChange: (currency(priceInput).value===0
                                || currency(priceInput).value!==currency(priceInput).value 
                                    ? false : true)
            },
            daysAffected: props.selectedDays.map(day=>(dayjs(day).format('YYYY-MM-DD')))
        })

        if(Success) {
            CancelCalenderChanges()
            props.setSelectedDays([])
        }
    }

    const SelectAllFromMonth = async (month,year)=>{
        console.log('Select all from month')
        let firstOfMonth = dayjs().set('year',year).set('month',month).set('date',1)
        let lastOfMonth = firstOfMonth.add(1,'month').subtract(1,'day')
        if(selectionModeOn){
            let daysInMonth = []
            let tmpDate = firstOfMonth
            let limiter = 0

            while(tmpDate.month()===month) {

                daysInMonth.push(new Date(tmpDate.toString()))
                tmpDate = tmpDate.add(1,'day')

                if(limiter>31) {
                    throw new Error('Logical error selecting dates from the specified month')
                }

                limiter++
            }

            console.log(daysInMonth)

            props.setSelectedDays(prevState=>[...prevState,...daysInMonth])
        }
        else if(unselectionModeOn){   
            props.setSelectedDays(prevState=>[...prevState.filter(selected_day=>(
                //Check if this date is within the month
                !dayjs(selected_day).isSame(firstOfMonth)&&(
                    dayjs(selected_day).isBefore(firstOfMonth,'day') ||
                    dayjs(selected_day).isAfter(lastOfMonth,'day')
                )
            ))])
        }
    }

    const CancelCalenderChanges= async ()=>{
        console.log('Cancel Calender Changes clicked')
        changePriceInput('0')
        changePriceSync('none')
    }

    //Get object by date
    const GetDayObjectByDate = (date) =>{
        let ReturnDate = {
            date: 'unknown',
            price: 0,
            available: false,
            syncPrice: false
        }

        let formatted_calender_date = dayjs(date)

        props.days.every(day=>{
            let formatted_day = dayjs(day.date)
            if(formatted_day.isSame(formatted_calender_date,'day')) {
                ReturnDate=day
                return false
            }
            return true
        })

        return ReturnDate
    }

    for (let i=0; i<12;i++){

        let thisMonth = startDate.set('M',i)

        DayPickers.push(
            <Calender>
                <MdIcons.MdDateRange style={{position: 'absolute',height: '20px',width: '20px',cursor: 'pointer',right: '10px',zIndex: 2}}
                    onClick={()=>SelectAllFromMonth(thisMonth.month(),thisMonth.year())}/>
                <style>{bookedStyle}{highlightedStyle}</style>
                <DayPicker fromMonth={thisMonth.toDate()} toMonth={thisMonth.toDate()}
                initialMonth={thisMonth.toDate()} modifiers={modifiers} onDayMouseEnter={HandleHover}
                    onDayClick={HandleDayClick} selectedDays={props.selectedDays} showWeekNumbers={true}
                        onWeekClick={HandleWeekClick}/>
            </Calender>
        )
    }

    return (
        <div style={{display: 'flex',justifyContent: 'center'}}>
            <Grid>
                <OptionsContainer>
                    <StickyOptions>
                        {hoveringDay==null ? '' : <DOIDisplay day={hoveringDay} priceManipulators={props.priceManipulators}/>}
                        {props.isEditing ? <DaySelectionPanel selecting={selectionModeOn} unselecting={unselectionModeOn}
                            off={props.isEditing} toggleSelecting={toggleSelectionMode} toggleUnselecting={toggleUnselectionMode}
                            unSelectAll={()=>props.setSelectedDays([])}/> : ''}
                        <PriceManipulatorMenu isHidden={hidePriceManipulators} canEdit={props.isEditing}
                            priceManipulators={props.priceManipulators} changeHighlightedDays={setHighlightedDays}
                            changePriceManipulators={props.changePriceManipulators} toggleIsHidden={()=>togglePriceManipulators(!hidePriceManipulators)}
                                selectedManipulator={selectedManipulator} setSelectedManipulator={setSelectedManipulator}
                                highlighting={highlighting} toggleHighlighting={()=>toggleHighlighting(!highlighting)}
                                selectedDays={props.selectedDays} changesStack={props.changesStack}
                            />
                        {props.isEditing&&Selected!=null ? 
                            <ManipulatorEditor isHidden={hideManipulatorEditor} toggleIsHidden={()=>toggleHideManipulatorEditor(!hideManipulatorEditor)}
                                selectedDays={props.selectedDays} changePriceManipulators={props.changePriceManipulators} selectedManipulator={Selected}
                                clearSelectedDays={()=>props.setSelectedDays([])}/>
                            :
                            ''
                        }
                        {props.selectedDays!=null&&props.selectedDays.length>0 ? <ChangeDays 
                            changePriceSync={changePriceSync} changePriceInput={changePriceInput} priceSyncSetting={priceSyncSetting}
                                priceInput={priceInput} submitChanges={UpdateCalenders} cancelChanges={CancelCalenderChanges}
                                isHidden={hideChangeDays} toggleIsHidden={()=>toggleHideChangeDays(!hideChangeDays)} /> : ''}
                    </StickyOptions>
                </OptionsContainer>
                {DayPickers}
            </Grid>
        </div>
    )
}

//Takes in array of days, returns array of days that are booked

const GetBookedDates = (days)=>{
    let Disabled = []
    days.forEach(day=>{
        if(!day.available) {
            Disabled.push(dayjs(day.date).toDate())
        }
    })
    return Disabled
}

let Grid = styled.div`
    display: grid;
    grid-template-columns: 300px 300px 300px 400px;
    grid-template-rows: 310px;
    gap: .5em;
`

let Calender = styled.div`
    min-width: 290px;
    position: relative;
`
let OptionsContainer = styled.div`
    grid-column: 4;
    grid-row: 1/5;
    min-width: 300px;
    border: 1px solid lightgray;
    border-radius: 1em;
`

let StickyOptions = styled.div`
    position: sticky;
    width: 100%;
    border-radius: inherit;
    top: 0px;
`


export default EditableDays
