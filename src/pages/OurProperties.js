import React, { useState, useEffect } from 'react'
import './OurProperties.css'
import Select from 'react-select'
import { useTransition, animated, useSpring } from 'react-spring'
import UnitCard from '../components/UnitCard'
import styled from 'styled-components'
import DayPicker from 'react-day-picker';
import GetUnits from '../components/GetUnits'
import dayjs from 'dayjs'
import Loader from 'react-spinners/PuffLoader'
import useClickAway from '../hooks/useClickAway'

let today = dayjs()

function OurProperties(props) {

  //State variables for search
  const [locationIndex, setLocation] = useState(0)
 
  const [guestsIndex, setGuests] = useState(0)
  const [bedsIndex, setBeds] = useState(0)
  const [bathsIndex, setBaths] = useState(0)

  const [units, updateUnits] = useState([])
  const [isLoaded, toggleIsLoaded] = useState(false)

  //calender
  const [calenderScreenOn,toggleCalenderScreen] = useState(false)
  const [arrivalDate,setArrivalDate]=useState(null)
  const [departureDate,setDepartureDate]=useState(null)


  useEffect(() => {
    GetUnits().then(res=>{
      if(res.OK){
        updateUnits(res.units)
      }
      toggleIsLoaded(true)
    })

    if(props.location.state){
      setArrivalDate(props.location.state.arrival)
      setDepartureDate(props.location.state.departure)
    }
  }, [])

  useEffect(()=>{
    if(arrivalDate&&departureDate){
      toggleCalenderScreen(false)
    }
  },[arrivalDate,departureDate])

  const ShowCalender = async (option)=>{
    if(option){
        setArrivalDate(null)
        setDepartureDate(null)
        toggleCalenderScreen(true)
    }else{
        toggleCalenderScreen(false)
    }
  }

  const HideCalender= async ()=>{
    console.log('Hide Calender')
    toggleCalenderScreen(false)
  }

  //Return false if the date range of arrival -> departure contains any 
  //booked dates
  const CheckAvailable = (calender)=>{
    //find entry index for arrival/dep date
    let startDateIndex = calender.findIndex(entry=>dayjs(entry.date).isSame(dayjs(arrivalDate),'day'))
    let endDateIndex = calender.findIndex(entry=>dayjs(entry.date).isSame(dayjs(departureDate),'day'))
    //if either doesn't exist return false
    if(startDateIndex===-1||departureDate===-1){
      console.warn(`Could not find arrival/departure date in unit calender`)
      return false
    }
    //Slice calender at arrival
    let SlicedCalender = calender.slice(startDateIndex,endDateIndex)

    //iterate through calender and return false if a day is booked
    let Available = SlicedCalender.every(entry=>{
      if(!entry.available){
        return false
      }
      return true
    })

    return Available

  }

  //hard-coded options
  const locationOptions = [
    { value: 0, label: 'Any Location', type: 'location' },
    { value: 1, label: 'Attitash,NH', type: 'location' },
    { value: 2, label: 'Wells,ME', type: 'location' },
    { value: 3, label: 'York,ME', type: 'location' }
  ]

  const guestMax = 15
  const guestOptions = []

  for (let i = 1; i <= guestMax; i++) {
    guestOptions.push({ value: i, label: i + '+', type: 'guest' })
  }

  const bedMax = 10
  const bedOptions = []

  for (let i = 1; i <= bedMax; i++) {
    bedOptions.push({ value: i, label: i + '+', type: 'bed' })
  }

  const bathMax = 10
  const bathOptions = []

  for (let i = 1; i <= bathMax; i++) {
    bathOptions.push({ value: i, label: i + '+', type: 'bath' })
  }

  const handleChange = (selectedOption) => {

    switch (selectedOption.type) {
      case 'location':
        setLocation(selectedOption.value)
        break;
      case 'guest':
        setGuests(selectedOption.value - 1)
        break;
      case 'bed':
        setBeds(selectedOption.value - 1)
        break;
      case 'bath':
        setBaths(selectedOption.value - 1)
        break;
    }
  }

  let visibleCards = []

  if (isLoaded) {
    visibleCards = units.filter((card) => {
      const s = JSON.parse(card.specifications)
      const calender = JSON.parse(card.calender)
      if(arrivalDate!=null&&departureDate!=null&&/*Check if calender dates are available*/!CheckAvailable(calender.days)) {
        return false
      }
      if(s==null||calender==null) {return false}
      const a = s.ammenities
      if ((locationOptions[locationIndex].label !== 'Any Location') && (s.city + ',' + s.state !== locationOptions[locationIndex].label))
        return false
      if (s.guests < guestOptions[guestsIndex].value)
        return false
      if (a.bedrooms < bedOptions[bedsIndex].value)
        return false
      if (a.bathrooms < bathOptions[bathsIndex].value)
        return false
      return true
    })
  }

  const trans = useTransition(visibleCards, visibleCards=>visibleCards.unitid, {
    from: { opacity: 0, transform: 'translate3d(0,100%,0)' },
    enter: { opacity: 1, transform: 'translate3d(0%,0,0)' },
    leave: { opacity: 0, transform: 'translate3d(0,50%,0)' },
    unique: true,
    reset: true
  })

  const calenderSpring = useSpring({
      height: calenderScreenOn ? `500px` : `0px`,
      opacity: calenderScreenOn ? 1 : 0
  })

  const clickAwayRef = useClickAway(()=>ShowCalender(false))

    const handleDayClick = (day,{disabled,selected},pickDate) =>{
        if(disabled){
            return
        }
        else if(selected){
            pickDate(null)
        }
        else{
            pickDate(day)
        }
    }


  return (
    <>
      <div className='space'>

      </div>
      <div className='properties-main-container'>
        <div className='properties-title'>
          Find a Place to Stay!
        </div>
        <div className="properties-searchbar">
          <div className='properties-searchbar-container'>
            <p>Location?</p>
            <Select className='properties-select' options={locationOptions} defaultValue={locationOptions[locationIndex]}
              onChange={handleChange} />
          </div>
          <div className='properties-searchbar-container'>
            <p>Search by Date?</p>
            <div style={{display: 'flex',justifyContent: 'center',position: 'relative'}}>
              <CalenderButton on={calenderScreenOn} onClick={()=>ShowCalender(true)}>
                {arrivalDate&&departureDate ? `${dayjs(arrivalDate).format('MM/DD')}-${dayjs(departureDate).format('MM/DD')}`: 'Show Calender'}
                </CalenderButton>
            </div>
          </div>
          <div className='properties-searchbar-container'>
            <p>Guests?</p>
            <Select className='properties-select' options={guestOptions} defaultValue={guestOptions[guestsIndex]} onChange={handleChange} />
          </div>
          <div className='properties-searchbar-container'>
            <p>Beds?</p>
            <Select className='properties-select' options={bedOptions} defaultValue={bedOptions[bedsIndex]} onChange={handleChange} />
          </div>
          <div className='properties-searchbar-container'>
            <p>Bathrooms?</p>
            <Select className='properties-select' options={bathOptions} defaultValue={bathOptions[bathsIndex]} onChange={handleChange} />
          </div>
        </div>
        <animated.div style={{...calenderSpring,width:'100%',display:'flex',justifyContent:'center',paddingBottom:'20px'}}>
            <div className='calender-select-wrapper' ref={clickAwayRef}>
                <div className='title'>Select Arrival & Departure</div>
                <div className='calender-flex-wrapper'>
                  <div className='calender-container-2'>
                    <div className='sub-title'>Arrival</div>
                    <div className='picker-wrapper-2'>
                      <DayPicker fromMonth={new Date()} toMonth={dayjs().add(1,'year').toDate()}
                        disabledDays={{before: new Date()}} onDayClick={(day,modifiers)=>handleDayClick(day,modifiers,setArrivalDate)}
                        selectedDays={arrivalDate&&dayjs(arrivalDate).toDate()}/>  
                    </div>
                  </div>
                  <div className='calender-container-2'>
                    <div className='sub-title'>Departure</div>
                    <div className='picker-wrapper-2'>
                      <DayPicker fromMonth={new Date()} toMonth={dayjs().add(1,'year').toDate()} month={arrivalDate ? dayjs(arrivalDate).toDate() : null}
                        disabledDays={{before: arrivalDate ? dayjs(arrivalDate).toDate() : new Date()}} onDayClick={(day,modifiers)=>handleDayClick(day,modifiers,setDepartureDate)}
                        selectedDays={departureDate&&dayjs(departureDate).toDate()}/>
                    </div>
                  </div>
                </div> 
            </div>
        </animated.div>
        { isLoaded ?
          <div className='properties-cards-container'>
          
            {trans.length>0 ?
            trans.map(({ item, key,props }) => (
              <animated.div style={props} key={key}>
                <UnitCard {...item} />
              </animated.div>
            ))
          
          :
              <div style={{fontSize: '3em',textAlign: 'center'}}>Sorry, no units match your specifications!</div>
          }
        </div> 
              :
            <Loader color='teal'/>
        }
      </div>
    </>
  );
}

let CalenderButton = styled.div`
  width: 95%;
  overflow: hidden;
  border: 1px solid lightgray;
  border-color: ${props=>props.on ? 'dodgerblue' : 'lightgray'};
  border-radius: 5%;
  padding-top: .3em;
  padding-bottom: .3em;
  color: ${props=>props.on ? 'black' : 'lightgray'};
  cursor: pointer;
`

export default OurProperties;