import React, { useState, useEffect } from 'react'
import './OwnersPage.css'
import ConfirmLogin from '../data/ConfirmLogin'
import Authenticate from '../data/Authenticate'
import OwnerDashboard from '../components/OwnerDashboard'
import Loader from '../components/Loader'

function OwnersPage() {

    const [token, changeToken] = useState(localStorage.getItem('k&m_auth_token'))
    const [loggedIn, toggleLogIn] = useState(false)
    const [passwordField, changePasswordField] = useState('')
    const [userNameField, changeUserNameField] = useState('')
    const [rejectionMessage, changeRejectionMessage] = useState('')
    const [submitDisabled, toggleDisableSubmit] = useState(false)
    const [isLoading, toggleIsLoading] = useState(false)

    const clearToken = (bool) => { if (bool) { changeToken(null); localStorage.clear('k&m_auth_token'); toggleLogIn(false) } }
    const cacheUserName = (user) => { { localStorage.setItem('k&m_username', user) } }

    const name = localStorage.getItem('k&m_username')

    useEffect(() => {
        if (token != null) {
            toggleIsLoading(true)
            localStorage.setItem('k&m_auth_token', token)
            setImmediate(() => ConfirmLogin(token, toggleLogIn, clearToken, cacheUserName).then(() => {
                changePasswordField('')
                changeUserNameField('')
                toggleIsLoading(false)
            }))
        }
    }, [token])

    const handleChange = (e) => {
        switch (e.target.className) {
            case 'input-pass':
                changePasswordField(e.target.value)
                break
            case 'input-user':
                changeUserNameField(e.target.value)
                break
        }
    }

    const handleSubmit = (e) => {
        if (submitDisabled)
            return
        console.log('Handle submit triggered')
        toggleDisableSubmit(true)
        Authenticate(userNameField, passwordField, changeToken, changeRejectionMessage, toggleDisableSubmit)
    }

    if(isLoading) {
        return (
            <div style={{height: '100%', width: '100%', display: 'flex',justifyContent: 'center',
                alignItems: 'center'}}>
                <Loader/>
            </div>
        )
    }

    const logInScreen = (
        <div className='login-page-main'>
            <div className='login-page-container'>
                <p style={{ fontSize: '5em' }}>Owner's Login</p>
                <div className='login-page-inner'>
                    <div className='login-user-container'>
                        <p>Username</p>
                    <input className='input-user'
                            value={userNameField} onChange={handleChange} />
                    </div>
                    <div className='login-pass-container'>
                        <p>Password</p>
                    <input  className='input-pass'
                            value={passwordField} onChange={handleChange}
                            type="password" />
                    </div>
                </div>
                <p style={{ color: 'red' }}>{rejectionMessage === '' ? '' : rejectionMessage}</p>
                <div className='unit-btn-container'>
                    <div className='unit-send-btn' onClick={handleSubmit} style={submitDisabled ? { backgroundColor: 'gray', cursor: 'initial' } : {}}>
                        Send
                </div>
                </div>
            </div>

        </div>
    )

    return (
        <>
            <div className='space'>

            </div>
            {loggedIn ?
                <OwnerDashboard name={name} token={token} clearToken={clearToken} />
                :
                logInScreen}
        </>
    )
}

export default OwnersPage
