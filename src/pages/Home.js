import React, { useState, useEffect, useRef } from 'react'
import './Home.css'
import BlurryLoadingImage from '../components/componentsv2/BlurryLoadingImage'
import { useSpring, useTransition,animated, useChain } from 'react-spring';
import DateSelectorButton from '../components/componentsv2/DateSelectorButton';
import { useHistory } from 'react-router';
import CardSlideShow from '../components/componentsv2/CardSlideShow';
import useMedia from '../hooks/useMedia'


function Home() {

  const [index,setIndex] = useState(0)

  const [arrival,setArrival] = useState(null)
  const [departure,setDeparture] = useState(null)

  const [bgLoaded,setbgLoaded] = useState(false)

  const history = useHistory()

  const getStartedBtn = useMedia(

      ['(max-width: 750px)'],
      [true],
      false
  )


  useEffect(()=>{
    if(arrival&&departure){
      history.push({pathname: '/OurProperties',state: {arrival,departure}})
    }
  },[arrival,departure])

  const GetStarted = ()=>{
    history.push('/OurProperties')
  }

  const titleSpring = useSpring({
    from: {opacity: 0,transform: 'translate3d(-50%,0,0)'},
    to: {opacity: bgLoaded ? 1 : 0, transform: bgLoaded ? 'translate3d(0,0,0)' : 'translate3d(-50%,0,0)'},
    delay: 1000
  })
  const step2 = useSpring({
    from: {opacity: 0,transform: 'translate3d(-50%,0,0)'},
    to: {opacity: bgLoaded ? 1 : 0, transform: bgLoaded ? 'translate3d(0,0,0)' : 'translate3d(-50%,0,0)'},
    delay: 1500
  })
  const step3 = useSpring({
    from: {opacity: 0,transform: 'translate3d(-50%,0,0)'},
    to: {opacity: bgLoaded ? 1 : 0, transform: bgLoaded ? 'translate3d(0,0,0)' : 'translate3d(-50%,0,0)'},
    delay: 2000
  })
  const step4 = useSpring({
    from: {opacity: 0,transform: 'translate3d(-50%,0,0)'},
    to: {opacity: bgLoaded ? 1 : 0, transform: bgLoaded ? 'translate3d(0,0,0)' : 'translate3d(-50%,0,0)'},
    delay: 2500
  })
  const step5 = useSpring({
    from: {opacity: 0},
    to: {opacity: bgLoaded ? 1 : 0},
    delay: 3500
  })
  
  return (
    <>
    <div className='home-main'>
        <BlurryLoadingImage preview={'./images/cover_thumbnail.jpg'} image={'./images/cover_york.jpg'}
          alt='Lighthouse on the brimming sea' imageStyleClass='bg-pic' divStyleClass='bg-pic-container'
          bgColor='lightgray' onLoad={()=>setbgLoaded(true)}/>
        <animated.h1 style={titleSpring}>Welcome to K&M Properties</animated.h1>
        <animated.h3 style={step2}>Browse</animated.h3>
        <animated.h3 style={step3}>Book</animated.h3>
        <animated.h3 style={step4}>Fall in Love!</animated.h3>
        <animated.div style={step5} className='get-started-wrapper'>
            {getStartedBtn ?
                <button className='get-started-btn' onClick={GetStarted}>Get Started!</button>
              :
              <div className='get-started-inner'>
                  <DateSelectorButton title={'Arrival'} selection={arrival} pickDate={setArrival}/>
                <DateSelectorButton title={'Departure'} selection={departure} pickDate={setDeparture}/>
              </div> 
            }
            
        </animated.div>
        
      </div>
      <CardSlideShow index={index} setIndex={setIndex}/>
      
        <div className='space-under'>

      </div>
    </>
  );
}

export default Home;