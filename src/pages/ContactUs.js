import React, { useState } from 'react';
import PostMessage from '../components/PostMessage';
import './ContactUs.css'
import { useTransition, animated, useSpring } from 'react-spring'

function ContactUs() {

  const [fnameText, changeFNameText] = useState("")
  const [lnameText, changeLNameText] = useState("")
  const [phoneText, changePhoneText] = useState("")
  const [emailText, changeEmailText] = useState("")
  const [commentText, changeCommentText] = useState("")


  const [rejectedPhone, changeRejectedPhone] = useState(false)
  const [rejectedEmail, changeRejectedEmail] = useState(false)
  const [rejectedFName, changeRejectedFName] = useState(false)
  const [rejectedLName, changeRejectedLName] = useState(false)

  const [acceptedMsgOn, changeMsgOn] = useState(false)
  const [acceptedMsg, changeAccepetedMsg] = useState(null)


  const pageLoadSpring = useSpring({
    from: {opacity: 0},
    opacity: 1
  })

  const handleChange = (e) => {
    switch (e.target.className) {
      case 'input-fname-cu':
        changeFNameText(e.target.value)
        changeRejectedFName(false)
        break;
      case 'input-lname-cu':
        changeLNameText(e.target.value)
        changeRejectedLName(false)
        break;
      case 'input-phone-cu':
        changePhoneText(e.target.value)
        changeRejectedPhone(false)
        break;
      case 'input-email-cu':
        changeEmailText(e.target.value)
        changeRejectedEmail(false)
        break;
      case 'text-area-cu':
        changeCommentText(e.target.value)
        break;
    }

  }

  const handleSubmit = (e) => {
    console.log('Handle submit triggered')

    const phoneRegEx = RegExp('^[\\d]{10}$');
    const emailRegex = RegExp('^\\S+@\\S+\\.\\S+$');
    const nameRegEx = RegExp('^[a-zA-z -]{1,30}$');

    let nameTest = nameRegEx.test(fnameText)
    //let lastNameTest = nameRegEx.test(lnameText)
    let phoneTest = phoneRegEx.test(phoneText)
    let emailTest = emailRegex.test(emailText)


    if (nameTest, /*lastNameTest,*/ phoneTest, emailTest) {

      changeRejectedPhone(false)
      changeRejectedEmail(false)
      changeRejectedFName(false)
      changeRejectedLName(false)

      console.log("All Tests passed")

      const json = {
        fname: fnameText,
        lname: '',
        phone: phoneText,
        email: emailText,
        message: commentText,
      }

      console.log(json)

      console.log("JSON created calling PostMessage")

      PostMessage(json, changeAccepetedMsg, changeMsgOn, resetState)
      window.scrollTo(0, 0)
    }

    else {

      console.log("Some tests failed, should be reflected by red text")

      changeRejectedPhone(!phoneTest)
      changeRejectedEmail(!emailTest)
      changeRejectedFName(!nameTest)
      //changeRejectedLName(!lastNameTest)
    }

  }

  const resetState = (reset) => {
    if (reset) {
      changeFNameText('')
      changeLNameText('')
      changePhoneText('')
      changeEmailText('')
      changeCommentText('')
    }
  }

  const acceptedTrans = useTransition(acceptedMsgOn, null, {
    from: { opacity: 0, height: '0px' },
    enter: { opacity: .8, height: '100px' },
    leave: { opacity: 0, height: '0px' },
    unique: true
  })

  return (
    <>
      <div className='space'>

      </div>
      <div className='contact-us-form-container'>
        {acceptedTrans.map(({ item, key, props }) => (
          item
            ?
            <animated.div
              key={key}
              style={props}
            >
              <div className='accepted-msg-container' style={{ backgroundColor: acceptedMsg.accept ? 'rgb(0, 233, 31)' : 'darkred' }}>{acceptedMsg.msg}</div>
            </animated.div>
            :
            ''
        ))}
        <animated.div className='contact-us-form-main' style={pageLoadSpring}>
          <p className='contact-title'>Contact Us</p>
          {rejectedFName ? <span className='rejection-msg'>   *We could not accept this name</span> : ''}
          <input className='input-fname-cu' placeholder={"Name"} onChange={handleChange} value={fnameText} />
          {rejectedPhone ? <span className='rejection-msg'>   *We could not accept this phone number</span> : ''}
          <input className='input-phone-cu' placeholder={"Phone Number"} onChange={handleChange} value={phoneText} />
          {rejectedEmail ? <span className='rejection-msg'>   *We could not accept this email</span> : ''}
          <input className='input-email-cu' placeholder={"Email"} onChange={handleChange} value={emailText} />
          <textarea className='text-area-cu' placeholder={"Tell us anything you want us to know..."} onChange={handleChange} value={commentText} />
          <div className='send-btn' onClick={handleSubmit}>
            Send
        </div>
        </animated.div>
       
      </div>
    </>
  );
}

export default ContactUs