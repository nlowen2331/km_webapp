import React, { useEffect, useRef, useState } from 'react'
import Select from 'react-select'
import './Unit.css'
import SlideShow from '../components/SlideShow';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { animated, useTransition } from 'react-spring';
import PostInquiry from '../components/PostInquiry'
import currency from 'currency.js'
import dayjs from 'dayjs';
import {animateScroll} from 'react-scroll'
import Loader from 'react-spinners/BeatLoader';

const TAX_RATE = 0.09

const m_accept = 'Your Inquiry Has Been Sent! Make Sure to Check your Email for Confirmation.'

const m_reject = 'Your Inquiry was not processed. Please try again, or contact us directly instead.'

function Unit(props) {

    const unit = props.attributes
    let today = new Date()
    const s = JSON.parse(unit.specifications)
    const a = s.ammenities
    const r = s.rules
    const calender = JSON.parse(unit.calender)
    const priceManipulators = JSON.parse(unit.priceManipulators)
    const p = JSON.parse(unit.pictures)
    const PictureKeys = []
    p.forEach(pic=>{
        if(pic.isHidden){
            return
        }
        else{
            PictureKeys.push(`${unit.unitid}/${pic.picture_id}`)
        }
    })

    /*
        State of form
    */

    const [rejectedArrival, changeRejectedArrival] = useState(false)
    const [rejectedDeparture, changeRejectedDeparture] = useState(false)
    const [rejectedPhone, changeRejectedPhone] = useState(false)
    const [rejectedEmail, changeRejectedEmail] = useState(false)
    const [rejectedFName, changeRejectedFName] = useState(false)
    const [rejectedLName, changeRejectedLName] = useState(false)

    const beginForm = useRef(null)

    const [acceptedMsgOn, changeMsgOn] = useState(false)
    const [acceptedMsg, changeAccepetedMsg] = useState('')

    const [index,setIndex] = useState(0)

    //Scroll to top on msgOn
    useEffect(()=>{
        if(acceptedMsgOn) {animateScroll.scrollToTop()}
    },[acceptedMsgOn])

    const [arrivalCalenderOpen, toggleArrivalCalender] = useState(false)
    const [departureCalenderOpen, toggleDepartureCalender] = useState(false)

    const [arrivalSelection, updateArrivalSelection] = useState(null)
    const [departureSelection, updateDepartureSelection] = useState(null)

    const disabledDates =
        calender.days.filter((e) => {
            if (!e.available)
                return true
            else
                return false
        }).map(e => (new Date(e.date.substring(0,4),e.date.substring(5,7)-1,e.date.substring(8,10))))
    
    const [initialMonth, changeInitialMonth] = useState(new Date(today.getFullYear(), today.getMonth()))

    let pastDays = Date.prototype.getRange(new Date(today.getFullYear(),today.getMonth(),1),today)

    const getConflictingDepartureSelections = () => {

        if(arrivalSelection==null)
            return []

        let beforeDays = Date.prototype.getRange(new Date(today.getFullYear(),today.getMonth(),1),arrivalSelection)
        let afterDays = Date.prototype.getRange(arrivalSelection,new Date(arrivalSelection.getFullYear()+1,arrivalSelection.getMonth(),arrivalSelection.getDate()))
        
        //before arrival
        let beforeArrivalSelections = []
        beforeDays.forEach(e=>(beforeArrivalSelections.push(e)))
        //Check day minimum rule
        let dayMinimumSelections = []
        //Check day maximum rule
        let dayMaximumSelections = []
        //Check saturday-saturday rule
        let saturdayRuleSelections = []
        //Check for gaps in availability
        let availabilityGapSelections = []
        afterDays.every(e1=>{
            if(Date.prototype.normalize(e1)===Date.prototype.normalize(arrivalSelection)) {
                return true
            }
            let flag = false
            disabledDates.every(e2=>{
                if(Date.prototype.normalize(e1)===Date.prototype.normalize(e2)) {
                    let after = Date.prototype.getRange(e1,new Date(e1.getFullYear()+1,
                        e1.getMonth(),e1.getDate()))
                    after.forEach(e3=>(availabilityGapSelections.push(e3)))
                    flag=true
                    return false
                }
                return true
            })
            return !flag
        })

        return [...beforeArrivalSelections,...dayMaximumSelections,
                ...dayMaximumSelections,...saturdayRuleSelections,...availabilityGapSelections]

    }

    const [guestIndex, setguestIndex] = useState(0)
    const [hasPets, setHasPets] = useState(false)
    const [petIndex, setpetIndex] = useState(0)
    const [speciesText, changeSpeciesText] = useState("")
    const [firstNameText, changeFirstNameText] = useState("")
    const [lastNameText, changeLastNameText] = useState("")
    const [phoneText, changePhoneText] = useState("")
    const [emailText, changeEmailText] = useState("")
    const [commentsText, changeCommentsText] = useState("")


    const guestOptions = []

    for (let i = 1; i <= s.guests; i++) {
        guestOptions.push({ value: i, label: i, type: 'guest' })
    }

    const hasPetOptions = [
        { label: "No", value: 0, type: "hasPets" },
        { label: "Yes", value: 1, type: "hasPets" }
    ]

    const numberPetsMax = 3
    const numberPetsOptions = []

    for (let i = 1; i <= numberPetsMax; i++) {
        if (i == numberPetsMax) { numberPetsOptions.push({ value: i, label: i + "+", type: 'numPets' }) }
        else { numberPetsOptions.push({ value: i, label: i, type: 'numPets' }) }
    }

    const getWindowWidth = () => {
        return window.innerWidth
    }

    const handleChange = (selectedOption) => {
        switch (selectedOption.type) {
            case 'numPets':
                setpetIndex(selectedOption.value - 1)
                break;
            case 'guest':
                setguestIndex(selectedOption.value - 1)
                break;
            case 'hasPets':
                setHasPets(!hasPets)
                break;
        }
    }

    const handleTextChange = (e) => {
        switch (e.target.className) {
            case 'input-breed':
                changeSpeciesText(e.target.value)
                break
            case 'input-first':
                changeFirstNameText(e.target.value)
                changeRejectedFName(false)
                break
            case 'input-last':
                changeLastNameText(e.target.value)
                changeRejectedLName(false)
                break
            case 'input-phone':
                changePhoneText(e.target.value)
                changeRejectedPhone(false)
                break
            case 'input-email':
                changeEmailText(e.target.value)
                changeRejectedEmail(false)
                break
            case 'text-area':
                changeCommentsText(e.target.value)
                break;
        }
    }

    const handleSubmit = (e) => {

        console.log('Handle submit triggered')

        const phoneRegEx = RegExp('^[\\d]{11}$');
        const emailRegex = RegExp('^\\S+@\\S+\\.\\S+$');
        const nameRegEx = RegExp('^[a-zA-z -]{1,30}$');

        let nameTest = nameRegEx.test(firstNameText)
        let lastNameTest = nameRegEx.test(lastNameText)
        let phoneTest = phoneRegEx.test(phoneText)
        let emailTest = emailRegex.test(emailText)
        let arrivalTest = arrivalSelection!=null
        let departureTest = departureSelection!=null

        console.log({
            nameTest: nameTest,
            lastNameTest: lastNameTest,
            phoneTest: phoneTest,
            emailTest: emailTest
        })


        if (nameTest, lastNameTest, phoneTest, emailTest,arrivalTest,departureTest) {

            changeRejectedArrival(false)
            changeRejectedDeparture(false)
            changeRejectedPhone(false)
            changeRejectedEmail(false)
            changeRejectedFName(false)
            changeRejectedLName(false)

            console.log("All Tests passed")

            let final_pets = hasPets ? petIndex + 1 : 0

            let {total,days} = displayPrice()

            const specs = {
                arrival: dayjs(arrivalSelection).format('YYYY-MM-DD'),
                departure: dayjs(departureSelection).format('YYYY-MM-DD'),
                guests: guestIndex + 1,
                pets: final_pets,
                petDetails: speciesText,
                fname: firstNameText,
                lname: lastNameText,
                phone: phoneText,
                email: emailText,
                comments: commentsText,
            }

            const priceInfo = {
                totalCleaningFee: cleaningFee.toString(),
                totalPetFee: petFee.toString(),
                totalRentalFee: currency(total).toString(),
                totalTax: taxes.toString(),
                totalPriceUnTaxed: untaxedTotal.toString(),
                totalPriceTaxed: taxedTotal.toString(),
                days: days
            }

            const json = {
                specs: specs,
                priceInfo: priceInfo,
                id: unit.unitid
            }

            console.log(json)

            console.log("JSON created calling PostInquiry")

            PostInquiry(json).then((res)=>{
                if(res.OK) {
                    changeAccepetedMsg('accept')
                }
                else{
                    changeAccepetedMsg('reject')
                }

                changeMsgOn(true)
                setTimeout(()=>{resetState(false)},8000)
                resetState(true)
                animateScroll.scrollToTop()

            })
        }

        else {

            console.log("Some tests failed, should be reflected by red text")

            changeRejectedArrival(!arrivalTest) //calenders need chanaging
            changeRejectedDeparture(!departureTest)
            changeRejectedPhone(!phoneTest)
            changeRejectedEmail(!emailTest)
            changeRejectedFName(!nameTest)
            changeRejectedLName(!lastNameTest)
        }

    }

    const resetState = (reset) => {
        if (reset) {
            setguestIndex(0)
            setHasPets(false)
            setpetIndex(0)
            changeSpeciesText("")
            changeFirstNameText("")
            changeLastNameText("")
            changePhoneText("")
            changeEmailText("")
            changeCommentsText("")
            updateArrivalSelection(null)
            updateDepartureSelection(null)
        }
        else{
            changeMsgOn(false)
        }
    }

    const handleDayClickArrival = (day, { selected, disabled }) => {
        if (selected) {
            updateArrivalSelection(null)
            updateDepartureSelection(null)
            return
        }
        if (disabled) { return }
        else {
            updateArrivalSelection(day)
            changeRejectedArrival(false)
            updateDepartureSelection(null)
            changeInitialMonth(new Date(day.getFullYear(), day.getMonth()))
        }
    }

    const handleDayClickDeparture = (day, { selected, disabled }) => {
        if (selected) {
            updateDepartureSelection(null)
            return
        }
        if (disabled || arrivalSelection==null) { return }
        else {
            updateDepartureSelection(day)
            changeRejectedDeparture(false)
        }
    }

    const arrivalCalenderTrans = useTransition(arrivalCalenderOpen, null, {
        from: { height: '0px', opacity: 0 },
        enter: { height: '300px', opacity: 1 },
        leave: { height: '0px', opacity: 0 },
        unique: true
    })

    const departureCalenderTrans = useTransition(departureCalenderOpen, null, {
        from: { height: '0px', opacity: 0 },
        enter: { height: '300px', opacity: 1 },
        leave: { height: '0px', opacity: 0 },
        unique: true
    })

    const acceptedTrans = useTransition(acceptedMsgOn, null, {
        from: { opacity: 0, height: '0px' },
        enter: { opacity: .8, height: '100px' },
        leave: { opacity: 0, height: '0px' },
        unique: true
    })

    const dateRange = (arrivalSelection !== null && departureSelection !== null ?
        Date.prototype.getRange(new Date(arrivalSelection), new Date(departureSelection))
        :
        []
    )

    const displayPrice = () => {

        let total = currency(0)
        //keep track of indivisual data on each day
        let Days = []

        if(dateRange.length===0) {
            return currency(total)
        }
        dateRange.forEach((date_range_date,index) => {
            if(index===dateRange.length-1)
                return
            let calenderDate = calender.days.find(calender_entry => dayjs(calender_entry.date).isSame(dayjs(date_range_date),'day'))

            if (calenderDate != null) {
                //Do this so as not to mutate
                let calenderDateCopy = {...calenderDate}
                //manipulator info
                let Manipulators = []
                //date/price information
                let DayPriceInfo ={date: dayjs(calenderDate.date).format('YYYY-MM-DD')}

                //If price manipulators include this date, apply price changes
                if(priceManipulators!=null){
                //iterate through each manipulator
                    priceManipulators.forEach(manipulator=>{

                        if(!manipulator.enabled){
                            return 
                        }
                            
                        //iterate through each date
                        manipulator.dates.every(man_date=>{
                            if(dayjs(man_date).isSame(dayjs(date_range_date),'day')&&typeof manipulator.value==='number'
                                && manipulator.value===manipulator.value){
                                switch(manipulator.operation){
                                    case '+':
                                        calenderDateCopy.price=(currency(calenderDateCopy.price).add(manipulator.value)).value
                                        break;
                                    case '-':
                                        calenderDateCopy.price=(currency(calenderDateCopy.price).subtract(manipulator.value)).value
                                        break;
                                    case '*':
                                        calenderDateCopy.price=(currency(calenderDateCopy.price).multiply(manipulator.value)).value
                                        break;
                                    case '/':
                                        calenderDateCopy.price=(currency(calenderDateCopy.price).divide(manipulator.value)).value
                                        break;
                                    default:
                                        console.error(`Error: Invalid operation for a manipulated date: ${manipulator.operation}`)
                                        break;
                                }

                                //Push the affecting manipulator
                                Manipulators.push({id: manipulator.id,name: manipulator.name,
                                    value: manipulator.value, operation: manipulator.operation})

                                return false
                            }
                            return true
                        })
                    })
                }

                DayPriceInfo.basePrice=calenderDate.price
                DayPriceInfo.finalPrice=calenderDateCopy.price
                DayPriceInfo.priceManipulators=Manipulators

                total=total.add(calenderDateCopy.price)
                Days.push(DayPriceInfo)
            }
            else{
                console.error('Could not find date in calender: ')
                console.error(dayjs(date_range_date).format('YYYY-MM-DD'))
            }

        })
        return {
            total: total.value,
            days: Days
        }
    }

    const petFee = hasPets ? currency(s.pet_fee) : currency(0)
    const cleaningFee = currency(s.cleaning_fee)
    const rentalFee = currency(displayPrice().total)
    const untaxedTotal = arrivalSelection!=null&&departureSelection!=null ? currency(displayPrice().total).add(petFee).add(cleaningFee) : petFee.add(cleaningFee)
    const taxes = untaxedTotal.multiply(TAX_RATE)
    const taxedTotal = untaxedTotal.add(taxes)

    return (
        <>
            <div className='space'>

            </div>
            {acceptedTrans.map(({ item, key, props }) => (
                <animated.div
                    key={key}
                    style={props}
                    className='animated-div'
                >
                    {item ? <div className={acceptedMsg==='accept' ? 'accepted-msg-container' : 'rejected-msg-container'}>
                            {acceptedMsg==='accept' ? m_accept : m_reject }</div> : ''}
                </animated.div>
            ))}
            <div className='unit-container'>

                <div className='unit-title'>
                    {unit.unitname}
                </div>
                <div className='slide-show-container'>
                    <SlideShow picKeys={PictureKeys} index={index} setIndex={setIndex}/>
                </div>
                <div className='unit-about'>
                    <div className='about-text-container'>
                    <pre className='unit-about-text'>
                            {unit.longdesc}
                    </pre>
                    </div>
                </div>
                <div className='unit-form-container'>
                <div style={{position: "absolute",top: '-100px'}} ref={beginForm}>

                </div>
                    <div className='unit-form'>

                        <p className='form-title'>
                            Book this Unit Now!
                        </p>
                        <div className='inq-form-container'>
                            <div className='select-row-container'>
                                <div className="form-item" onMouseEnter={() => toggleArrivalCalender(true)}
                                    onMouseLeave={() => toggleArrivalCalender(false)}>
                                    Arrival{rejectedArrival ? <span className='rejection-msg'>   *We could not accept this date</span> : ''}
                                    <input className='input-first' disabled={true} value={arrivalSelection != null ?
                                        arrivalSelection.toLocaleDateString() : ''} />
                                    {arrivalCalenderTrans.map(({ item, key, props }) => (
                                        <animated.div style={props}>
                                            {item ?
                                                <div className='calender-container'>
                                                    <DayPicker onDayClick={handleDayClickArrival}
                                                        selectedDays={arrivalSelection}
                                                        modifiers={{ disabled: [...disabledDates,...pastDays] }}
                                                        initialMonth={initialMonth} 
                                                        fromMonth={today}
                                                        toMonth={new Date(today.getFullYear()+1,today.getMonth(),today.getDate())}
                                                        />
                                                </div>
                                                :
                                                <></>
                                            }
                                        </animated.div>

                                    )
                                    )}
                                <div style={{height: '30px'}}>

                                </div>
                                </div>
                                <div className="form-item" onMouseEnter={() => toggleDepartureCalender(true)}
                                    onMouseLeave={() => toggleDepartureCalender(false)}>
                                    Departure{rejectedDeparture ? <span className='rejection-msg'>   *We could not accept this date</span> : ''}
                                    <input className='input-first' disabled={true} value={departureSelection != null ?
                                        departureSelection.toLocaleDateString() : ''} />
                                    {departureCalenderTrans.map(({ item, key, props }) => (
                                        <animated.div style={props}>
                                            {item ?
                                                <div className='calender-container'>
                                                    <DayPicker onDayClick={handleDayClickDeparture}
                                                        selectedDays={departureSelection}
                                                        modifiers={{ disabled: [...disabledDates,...pastDays,...getConflictingDepartureSelections()]}}
                                                        initialMonth={initialMonth}
                                                        fromMonth={today}
                                                        toMonth={new Date(today.getFullYear()+1,today.getMonth(),today.getDate())}
                                                        />
                                                </div>
                                                :
                                                <></>
                                            }
                                        </animated.div>

                                    )
                                    )}
                                </div>
                            </div>
                            <div style={{height: '30px'}}>

                                </div>
                            <div className='form-item'>
                                <div id='guests' name='guests'>Guests</div>
                            <Select options={guestOptions} value={guestOptions[guestIndex]} onChange={handleChange} />
                            </div>
                            <div className='select-row-container'>
                                <div className='form-item'>
                                    Pets?
                                <Select options={hasPetOptions} value={hasPets ? hasPetOptions[1] : hasPetOptions[0]} onChange={handleChange}
                                        isDisabled={r.pets === 'false'} />
                                </div>
                                <div className='form-item'>
                                    How Many?
                                <Select options={numberPetsOptions} isDisabled={!hasPets} value={numberPetsOptions[petIndex]}
                                        onChange={handleChange} />
                                </div>
                                <div className='form-item'>
                                    Breed(s)/Species?
                                <input className='input-breed' disabled={!hasPets} onChange={handleTextChange} maxLength={40}
                                    value={speciesText} />
                                </div>
                            </div>
                            <div className='select-row-container'>
                                <div className='form-item'>
                                    First Name{rejectedFName ? <span className='rejection-msg'>   *We could not accept this name</span> : ''}
                                    <input className='input-first' onChange={handleTextChange} maxLength={20} 
                                        value={firstNameText}/>
                                </div>
                                <div className='form-item'>
                                    Last Name{rejectedLName ? <span className='rejection-msg'>   *We could not accept this last name</span> : ''}
                                    <input className='input-last' onChange={handleTextChange} maxLength={20}
                                        value={lastNameText} />
                                </div>
                            </div>
                            <div className='select-row-container'>
                                <div className='form-item'>
                                    Phone Number{rejectedPhone ? <span className='rejection-msg'>   *We could not accept this phone number</span> : ''}
                                    <input className='input-phone' onChange={handleTextChange} maxLength={12} 
                                        value={phoneText}/>
                                </div>
                                <div className='form-item'>
                                    Email{rejectedEmail ? <span className='rejection-msg'>   *We could not accept this email</span> : ''}
                                    <input className='input-email' onChange={handleTextChange} maxLength={30}
                                        value={emailText} />
                                </div>
                            </div>
                            <div className='contact-us-grid-wrapper'>
                                <div className='contact-us-grid-layout'>
                                    <div className='text-area-container'>
                                        <p className='comments'>Comments/Questions?</p>
                                        <textarea className='text-area' onChange={handleTextChange} maxLength={400}
                                            value={commentsText} />
                                    </div>
                                    <div className='unit-btn-container'>
                                        <div className='unit-send-btn' onClick={handleSubmit}>
                                            Send
                                        </div>
                                    </div>
                                    <div className='unit-price-container'> 
                                        <div className='unit-price-inner'>
                                            <div className='checkin-out-container'>
                                                <div className='title'>Arrival</div>
                                                <div className='val'>{arrivalSelection ? dayjs(arrivalSelection).format('MM/DD/YYYY') : <Loader/>}</div>
                                                <div className='title'>Departure</div>
                                                <div className='val'>{departureSelection ? dayjs(departureSelection).format('MM/DD/YYYY') : <Loader/>}</div>
                                            </div>
                                            {rentalFee.intValue>0 ? 
                                            <div className='prices-main'>
                                                <div className='title'>Rental</div>
                                                <div className='val'>${rentalFee.toString()}</div>
                                                <div className='title'>Cleaning</div>
                                                <div className='val'>${cleaningFee.toString()}</div>
                                                <div className='title'>Pets</div>
                                                <div className='val'>${petFee.toString()}</div>
                                                <div className='title'>Taxes</div>
                                                <div className='val'>${taxes.toString()}</div>
                                            </div>
                                                :
                                            <div className='prices-main' style={{display: 'flex',justifyContent:'center',alignItems:'center',minHeight: '220px'}}>
                                                <Loader/>
                                            </div>
                                            }
                                            <div className='total-div'>
                                                <div className='title'>Total</div>
                                                <div className='val'>${rentalFee.intValue>0&&taxedTotal.intValue>0 ? taxedTotal.toString() : <Loader/>}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    
                </div>
            </div>
        </>
    )
}


export default Unit
