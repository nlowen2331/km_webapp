import { render } from 'react-dom'
import React, { useState, useCallback } from 'react'
import { useTransition, animated } from 'react-spring'
import './Test.css'
import currency from 'currency.js'

export default function Test() {
  
  const [array,changearray] = useState([currency(0)])

  const assignCurrency = () => {
    console.log('Called Function')
    let newarray = array
    newarray.push(currency(array[array.length-1]+0.51))
    console.log(newarray)
    changearray(newarray)
  }

  console.log(array)
 
  return (
    <div style={{paddingTop:'100px',height:'200px',width:'100%'}} onClick={(()=>{assignCurrency()})}>
        
    </div>
  )
}