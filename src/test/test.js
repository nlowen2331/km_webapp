const currency = require('currency.js')
//returns an object with {display,stringValue,value}
//display format: +$30.00, stringValue format: +30,value format: {operator: '+', value: 30}
const ValueInterprter = (value) =>{

    //Set both regexps
    let operatorRegEx=/(add|subtract|sub|divide|div|times|mul|mult|multiply|x|\+|-|\*|\/){1}/g
    //Get Operator
    let Operator=operatorRegEx.exec(value)
    let OperatorValue

    //determine operator
    if(Operator!=null) {
        switch(Operator[0]) {
            case 'subtract':
            case 'sub':
            case 'su':
            case 's':
            case '-':
                OperatorValue='-'
                break;
            case 'multiply':
            case 'mult':
            case 'mul':
            case 'mu':
            case 'm':
            case 'times':
            case 'x':
            case '*':
                OperatorValue='*'
                break;
            case 'divide':
            case 'div':
            case 'di':
            case 'd':
            case '/':
                OperatorValue='/'
                break;
            default:
                console.log('Defaulted to +')
                OperatorValue='+'
                break;
        }
    }
    else{
        OperatorValue='+'
    }
    
    //Set floatingRegEx
    let floatingRegEx=/[0-9]+.{0,1}[0-9]*/g

    //Get float
    let Float = floatingRegEx.exec(value)
    let CurrencyValue = currency(0)
    //Set default if null
    if(Float!=null) {
        CurrencyValue=currency(Float[0])
    }

    //Return
    return Interpreted = {
        display: `${OperatorValue}$${CurrencyValue.toString()}`,
        stringValue: `${OperatorValue}${CurrencyValue.value}`,
        value: {
            operator: OperatorValue,
            value: CurrencyValue.value
        }
    }
}

let Obj = {
    someValue: 1,
    notavalue: null,
    nextValue: 'Hello'
}

console.log(Obj)