
const _YORK_1_PICTURES = [
    "IMG_0705.jpg","IMG_0713.jpg","IMG_0988.jpg","IMG_2394.jpg","IMG_0722.jpg","IMG_0670.jpg","IMG_0717.jpg","IMG_2489.jpg"
]

const _YORK_2_PICTURES = [
    "IMG_2294.jpg","IMG_2987.jpg","IMG_2768.jpg","IMG_2303.jpg","IMG_0987.jpg","IMG_0280.jpg" 
]

const _YORK_5_PICTURES = [
    "IMG_0391.jpg","IMG_0407.jpg","IMG_0689.jpg","IMG_2355.jpg","IMG_2764.jpg","IMG_2527.jpg"
]

const _YORK_6_PICTURES = [
    "IMG_2352.jpg","IMG_2379.jpg","IMG_0700.jpg","IMG_2550.jpg","IMG_2381.jpg","IMG_2785.jpg"
]

const _1F_ATTITASH_PICTURES = [
    "IMG_2622.jpg","IMG_2816.jpg","IMG_2626.jpg","IMG_2629.jpg","IMG_2634.jpg"
]

const _1H_ATTITASH_PICTURES = [
    "IMG_0120.jpg","IMG_0131.jpg","IMG_0145.jpg","IMG_0467.jpg","IMG_1198.jpg","IMG_2675.jpg"
]

const getPictureNames = (index) => {
    switch(index) {
        case 'YORK_1':
            return _YORK_1_PICTURES
        case 'YORK_2':
            return _YORK_2_PICTURES
        case 'YORK_5':
            return _YORK_5_PICTURES
        case 'YORK_6':
            return _YORK_6_PICTURES
        case '1F_ATT':
            return _1F_ATTITASH_PICTURES
        case '1H_ATT':
            return _1H_ATTITASH_PICTURES
        default:
            return []
    }
}

export default getPictureNames