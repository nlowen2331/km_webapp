import imconfig from './config.js'
const config = imconfig.config

const Update = (returnData,returnLoaded)=> fetch(config.ip+config.port+config.route_getUnits).then((res)=> {
    res.json().then(json=>{
        if(json!==null && json.error_code==0) {

            const data = json[0].filter((e)=>{
                if(e.specifications==null||e.calender==null||e.airbnbid==null||e.pictures==null)
                    return false
                return true
            })
            returnData(data)
            returnLoaded(true)
        }
        else{
            /*
                The significance of this is the items are no longer up to date
            */
        }
    });
}).catch(err=>{
    console.log('There was an error getting unit data')
    console.log(err)
});

export default Update