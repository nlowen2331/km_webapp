const { config } = require("./config")

//Takes in a picture key (ex. YORK_2/IMG_0098.jpg) and returns URL to picture

const GetPicture = async (unitid,key) =>{
    let error = 'none'

    let ResStream = await fetch(config.ip+config.port+config.route_GetImage+`/${unitid}/${key}`).catch(err=>{
        error=err
    })

    if(error!=='none'){throw new Error(error)}
    
    let Blob = await ResStream.blob()

    let Image = URL.createObjectURL(Blob)

    return ({OK: true,img: Image})
    
}

const GetPictureFullKey = async (key) =>{

    let ResStream 
    
    ResStream = await fetch(config.ip+config.port+config.route_GetImage+`/${key}`)
    
    let Blob = await ResStream.blob()

    let Image = URL.createObjectURL(Blob)

    return ({OK: true,img: Image})
    
}

export {GetPicture,GetPictureFullKey}