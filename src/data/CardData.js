
const CardData = [
    {
    "key": 154389,
    "index": "1F_ATT",
    "name": "1F Attitash",
    "city": "Attitash",
    "state": "NH",
    "beds": 1,
    "baths": 1,
    "guests": 1,
    "wifi": true,
    "coverpicture": "images/1F_ATT/cover.jpg",
    "pictures": [
        "IMG_2622.jpg","IMG_2816.jpg","IMG_2626.jpg","IMG_2629.jpg","IMG_2634.jpg"
    ],
    "ammenities": [
        "Hot Tub","Pool","Clubhouse","Towels"
    ],
    "attractions" : [
        "Mountains","Trails","The Kancamangus Highway","Snow","Colorful Leaves"
    ],
    "description1": "This spacious unit is newly renovated and is in the closest building to the clubhouse. it has two mini splits to keep you cool all summer long. You enter into a well stocked kitchen with a full sized refrigerator.  Off the kitchen is a washer and dryer and a over-sized closet for storage.  The dining area has a very large table, able to comfortably seat 10.  Not only great for family dining,  but makes a great gathering space for family game night or doing a puzzle. Off the dining area is a spacious living area with flat screen TV, comfy couches and cozy blankets for movie watching.  There is a full bath off the living room.  The master bedroom has a king size bed, flat screen TV, private bathroom and balcony.  Upstairs is a loft, on the right side of the loft is a queen bed.  On the left side are 2 bunk-beds, both twin over full.  There is also a couch, TV and games for the kids.",
    "calender(UNSUPPORTED)": null,
    "link": "/Attitash1F",
    },
    {
    "key": 296752,
    "index": "1H_ATT",
    "name": "1H Attitash",
    "city": "Attitash",
    "state": "NH",
    "beds": 1,
    "baths": 1,
    "guests": 1,
    "wifi": true,
    "coverpicture": "images/1H_ATT/cover.jpg",
    "pictures": [
        "IMG_0120.jpg","IMG_0131.jpg","IMG_0145.jpg","IMG_0467.jpg","IMG_1198.jpg","IMG_2675.jpg"
    ],
    "description1": "Come stay at this well appointed condo close to all that the White Mountains has to offer.  The condo is less than a mile from Attitash Mountain, 5 miles to Storyland, around the corner from downtown Jackson, and 8 miles to the heart of North Conway.  Smoke free After a day of hiking, biking or shopping come back to the condo and enjoy all the amenities that Seasons at Attitash has to offer.  There is a pool, giant hot tub, game room, tennis courts, a fishing pond, and trails to hike. This 2 bedroom plus sleeping loft condo is in the best location at Seasons.  It is literally 100 steps from the Season's clubhouse where you can swim, take a sauna, or relax in the largest Jacuzzi in the White Mountains.  There's even an a pond that you can see from the back deck.  You can fish at the pond in the summer and skate on it in the summer. This condo comfortably sleeps 10.",
    "ammenities": [
        "Hot Tub","Pool","Clubhouse","Towels"
    ],
    "attractions" : [
        "Mountains","Trails","The Kancamangus Highway","Snow","Colorful Leaves"
    ],
    "calender(UNSUPPORTED)": null,
    "link": "/Attitash1H",
    },
    {
    "key": 924392,
    "index": "YORK_2",
    "name": "Unit 2 York",
    "city": "York",
    "state": "ME",
    "beds": 6,
    "baths": 2,
    "guests": 6,
    "wifi": true,
    "coverpicture": "images/YORK_2/cover.jpg",
    "pictures": [
        "IMG_2294.jpg","IMG_2987.jpg","IMG_2768.jpg","IMG_2303.jpg","IMG_0987.jpg","IMG_0280.jpg"
    ],
    "ammenities": [
        "Towels","Hot Dogs","Attractive Ladies/Men","Sand"
    ],
    "attractions" : [
        "Longs Sands Beach","Arcade","Neighbor Bill"
    ],
    "description1": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "calender(UNSUPPORTED)": null,
    "link": "/York2"
    },
    {
        "key": 559324,
        "index": "YORK_5",
        "name": "Unit 5 York",
        "city": "York",
        "state": "ME",
        "beds": 2,
        "baths": 1,
        "guests": 2,
        "wifi": true,
        "coverpicture": "images/YORK_5/cover.jpg",
        "pictures": [
            "IMG_0391.jpg","IMG_0407.jpg","IMG_0689.jpg","IMG_2355.jpg","IMG_2764.jpg","IMG_2527.jpg"
        ],
        "ammenities": [
            "Towels","Hot Dogs","Attractive Ladies/Men","Sand"
        ],
        "attractions" : [
            "Longs Sands Beach","Arcade","Neighbor Bill"
        ],
        "description1": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "calender(UNSUPPORTED)": null,
        "link": "/York5"
    },
    {
        "key": 302499,
        "index": "YORK_6",
        "name": "Unit 6 York",
        "city": "York",
        "state": "ME",
        "beds": 2,
        "baths": 1,
        "guests": 2,
        "wifi": true,
        "coverpicture": "images/YORK_6/cover.jpg",
        "pictures": [
            "IMG_2352.jpg","IMG_2379.jpg","IMG_0700.jpg","IMG_2550.jpg","IMG_2381.jpg","IMG_2785.jpg"
        ],
        "ammenities": [
            "Towels","Hot Dogs","Attractive Ladies/Men","Sand"
        ],
        "attractions" : [
            "Longs Sands Beach","Arcade","Neighbor Bill"
        ],
        "description1": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "calender(UNSUPPORTED)": null,
        "link": "/York5"
        }
]

export {CardData}