const { config } = require("./config")


//params: {route,token,changes,unitid}
const PostChangesToServer = async (params) => {

    let Response = await fetch(config.ip + config.port + params.route, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + params.token
            },
            body: JSON.stringify({
                changes: params.changes,
                unitid: params.unitid
            })
        })
    
    let Status = Response.status

    try{
        let JSON = await Response.json()
        console.log(JSON)
    }
    catch(err) {
        
    }
    
    switch(Status) {
        //return success
        case 200:
            return({msg: 'OK'})
        //Service unavailable, try again
        case 503:
            return({msg: 'Retry'})
        //Forbidden
        case 403:
            return({msg: 'Forbidden'})
        //Bad request
        case 409:
            return({msg: 'Bad Request'})
        default:
            console.log('App has no response to HTTP Code '+Status)
            return({msg: 'Error'})
    }

}


export default PostChangesToServer