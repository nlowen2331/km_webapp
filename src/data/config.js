
const config = {
    ip: 'https://test.sandandskirental.com',
    port: '',
    route_getUnits: '/GetUnits',
    route_test: '/Ping',
    route_postInq: '/Inquiry',
    route_postMessage: '/Message',
    route_authenticate: '/Authenticate',
    route_login: '/Login',
    route_getInquiries: '/GetInquiryByOwner',
    route_updateStatus: '/UpdateStatus',
    route_updateSpecs: '/UpdateSpecs',
    route_post_owner_comment: '/PostOwnerComment',
    route_getCalenderByUnit: '/SelectCalenderByUnit',
    route_getMessages: '/GetMessages',
    route_update_msg_status: '/UpdateMsgStatus',
    route_getOwner: '/GetOwner',
    route_GetImage: '/GetImage',
    route_GetPictureListing: '/GetPictureListingForUnit',
    route_PostPictureChanges: '/PostPictureChanges',
    route_PostSpecsChanges: '/PostSpecsChanges',
    route_PostCalenderChanges: '/PostCalenderChanges',
    route_PostPriceMChanges: '/PostPriceMChanges',
    route_GetPriceManipulators: '/GetPriceManipulatorsForUnit',
    route_GetLeaseForUnit: '/GetLeaseForUnit',
    route_PostLeaseToS3: '/PostLeaseToS3',
    route_AcceptInquiryV2: '/AcceptInquiryV2',
    route_PostMiscChanges: '/PostMiscChanges'
}

exports.config = config