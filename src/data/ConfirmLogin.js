import imconfig from '../data/config'
const config = imconfig.config

const ConfirmLogin = (token, setSuccesful, clearToken,cacheName) => fetch(config.ip + config.port + config.route_login, {
    method: "get",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +token
    }
}).then((res) => {
    res.json().then(json => {
        console.log('Recieved login response, checking for errors')
        if (json.error_code === 0) {
            console.log('No errors, login success')
            setSuccesful(true)
            cacheName(json.user)
            return
        }
        else if (json.error_code === 9 || json.error_code === 10) {
            console.log('Token invalid or expired')
            clearToken(true)
            return
        }
        console.log('Unexpected error occured when attempting to confirm Login')
    });
}).catch(err => {
    console.log('Server gave no response or invalid response to login confirmation')
    console.log(err)
});

export default ConfirmLogin
