import imconfig from '../data/config'
const config = imconfig.config

const Authenticate = (user,pass,setToken,setRejectionReason,setFalseOnCompletion) => fetch(config.ip + config.port + config.route_authenticate, {
    method: "post",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        username: user,
        password: pass
    })
}).then((res) => {
    res.json().then(json => {
        console.log('Recieved server response checking for problems')
        if(json!==null && json.error_code==0){
            console.log('Token successfully recieved and set')
            setToken(json.token)
            setFalseOnCompletion(false)
        }
        else{
            console.log('There was an error authanticating')
            switch (json.error_code) {
                //Couldn't find user
                case 5:
                    console.log("Username not found")
                    setRejectionReason('Incorrect user name')
                    setFalseOnCompletion(false)
                    break
                //Password didnt match
                case 6:
                    console.log("Password did not match user")
                    setRejectionReason('Incorrect password')
                    setFalseOnCompletion(false)
                    break
                //Improper Request
                case 7:
                    console.log("The request was made improperly by the website")
                    setRejectionReason('Error')
                    setFalseOnCompletion(false)
                    break
                //Server-side error (the worst case)
                case 8:
                    console.log("There was a server error that caused this request to fail")
                    setRejectionReason('Error')
                    setFalseOnCompletion(false)
                    break
                default:
                    setFalseOnCompletion(false)
                    break;
            }
            setTimeout(()=>{setRejectionReason('')},4000)
        }
    });
}).catch(err => {
    console.log('Server gave no response or invalid response to the authentication request')
    console.log(err)
    setRejectionReason('Error: This feature may by momentarily unavailable')
    setTimeout(()=>{setRejectionReason('')},4000)
    setFalseOnCompletion(false)
});

export default Authenticate
