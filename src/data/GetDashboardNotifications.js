
import imconfig from './config.js'
const config = imconfig.config


const GetDashboardNotifications = (updateNotifications, token,sort) => fetch(config.ip + config.port + config.route_getInquiries, {
    method: "get",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
}).then((res) => {
    res.json().then(json => {
        if (json !== null && json.error_code == 0) {
            console.log('Recieved notifications from server with no errors')
            let data = json[0]
            if(sort){
                data = data.sort((a,b)=>{
                    if(a.status!==b.status) {
                        return a.status-b.status
                    }
                    return -(Date.parse(new Date(a.dateof))-Date.parse(new Date(b.dateof)))
                })
            }
            updateNotifications(data)
        }
        else {
            console.log('There was an error getting notifications from the server')
            console.log(json.message)
            /*
                The significance of this is the items are out of date
            */
        }
    });
}).catch(err => {
    console.log('There was an error getting inquiry notifications')
    console.log(err)
});

export default GetDashboardNotifications